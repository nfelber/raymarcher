# Raymarcher

This is a demo of an experimental renderer based on the sphere tracing technique
for raymarching written in CUDA. The renderer has been designed with the two
following goals in mind:

1. Flexibility: sphere tracing is generally used in shaders where the map
function for the scene is hard coded. The proposed renderer allows to compose
scenes at runtime with as much flexibility as possible.
2. Performance: while flexibility implies a compromise on performance, the point
of a flexible renderer would be defeated if performance issues make it unusable.
Hence many design choices have been made regarding performance where it does not
interfere too much with flexibility.

## Sphere tracing and associated challenges

Sphere tracing is a raymarching technique where rays are iteratively marched by
the distance to the nearest geometry as given by a signed distance field. The
signed distance field is implicitely defined by a signed distance function (SDF)
returning the minimal distance to the scene geometry at any point in 3D space.
The value returned by the SDF is a conservative distance $d$ by which the ray
can be marched without crossing any surface geometry (defined as the
zero-isosurface of the distance field). This effectively computes a "safe"
sphere of radius $d$ inside of which the ray can advance without crossing the
zero-isosurface, hence the name. When the distance to the zero-isosurface is
smaller than a certain threshold, a hit is registered and the raymarching
terminates.

Sphere tracing is very popular in the demo-scene community, as it allows to
perform complex rendering with minimal setup and very compact code. It is also
embarrassingly parallel, and can easily be implemented in shaders to be run on
GPUs. It has some noticeable advantages compared to other rendering methods such
as rasterization and raytracing (not exhaustive):

- Exact primitives: mathematical primitives such as spheres must not be
approximated by polygons, resulting in a much better surface resolution.
- Primitive parametrization: as primitives are defined by parmetrized SDFs, they
can be easily and smoothly animated.
- Combinations: primitives distance fields can be fused together to obtain more
complex shapes through mathematical operations such as union, difference,
intersection and their smooth equivalents which yield very organic results.
- Distance field transformation: by transforming the distance field (or part of
it) it is possible to achieve infinite duplication, reflections, twists,
rounding and other kinds of effects with only a very low associated memory cost.
- Displacement maps: similarly, true displacement maps can be used to modify the
distance field in certain places to add geometric details.
- Fractal rendering: SDFs can be approximated for many fractals such as 4D julia sets
and used for raymarching in order to render them in 3D.
- Soft shadows: partial occlusion can be approximated by keeping track of the
minimum distance field value along shadow rays, which allows to render quite
realistic soft shadows without performing any kind of multisampling.
- Memory footprint: complex scenes can be described with a comparatively small
number of primitives, combinations and transforms, which are easy to store due
to their parameterized nature.

And also suffers some noticeable disadvantages (not exhaustive):

- Modelling: signed distance functions are not trivial to define for complex
objects, and often result in the combination of many simpler SDFs. This makes
modelling a complicated process, and no good solution exists for converting
meshes into SDFs to date.
- Inexact distance fields: most of the combinations and distance field
transforms result in an inexact distance field, which in some cases can make
raymarching longer (potentially never terminates) and in other cases can produce
unintended results, such as missing parts of geometry.
- Poor performance guaranties: the worst case scenario for sphere tracing (in
exact distance fields) arises when a ray is marched parallel and very close to a
flat surface, but just not close enough to register a hit. In that case, the ray
will advance in very small steps, requiring a very high number of SDF
evaluations. Planes are therefore an issue, and so are boxes with faces parallel
to a directional light for example.
- Normals: normals need to be estimated through four evaluations of the distance
field, which is costly to perform.
- Texture mapping: there is no trivial way to map textures to procedural
geometry, requiering the use of general texture projection techniques which
leaves little artistic control.

However the main challenge when rendering SDFs lies in another crucial
difference with mesh based techniques: meshes are just standard triangles which
can all be processed in the same fashion (the scene is defined by data), whereas
a SDF is a (parameterized) **function**, as the name implies, which means that
the program flow varies a lot between different SDFs (the scene is mainly
defined by the computations). In other words, no difference is made between
rendering a box or a sphere in mesh based methods (both are an array of
triangles), whereas in sphere tracing two different functions need to be called
depending on whether we render one or the other. And while the former maps
extremely well to the SIMD nature of GPUs, the later relies on program flow
control to evaluate the SDF described by the scene, which can be very costly on
GPUs, as they are not optimized for lots of jumps and branches. These
limitations have been taken into account in the proposed scene format so as to
minimize their impact as much as possible.

## Unsuccessful attempts

### Shader generation

The first very naive attempt inspired by shader based demo-scenes was to write
an OpenGL program which would generate GLSL fragment shader code at runtime
based on a high-level scene description, compile it and send it to the GPU for
rendering a full-screen rectangle.

The idea was quickly abandoned due to the complexity of generating GLSL code
(let alone good code) without compromising on flexibility, and because
generating and compiling shader code on each scene update has a high performance
cost.

### CUDA polymorphic scene

The second attempt was to rewrite the renderer using CUDA with OpenGL
interoperability (for rendering to the framebuffer) to get much more flexibility
than using fragment shaders. This approach made heavy use of polymorphic objects
to represent many types of scene components like primitives and unions, combined
together in a dynamic hierarchical scene data structure residing in the GPU
memory. Every scene component would inherit from a `Node` virtual base class
providing a transform matrix, and components participating to the scene SDF
would further inherit from a `SDFNode` virtual base class and implement the
`originSDF(p)` method which returns the minimal signed distance to the object's
surface given a point `p`. Together with this, for every scene component would
be defined a corresponding device handle (also inheriting from a common
`DeviceNodeHandle` virtual base class) residing in CPU memory in order to keep a
reference to the object and be able to modify it. Here is an example with the
`Sphere` primitive (without implementation details):


```cpp
class Sphere : public SDFNode {
protected:
    float m_radius;

    __host__ __device__ virtual float originSDF(glm::vec3 p) const override;
public:
    __host__ __device__ Sphere(float radius);
    __host__ __device__ float getRadius() const;
    __host__ __device__ void setRadius(float radius);
};

class DeviceSphereHandle : public DeviceNodeHandle {
public:
    __host__ DeviceSphereHandle();
    __host__ virtual void init();
    __host__ virtual void init(const Sphere& s);

    __host__ virtual ~DeviceSphereHandle();

    // Delete copy constructors
    __host__ DeviceSphereHandle(const DeviceSphereHandle&) = delete;
    __host__ DeviceSphereHandle& operator=(const DeviceSphereHandle&) = delete;

    __host__ void setRadius(float radius) const;
};
```

In order to create or modify the object in GPU memory, the device node handle
calls a specialized kernel with the pointer to the underlying node as an
argument. Here is an example with the implementation of
`DeviceSphereHandle::setRadius`:

```cpp
__global__ void setDeviceSphereRadius(Node** sphere_d, float radius) {
    ((Sphere*)(*sphere_d))->setRadius(radius);
}

__host__ void DeviceSphereHandle::setRadius(float radius) const {
    setDeviceSphereRadius<<<1, 1, 0, m_stream>>>(m_devicePtr, radius);
}
```

Finally, constructing the scene on the CPU side would look like the following:

```cpp
// INIT SCENE
DeviceSphereHandle sphereHandle1 = new DeviceSphereHandle();
sphereHandle1.init();
DeviceSphereHandle sphereHandle2 = new DeviceSphereHandle();
sphereHandle2.init();

DeviceBoxHandle boxHandle = new DeviceBoxHandle();
boxHandle.init();

DeviceUnionHandle unionHandle = new DeviceUnionHandle();
unionHandle.init(1.0f);
unionHandle.addComponent(sphereHandle1);
unionHandle.addComponent(sphereHandle2);
unionHandle.addComponent(boxHandle);

DeviceSceneHandle sceneHandle = new DeviceSceneHandle();
Camera camera(radians(45.0f), 1.0f, 0.1f, 100.0f);
camera.transform(translate(cameraPosition));
sceneHandle.init(camera);
sceneHandle.addSDFNode(unionHandle);

// [...]

// RENDERING LOOP
// Dynamically modify sphere radius
sphereHandle1.setRadius(0.1f * time);
renderer.render(width, height, sceneHandle);
```

Rendering is then made trivial by dynamic polymorphism, as evaluating the SDF
for the scene is as simple as calling the `evalSDF()` function (`originSDF()`
taking transform matrix into account) of all its children and taking the union
(i.e.  the minimum) of the results. In terms of flexibility, this solution is
pretty successful as the program flow naturally follows from dynamic data
structures and polymorphism. However performance was observed to be very poor
even for simple scenes, mainly for the following reasons:

1. Dynamic polymorphism requires nodes to be stored in an array of pointers, as
the array would otherwise be heterogeneous. This means that the actual nodes
need to be allocated on the heap in global device memory and cannot be brought
to shared memory (which doesn't support dynamic memory allocation). This results
in having to fetch the nodes from memory each time their `evalSDF()` method is
called (several millions of times). As global memory has a very high latency,
this becomes a major bottleneck.
2. Each time a polymorphic method is called, a lookup is made to the vtable to
get the address of the corresponding derived method and call it. However,
function calls are very costly to the GPU, as it usually relies on highly
aggressive inlining.

Another drawback is the scalability of the program, as creating and maintaining
a device handle in addition to each node is time consuming and error-prone.

Learning from this attempt, it was hence needed to rethink the whole design
taking these issues into account while trying to maintain an equivalent level of
flexibility.

## Proposed scene format

As discussed in the challenges and previous attempts, the main difficulty of
rendering using sphere tracing is finding a way to represent a scene that can be
sent to the GPU, is able to capture complex hierarchical scenes and can be
efficiently parsed for evaluation of the SDF.

### Serialization

The first measure compared to the polymorphic scene attempt was to move all the
dynamic structures and polymorphism to the CPU, where it offers as much
flexibility (if not more) while being efficiently processed by the hardware.
Then on each frame the CPU builds and sends a serialized data stream
representing the scene and specially designed for efficient parsing on the GPU
from the high-level structure.

The serialized scene is an array of 32 bits unsigned integers (as the 32 threads
in a warp can read up to 4 bytes each in a single coalesced memory access) and
consists in 32 bits headers to direct the flow of the program and raw binary
data (32 bits aligned) containing the SDF parameters.

As binary data is not friendly to deal with, the whole process of serialization
and deserialization has been made as transparent as possible thanks to the
following constructs.

First, all the raw binary data contained in the serialized scene comes from
objects which can be serialized on one side and seamlessly reconstructed on the
other by inheriting from the `Serializable` class. Serialization and
deserialisation is performed through pointer casting, which might not be
portable to every system and compiler. However, it is fast and easy to
implement, and was hence chosen by the lack of a convincing alternative.

```cpp
#define sizeof32b(type) ((sizeof(type) + 3) / 4)

template <typename T>
class Serializable {
public:
	__host__ void serialize(std::vector<unsigned int>& v) const {
        v.reserve(v.size() + sizeof32b(T));
        std::copy(((unsigned int*) this), &((unsigned int*) this)[sizeof32b(T)], std::back_inserter(v));
	}

	__host__ __device__ static unsigned int serializedSize() {
		return sizeof32b(T);
	}

	__host__ __device__ static T deserialize(const unsigned int* raw) {
		return *((T*) raw);
	}
};
```

In a similar fashion, headers can be serialized or deserialized from the
corresponding structs, except that they are compressed to a 32 bit unsigned
integer for serialization. Here is an example of the primitive header:

```cpp
struct PrimitiveHeader {
	PrimitiveId id; // 12 bits
  unsigned int materialIdx; // 12 bits
	unsigned int spaceTransformerCount; // 4 bits
	unsigned int distanceTransformerCount; // 4 bits

	__host__ __device__ PrimitiveHeader(PrimitiveId id, unsigned int materialIdx,
										unsigned int spaceTransformerCount, unsigned int distanceTransformerCount) :
									   		id(id), materialIdx(materialIdx), spaceTransformerCount(spaceTransformerCount),
											distanceTransformerCount(distanceTransformerCount) {}

	__host__ __device__ unsigned int serialize() {
		return (primitiveId2uint(id) << 20) |
			   ((materialIdx & 0xFFF) << 8) |
			   ((spaceTransformerCount & 0xF) << 4) |
			   (distanceTransformerCount & 0xF);
	}

	__host__ __device__ static PrimitiveHeader deserialize(unsigned int raw) {
		return PrimitiveHeader {
			uint2PrimitiveId(raw >> 20),
			(raw >> 8) & 0xFFF,
			(raw >> 4) & 0xF,
			raw & 0xF
		};
	}
};
```

### Scene components

In order to make parsing the scene as easy as possible for the GPU, it has been
necessary to divide scene components into four different categories depending on
their role regarding the SDF evaluation, which have been found to be
sufficiently general to express complex SDFs:

- Primitives
- Combiners
- Space transformers
- Distance transformers

Each component from a given category inherits from a single base class which
itself inherits from `Serializable` and uses the curiously recurring template
pattern
([CRTP](https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern)) to
take advantage of compile-time polymorphism and not suffer from the issues of
dynamic polymorphism discussed in [CUDA polymorphic
scene](#cuda-polymorphic-scene).

Furthermore, for each category is defined an enum which maps to every subclass
derived from the base class, giving them a unique id, and a function which calls
a templated lambda with the corresponding type depending on the given id. Here
for the primitive category as an example:

```cpp
enum class PrimitiveId : uint16_t {
	Sphere,
	Box,
	// [...]
};

#define uint2PrimitiveId(value) static_cast<PrimitiveId>(value)
#define primitiveId2uint(value) static_cast<unsigned int>(value)

template<class F> __host__ __device__ void callOnPrimitiveId(PrimitiveId id, F f) {
	switch (id) {
		case PrimitiveId::Sphere:
			return f.template operator()<Sphere>();
		case PrimitiveId::Box:
			return f.template operator()<Box>();
    // [...]
	}
}
```

This last construct is very powerful, as it allows to write compact generic
blocks of code which are then specialized for every possible subclass in the
category and inlined in a big switch statement at compile-time. At runtime, all
we pay is an indirection to get to the right `case` statement depending on the
received id, and then all the methods are called on an actual instance of the
subtype thanks to the CRTP. Example:

```cpp
callOnPrimitiveId(pid, [&]<typename P>() {
  // Deserialize primitive
  P primitive = P::deserialize(&scenePtr[*sceneIndex]);

  // primitive is now an instance of the derived class corresponding to `pid`
  // and we can call any method implemented by all primitives
});
```

#### Primitives

Every primitive inherits from the `Primitive` base class below. They must define
the static `id()` method which simply returns the associated `PrimitiveId` enum
value, and the `originSDF(p)` method which returns the closest distance from the
point `p` to the surface of the primitive in its own frame of reference.
Optionally, they can also define the `originDirectionalDistance(p, dir, steps)`
which should return a less conservative shortest distance based on additional
ray direction data (it can for example compute the exact intersection point with
the primitive using analytic methods). The default implementation of this method
is to perform `steps` iterations of raymarching in `dir` direction and return
the travelled distance.

```cpp
template <typename T>
class Primitive : public Serializable<T> {
public:
	__host__ __device__ static PrimitiveId id() {
        return T::id();
    }

	__host__ __device__ float originSDF(const glm::vec3& p) {
        return static_cast<T*>(this)->originSDF(p);
    }

	__host__ __device__ float originDirectionalDistance(const glm::vec3& p, const glm::vec3& dir, unsigned int steps) {
		float d = 0.0f;
		for (int i=0; i<steps; ++i) {
			float r = static_cast<T*>(this)->originSDF(p + d * dir);
			d += r;
		}
    return d;
  }
};
```

Adding a primitive to the engine is therefore as simple as inheriting from the
`Primitive` class, implementing the required methods and adding the
corresponding `PrimitiveId` and `case` statement in `callOnPrimitiveId`. For
example the `Sphere` class:

```cpp
class Sphere : public Primitive<Sphere> {
protected:
	float m_radius;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::Sphere;
    }

	__host__ __device__ Sphere() = default;
	__host__ __device__ Sphere(float radius) : m_radius(radius) {}
  __host__ __device__ float getRadius() const { return m_radius; }
  __host__ __device__ void setRadius(float radius) { m_radius = radius; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		return length(p) - m_radius;
	}
};
```

Notice that (also applies to the other categories):
1. all that gets serialized and sent to the device to represent a
sphere is one single float (`m_radius`) and
2. no modification of the renderer is required to use `Sphere` in the scene.

#### Combiners

Every combiner inherits from the `Combiner` base class below. Combiners provide
a way of combining several distances (obtained from several primitives and/or
other combiners) into one single distance.

They must provide an accumulator which is a specialization of `Accumulator`. The
accumulator will be instanciated during the scene evaluation and is responsible
for keeping all the state information it needs. It must provide an
`accumulate(d, hit)` method which is called whenever a new distance (and
corresponding potential hit, which contains additional info such as texture) is
reported, as well as a `getDistance()` and a `getClosestHit()` method which
respectively return the final distance and corresponding hit after accumulation.

The combiner subclass must implement the static `id()` method which returns the
associated `CombinerId` enum value and the `getAccumulator()` method which
returns an instance of the combiner's accumulator. Additionally, they can
override the `enableDirectionalDistance()` method if they want to enable
directional distance evaluation of their children in the scene, and
`enablePrimitiveHit()` if they want to enable isolated evaluation of their
children for normal calculation and texture mapping.

```cpp
template <typename T>
class Combiner : public Serializable<T> {
public:
	template <typename D>
	class Accumulator {
	public:
		__host__ __device__ void accumulate(float d, unsigned int hit) {
			return static_cast<Accumulator<D>*>(this)->accumulate(d, hit);
		}

		__host__ __device__ float getDistance() {
			return static_cast<Accumulator<D>*>(this)->getDistance();
		}

		__host__ __device__ unsigned int getClosestHit() {
			return static_cast<Accumulator<D>*>(this)->getClosestHit();
		}
	};

	__host__ __device__ static bool enableDirectionalDistance() {
        return false;
    }

	__host__ __device__ static bool enablePrimitiveHit() {
        return false;
    }

	__host__ __device__ static CombinerId id() {
        return T::id();
    }

	__host__ __device__ Accumulator<T> getAccumulator() {
        return static_cast<T*>(this)->getAccumulator();
    }
};
```

The reason why accumulators are needed as a separate object is that they contain
some state information which is only useful at evaluation time, and we do not
want these fields to be serialized together with the combiner.

The `SmoothUnion` class as an example subclass:

```cpp
class SmoothUnion : public Combiner<SmoothUnion> {
protected:
	float m_smoothness = 0.0f;

public:
	class SmoothUnionAccumulator : Accumulator<SmoothUnion> {
	protected:
		float m_smoothness = 0.0f;
		float m_d0 = MAXFLOAT;
		float m_d1 = MAXFLOAT;
		unsigned int m_hit;

	public:
		__host__ __device__ SmoothUnionAccumulator(float smoothness) : m_smoothness(smoothness) {}

		__host__ __device__ void accumulate(float d, unsigned int hit) {
			if (d < m_d0) {
				m_d1 = m_d0;
				m_d0 = d;
				m_hit = hit;
			}
			else if (d < m_d1) {
				m_d1 = d;
			}
		}

		__host__ __device__ float getDistance() {
			float h = max(m_smoothness - (m_d1 - m_d0), 0.0f);
			return m_d0 - h * h / (4.0f * m_smoothness);
		}

		__host__ __device__ unsigned int getHit() {
			return m_hit;
		}
	};

	__host__ __device__ static CombinerId id() {
		return CombinerId::SmoothUnion;
	}
	__host__ __device__ SmoothUnion() = default;
	__host__ __device__ SmoothUnion(float smoothness) : m_smoothness(smoothness) {}

	__host__ __device__ SmoothUnionAccumulator getAccumulator() {
		return SmoothUnionAccumulator(m_smoothness);
	}
};
```

#### Space transformer

Each space transformer inherits from the `SpaceTransformer` base class below.
The purpose of space transformers is to transform the input point `p` prior to
the SDF evaluation. A surprising number of sphere tracing techniques can be
implemented using space transformers the simplest (and most useful) of them
being linear transformations.

Space transformers must implement the static `id()` method which returns the
associated `SpaceTransformerId` enum value, and the `transform(p)` method which
returns the point resulting from the transformation of `p`.

```cpp
template <typename T>
class SpaceTransformer : public Serializable<T> {
public:
	__host__ __device__ static SpaceTransformerId id() {
        return T::id();
    }

	__host__ __device__ glm::vec3 transform(const glm::vec3& p) {
        return static_cast<T*>(this)->transform(p);
    }
};
```

And `LinearTransform` as an example:

```cpp
class LinearTransform : public SpaceTransformer<LinearTransform> {
protected:
	glm::vec3 m_position = glm::vec3(0.0);
	glm::quat m_orientation = glm::quat(1.0, 0.0, 0.0, 0.0);

public:
	__host__ __device__ static SpaceTransformerId id() {
    return SpaceTransformerId::LinearTransform;
  }

	__host__ __device__ LinearTransform() = default;
	__host__ __device__ LinearTransform(const glm::vec3& position, const glm::quat& orientation) :
                                      m_position(position), m_orientation(orientation) {}
	__host__ __device__ glm::vec3 getPosition() const { return m_position; }
	__host__ __device__ glm::quat getOrientation() const { return m_orientation; }
	__host__ __device__ void setPosition(const glm::vec3& position) { m_position = position; }
	__host__ __device__ void setOrientation(const glm::quat& orientation) { m_orientation = orientation; }

	__host__ __device__ glm::vec3 transform(const glm::vec3& p) {
		return conjugate(m_orientation) * (p - m_position);
	}

	__host__ __device__ glm::vec3 dirTransform(const glm::vec3& dir) {
		return conjugate(m_orientation) * dir;
	}
};
```

#### Distance transformer

Each distance transformer inherits from the `DistanceTransformer` base class
below.  The purpose of distance transformers is to transform the distance `d`
resulting from the SDF evaluation at point `p`. They can achieve very simple
effects such as rounding, and much more complex ones such as apply true
displacement maps to surfaces.

Distance transformers must implement the static `id()` method which returns the
associated `DistanceTransformerId` enum value, and the `transform(p, d)` method which
returns the distance resulting from the transformation of `d` evaluated at `p`.

```cpp
template <typename T>
class DistanceTransformer : public Serializable<T> {
public:
	__host__ __device__ static DistanceTransformerId id() {
    return T::id();
  }

	__device__ float transform(const glm::vec3& p, float d) {
    return static_cast<T*>(this)->transform(p, d);
  }
};
```

And `Round` as an example:

```cpp
class Round : public DistanceTransformer<Round> {
protected:
	float m_radius;
public:
	__host__ __device__ static DistanceTransformerId id() {
    return DistanceTransformerId::Round;
  }
	__host__ __device__ Round() = default;
	__host__ __device__ Round(float radius) : m_radius(radius) {}
	__host__ __device__ float getRadius() const { return m_radius; }
	__host__ __device__ void setRadius(float radius) { m_radius = radius; }

	__host__ __device__ float transform(const glm::vec3& p, float d) {
		return d - m_radius;
	}
};
```

### Scene serialization format

As mentionned, scene components are just data together with the associated
methods to process it. They do not contain any info on how the scene needs to be
evaluated, i.e. their relation to oneanother. For this reason, headers have been
created for each component, adding all the metadata required to deserialize and
process them. Headers are serialized in a compressed 32 bits format. Scene
component are always preceded by a matching header.

A scene **always** starts with a combiner header, and ends with the special
`CombinerHeader::End` value, such that the number of elements in the scene
doesn't need to be tracked. Otherwise, the whole scene is serialize as follows.

#### Combiner serialization

A combiner header contains the following information:

| Field                       | Size [bits] |
| --------------------------- | ----------- |
| id                          | 5           |
| isRoot                      | 1           |
| proxyCount                  | 4           |
| childCombinerCount          | 3           |
| childPrimitiveCount         | 8           |
| parentSpaceTransformerCount | 5           |
| spaceTransformerCount       | 3           |
| distanceTransformerCount    | 3           |

Where `id` is the corresponding `CombinerId`, `isRoot` indicates wether the
combiner is a children to another combiner or not, and the other fields indicate
then number of different types of components that will follow.

A combiner is serialized in the following way, where the first block is at
the top of the diagram, indented blocks indicate that they are optional and `#`
indicates the number of blocks.

```
     ___________________________
    |                           |
    | Child combiners           | # childCombinerCount
    |___________________________|
 ___________________________
|                           |
| CombinerHeader            |
|___________________________|
     ___________________________
    |                           |
    | Parent space transformers | # parentSpaceTransformerCount
    |___________________________|
 ___________________________
|                           |
| Linear transform          |
|___________________________|
     ___________________________
    |                           |
    | Proxies                   | # proxyCount
    |___________________________|
     ___________________________
    |                           |
    | Space transformers        | # spaceTransformerCount
    |___________________________|
 ___________________________
|                           |
| Serialized combiner       |
|___________________________|
     ___________________________
    |                           |
    | Child primitives          | # childPrimitiveCount
    |___________________________|
     ___________________________
    |                           |
    | Distance transformers     | # distanceTransformerCount
    |___________________________|
```

Serializing child containers before the parent is crucial to allow for bottom up
evaluation and avoid recursion when parsing the scene. Notice that the scene
still necessarily begins with a combiner header!

Note: due to the children being serialized first, proxies need to be transmitted
to the first children, as it will be the entry point for the whole combiner.

#### Proxy serialization

A proxy is a special primitive associated to a combiner that will be evaluated
before the content of the combiner and allows to completely skip the combiner if
the to the proxy is greater than a certain threshold, resulting in a basic but
effective acceleration structure. Its header is very simple:

| Field     | Size [bits] |
| --------- | ----------- |
| isRoot    | 1           |
| skipIndex | 31          |

Where `id` indicates whether it is a proxy to a root combiner, and `skipIndex`
is the scene index to jump at if the combiner can be skipped.

A proxy is serialized in the following way:

```
 ___________________________
|                           |
| ProxyHeader               |
|___________________________|
 ___________________________
|                           |
| Primitive                 |
|___________________________|
```

#### Primitive serialization

Primitives start by their header:

| Field                       | Size [bits] |
| --------------------------- | ----------- |
| id                          | 12          |
| materialIdx                 | 12          |
| spaceTransformerCount       | 4           |
| distanceTransformerCount    | 4           |

And are serialized as follows:

```
 ___________________________
|                           |
| PrimitiveHeader           |
|___________________________|
 ___________________________
|                           |
| Linear transform          |
|___________________________|
     ___________________________
    |                           |
    | Space transformers        | # spaceTransformerCount
    |___________________________|
 ___________________________
|                           |
| Serialized primitive      |
|___________________________|
     ___________________________
    |                           |
    | Distance transformers     | # distanceTransformerCount
    |___________________________|
```

One linear transform (which is in fact a space transformer) is mandatory, due to
how common it is for primitives.

#### SpaceTransformer serialization

Space transformers share one header by groups of up to 4, due to how little
information is required in the header:

| Field                       | Size [bits] |
| --------------------------- | ----------- |
| id1                         | 8           |
| id2                         | 8           |
| id3                         | 8           |
| id4                         | 8           |

And they are serialized as follows:

```
 ___________________________
|                           |
| SpaceTransformerHeader    |
|___________________________|
 ___________________________
|                           |
| Serialized space trans. 1 |
|___________________________|
     ___________________________
    |                           |
    | Serialized space trans. 2 |
    |___________________________|
     ___________________________
    |                           |
    | Serialized space trans. 3 |
    |___________________________|
     ___________________________
    |                           |
    | Serialized space trans. 4 |
    |___________________________|
```

#### DistanceTransformer serialization

Distance transformers are serialized in the exact same way as space transformers:

| Field                       | Size [bits] |
| --------------------------- | ----------- |
| id1                         | 8           |
| id2                         | 8           |
| id3                         | 8           |
| id4                         | 8           |

```
 ___________________________
|                           |
| DistanceTransformerHeader |
|___________________________|
 ___________________________
|                           |
| Serialized dist. trans. 1 |
|___________________________|
     ___________________________
    |                           |
    | Serialized dist. trans. 2 |
    |___________________________|
     ___________________________
    |                           |
    | Serialized dist. trans. 3 |
    |___________________________|
     ___________________________
    |                           |
    | Serialized dist. trans. 4 |
    |___________________________|
```

### Scene parsing

Here follow some details on scene parsing on the GPU.

The scene SDF evaluation consists in the following loop (remember that a scene
always start with a combiner header and ends with the special `End` combiner
header):

1. Initialize minDist to MAXFLOAT.
2. Read combiner header
3. If header is `End`, go to 6.
4. Evaluate combiner SDF.
5. Min-accumulate the result to minDist and go back to 2.
6. Return minDist.

#### Combiner parsing

Combiners are by far the trickiest to parse. This is mostly due to the bottom-up
parsing which is necessary to avoid recursion, as recursion can't be inlined by
the CUDA compiler and results in function calls which add latency and register
usage.

Here is the outline of the algorithm (given an input point):

1. Start by initializing a distance stack, which stores distances computed by
child containers.
2. Read the combiner header.
3. Apply the parent space transformers to the input point.
4. Apply the combiner's linear transform to the point.
5. Evaluate every proxy in order, and if one decides to skip (for every
thread in the warp), we push the distance to the proxy to the distance stack,
then we jump to the skip location.  If the proxy was a root combiner proxy we go
to 15. otherwise we go back to 2.
6. Apply the combiner's space transformers to the point.
7. Deserialize and intanciate the combiner.
8. Get an accumulator instance from the combiner.
9. Pop the distance stack as many times as the number of child combiners and
accumulate the results.
10. Evaluate every child primitive, and accumulate the resulting distances.
11. Compute the accumulated distance.
12. Apply the combiner's distance transformers to the accumulated distance.
13. Push the resulting distance to the distance stack.
14. If the combiner was not a root combiner, we go to 2.
15. Pop the stack and return the result.

Notice how the order of the serialized scene data matches the order in which it
is being processed. This is on purpose, so that the parsing is as linear as
possible.

Some details have been left out from this outline, notably:
- Accumulate and return a closest hit value together with the distance to
retrieve some information such as textureId and hit location after the ray has
been marched.
- Determine if we enable directional SDF evaluation based on combiner
preference, parents preference and presence of non-linear transformers, and
pass the result to child primitive evaluation.

For more details, feel free to look at the code.

#### Primitive parsing

Here is the outline for SDF evaluation:

1. Read the primitive header.
2. Apply the primitive's linear transform to the input point.
3. Apply the primitive's space transformers to the point.
4. Deserialize and instanciate primitive.
5. Determine if we can use directional SDF evaluation based on value passed by
combiner and the presence of non-linear transformers.
6. If enabled, apply linear transform to direction vector and call primitive
`originDirectionalDistance()` method, else call `originSDF()` method.
7. Apply the primitive's distance transformers on the result.
8. return the resulting distance.

Note that we also read the texture index from the header and return it for the
parent combiner to accumulate in the closest hit value.

#### Space and distance transformers parsing

The parsing for space and distance transformers is very simple. It consists in
reading the header for (up to) the next 4 transformers, then deserializing and
instanciating them one by one and successively transforming the input by calling
the transformers `transform()` method.

### Polymorphic scene on the CPU

To get as much flexibility as possible and hide implementation details to the
user, a user-friendly scene representation is used on the CPU. It utilizes 
dynamic vectors and dynamic polymorphism to allow for easy scene construction
and modification at runtime.

In order for the polymorphic scene to work seamlessly with the CRTP datatypes of
the scene components, we define containers for each of the four categories which
provide a polymorphic layer on top of them. This is done by writing a templated
container which inherit from a virtual base class for every scene component
category, so that the container is automatically specialized for every subtype
in the category.

The CRTP type is accessible as a public member for easy access through the
container, and containers implement virtual methods to serialize their content
at scene build time before sending it to the GPU.

Here is an example with the `SpaceTransformerContainer`:

```cpp
class VSpaceTransformerContainer {
public:
    virtual unsigned int getHeader() const = 0;
    virtual void serializeAndPush(std::vector<unsigned int>& v) const = 0;
};

template<typename T>
class SpaceTransformerContainer : public VSpaceTransformerContainer {
    static_assert(std::is_base_of<SpaceTransformer<T>, T>::value, "T must be a descendant of SpaceTransformer<T>");
public:
    T spaceTransformer;

    SpaceTransformerContainer(T st) : spaceTransformer(st) {}

    static std::shared_ptr<SpaceTransformerContainer<T>> createNew(T st) {
        return std::shared_ptr<SpaceTransformerContainer<T>>(new SpaceTransformerContainer<T>(st));
    }

    virtual unsigned int getHeader() const {
        return spaceTransformerId2uint(T::id());
    }

    virtual void serializeAndPush(std::vector<unsigned int>& v) const {
        spaceTransformer.serialize(v);
    }
};
```

Containers are fairly high-level, and keep track of all the information required
to build headers. For example, combiner containers maintain pointers to
containers of their children and to their optional proxy so they can be
recursively serialized. This results in a hierarchical scene representation
where nodes link to one another.

```cpp
class VCombinerContainer : public SDFContainer, public Transformable {
protected:
    std::shared_ptr<VPrimitiveContainer> m_proxy;
    std::vector<std::shared_ptr<VCombinerContainer>> m_childCombiners;
    std::vector<std::shared_ptr<VPrimitiveContainer>> m_childPrimitives;
public:
    // [...]

    void setProxy(std::shared_ptr<VPrimitiveContainer> proxy) {
        m_proxy = proxy;
    }

    void addChildCombiner(std::shared_ptr<VCombinerContainer> child) {
        m_childCombiners.push_back(child);
    }

    void removeChildCombiner(std::shared_ptr<VCombinerContainer> child) {
        m_childCombiners.erase(
            std::remove(m_childCombiners.begin(), m_childCombiners.end(), child),
            m_childCombiners.end());
    }

    void addChildPrimitive(std::shared_ptr<VPrimitiveContainer> child) {
        m_childPrimitives.push_back(child);
    }

    void removeChildPrimitive(std::shared_ptr<VPrimitiveContainer> child) {
        m_childPrimitives.erase(
            std::remove(m_childPrimitives.begin(), m_childPrimitives.end(), child),
            m_childPrimitives.end());
    }
};
```

Finally, a reference to each of the top-level (root) combiners is stored in a
`Scene` object together with scene lights and the camera. They can easily be
added and removed at runtime. The scene class defines a `sendToDevice()` method
which takes care of allocating memory on the GPU, serializing the whole scene
(together with the camera, lights and materials) and copying all the data to the
device for rendering.

```cpp
class Scene {
protected:
    std::shared_ptr<Camera> m_camera;
    std::vector<std::shared_ptr<VCombinerContainer>> m_combiners;
    std::vector<std::shared_ptr<VLightContainer>> m_lights;

public:
    Scene() = default;

    void setCamera(std::shared_ptr<Camera> camera) { m_camera = camera; }
    std::shared_ptr<Camera> getCamera() { return m_camera; }

    void addCombiner(std::shared_ptr<VCombinerContainer> child) {
        m_combiners.push_back(child);
    }

    void removeCombiner(std::shared_ptr<VCombinerContainer> child) {
        m_combiners.erase(
            std::remove(m_combiners.begin(), m_combiners.end(), child),
            m_combiners.end());
    }

    void addLight(std::shared_ptr<VLightContainer> light) {
        m_lights.push_back(light);
    }

    void removeLight(std::shared_ptr<VLightContainer> light) {
        m_lights.erase(
            std::remove(m_lights.begin(), m_lights.end(), light),
            m_lights.end());
    }

    void sendToDevice(/*[...]*/) const {
      // [...]
    }
};
```

Feel free to look at the code for more details. Examples of scenes can be found
in the [test_scenes](./test_scenes/) folder.

## The rendering pipeline

Rendering is performed through a multi-stage pipeline consisting of the
following steps:

1. **Scene setup**: serialize the scene and send it to the device.
2. **Initialize primary rays**: initialize the pool of primary rays depending on
camera position, direction and settings.
3. **March primary rays**: march all the primary rays in the pool until they hit the
scene or the far plane.
4. **Compute normals**: estimate normals at every hit position.
5. **Shade lights**: for each light and each hit position, cast a shadow ray towards
the light, compute PBR shading based on material at hit position and  blend the
results.
6. **Render**: write the colors obtained from the shading pass to the render buffer.

Intermediate results of the rendering process are stored in `Pool` objects,
which seamlessly translates AoS-style calls into SaO operations. This is
performed by managing one array for each 32-bits slice of a datatype under the
hood, and results in a much better memory behavior (operations can coalsece).
Here is an extract of the `Pool` definition:

```cpp
template <typename T>
class Pool {
private:
    unsigned int* m_arrays[sizeof32b(T)];
    unsigned int m_size = 0;
public:
    __host__ __device__ unsigned int size() const {
        return m_size;
    }

    __host__ void allocate(size_t size) {
        for (int i=0; i<sizeof32b(T); ++i) {
            cudaMalloc(&m_arrays[i], size * sizeof(unsigned int));
        }
        m_size = size;
    }
    __host__ void free() {
        for (int i=0; i<sizeof32b(T); ++i) {
            checkCudaErrors(cudaFree(m_arrays[i]));
        }
        m_size = 0;
    }

    __device__ T read(unsigned int index) {
        unsigned int obj[sizeof32b(T)];
        for (int i=0; i<sizeof32b(T); ++i) {
            obj[i] = m_arrays[i][index];
        }
        return *((T*) obj);
    }

    __device__ void write(const T& element, unsigned int index) {
        unsigned int* obj = (unsigned int*) &element;
        for (int i=0; i<sizeof32b(T); ++i) {
            m_arrays[i][index] = obj[i];
        }
    }

    // [...]
};
```

### Initialize primary rays