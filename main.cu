#include <stdio.h>
#include <iomanip>
#include <stdlib.h>
#include <GL/glew.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "rm_renderer.cuh"
#include "cuda_helper.cuh"
#include "test_scenes/scene1.cuh"
#include "test_scenes/scene2.cuh"
#include "test_scenes/scene3.cuh"
#include "test_scenes/scene4.cuh"
#include "test_scenes/scene5.cuh"
#include "test_scenes/scene6.cuh"
 
using namespace std;
using namespace glm;

unsigned int MAX_FPS = 144;
float FPS_SMOOTHING = 0.95;

GLFWwindow* window;

RMRenderer renderer;

TestScene* scene;

GLuint rbo;
GLuint fbo;

// Viewport dimensions
unsigned int width = 1280;
unsigned int height = 720;

float cameraSpeed = 3.0;
vec3 upDirection = vec3(0.0, 1.0, 0.0);
vec3 movingDirection = vec3(0.0);

dvec2 lastMouseCoords = dvec2(width, height) / 2.0;
float cameraRotationSpeed = 0.005;

bool printFPS = false;
bool printRenderStats = false;
bool showNormals = false;
bool controllingCamera = false;

static void error_callback(int error, const char* description) {
    fprintf(stderr, "Error: %s\n", description);
}

template<typename T>
void changeScene() {
    free(scene);
    scene = new T();

    scene->setup();
}
 
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        controllingCamera = false;
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
    if (key == GLFW_KEY_F && action == GLFW_PRESS)
        printFPS = !printFPS;
    if (key == GLFW_KEY_R && action == GLFW_PRESS)
        printRenderStats = !printRenderStats;
    if (key == GLFW_KEY_N && action == GLFW_PRESS)
        showNormals = !showNormals;

    if (key == GLFW_KEY_1 && action == GLFW_PRESS)
        changeScene<Scene1>();
    if (key == GLFW_KEY_2 && action == GLFW_PRESS)
        changeScene<Scene2>();
    if (key == GLFW_KEY_3 && action == GLFW_PRESS)
        changeScene<Scene3>();
    if (key == GLFW_KEY_4 && action == GLFW_PRESS)
        changeScene<Scene4>();
    if (key == GLFW_KEY_5 && action == GLFW_PRESS)
        changeScene<Scene5>();
    if (key == GLFW_KEY_6 && action == GLFW_PRESS)
        changeScene<Scene6>();

    scene->keyEvent(key, scancode, action, mods);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
        controllingCamera = true;
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        glfwGetCursorPos(window, &lastMouseCoords.x, &lastMouseCoords.y);
}

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos) {
    if (!controllingCamera) return;
    float dx = xpos - lastMouseCoords.x;
    float dy = ypos - lastMouseCoords.y;
    quat cameraOrientation = scene->getCamera()->getOrientation();
    vec3 rightDir = cameraOrientation * vec3(1.0f, 0.0f, 0.0f);
    quat yaw = rotate(quat(1.0, 0.0, 0.0, 0.0), cameraRotationSpeed * -dx, upDirection);
    quat pitch = rotate(quat(1.0, 0.0, 0.0, 0.0), cameraRotationSpeed * -dy, rightDir);
    scene->getCamera()->setOrientation(yaw * pitch * cameraOrientation);
    lastMouseCoords = vec2(xpos, ypos);
}


void setupRendererBuffers(unsigned int w, unsigned int h) {
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA32F, w, h);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
    glFramebufferRenderbuffer(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, rbo);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

    renderer.registerRBO(rbo, w, h);
}

void initialize() {
    changeScene<Scene6>();

	glViewport(0, 0, width, height);
    setupRendererBuffers(width, height);
}

void resize(unsigned int w, unsigned int h) {
	glViewport(0, 0, w, h);

    glDeleteFramebuffers(1, &fbo);
    glDeleteRenderbuffers(1, &rbo);

    renderer.unregisterRBO();
    setupRendererBuffers(w, h);

    width = w;
    height = h;

    cout << "Resized window to " << w << ", " << h << endl;
}

void getInputs() {
    cameraSpeed = 3.0;
    movingDirection = vec3(0.0);
    quat cameraOrientation = scene->getCamera()->getOrientation();
    vec3 lookingDir = cameraOrientation * vec3(0.0f, 0.0f, -1.0f);
    vec3 rightDir = cameraOrientation * vec3(1.0f, 0.0f, 0.0f);
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        movingDirection += lookingDir;
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        movingDirection -= lookingDir;
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        movingDirection -= rightDir;
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        movingDirection += rightDir;
    if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
        movingDirection += upDirection;
    if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
        movingDirection -= upDirection;
    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
        cameraSpeed = 10.0;
        if (glfwGetKey(window, GLFW_KEY_LEFT_ALT) == GLFW_PRESS)
            cameraSpeed = 50.0;
    }
}

void display(float dt) {
    getInputs();

    int w, h;
    glfwGetWindowSize(window, &w, &h);

    if (w != width || h != height) resize(w, h);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    double t = glfwGetTime();

    scene->getCamera()->translate(dt * cameraSpeed * movingDirection);
    scene->update(dt);

    renderer.render(width, height, scene, printRenderStats, showNormals);

    glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBlitFramebuffer(0, 0, width, height, 0, 0, width, height,
                    GL_COLOR_BUFFER_BIT, GL_NEAREST);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}


int main(void) {
    glfwSetErrorCallback(error_callback);

    if(!glfwInit()) {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        return -1;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4); // We want OpenGL 4.1
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

    window = glfwCreateWindow(width, height, "Raymarcher", NULL, NULL);
    if(!window) {
        fprintf(stderr, "Failed to open GLFW window.\n");
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    if (glfwRawMouseMotionSupported())
        glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);

    glfwSetCursorPosCallback(window, cursor_position_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetKeyCallback(window, key_callback);

    glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }

    int dev;
    cudaDeviceProp devProp;
    checkCudaErrors(cudaGetDevice(&dev));
    checkCudaErrors(cudaGetDeviceProperties(&devProp, 0));
    cout << "Using CUDA device: " << devProp.name << "." << endl;

    initialize();

    cout << fixed;
    cout << setprecision(2);
    double lastFrame = 0.0;
    double fps = 0.0;
    while (!glfwWindowShouldClose(window)) {
        double t = glfwGetTime();
        double dt = t - lastFrame;
        if (dt > 1.0 / MAX_FPS) {
            lastFrame = t;
            display(dt);
            glfwSwapBuffers(window);
            fps = fps * FPS_SMOOTHING + (1 - FPS_SMOOTHING) / dt;
            if (printFPS) cout << fps << " fps (render time: " << ((glfwGetTime() - t) * 1000.0) << " ms)   \r" << flush;
        }

        glfwPollEvents();
    }

	renderer.unregisterRBO();
    glDeleteFramebuffers(1, &fbo);
    glDeleteRenderbuffers(1, &rbo);

    glfwDestroyWindow(window);
 
    glfwTerminate();

    cudaDeviceReset();

    exit(EXIT_SUCCESS);
}