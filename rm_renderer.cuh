#ifndef RM_RENDERER_h
#define RM_RENDERER_h

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/ext/quaternion_float.hpp>
#include <cuda_gl_interop.h>
#include <GLFW/glfw3.h>

#include "cuda_helper.cuh"
#include "scene/scene.cuh"

#define RAYMARCH_UNROLL 1
#define INACTIVE_THREADS_THRESHOLD 32
#define EPSILON 0.001f 
#define SHADOW_RAY_NORMAL_OFFSET 2.0f * EPSILON
#define PROXY_EPSILON 2.0f 
#define MAX_STEPS 512
#define DIRECTIONAL_DISTANCE_STEPS 4
#define MAX_COMBINER_CHILDREN 4

struct Ray {
    glm::vec3 origin, direction;
    float length;

    __device__ Ray() = default;
    __device__ Ray(const glm::vec3& o, const glm::vec3& d, float l) : origin(o), direction(d), length(l) {}
    __device__ Ray(const glm::vec3& o, const glm::vec3& d) : origin(o), direction(d), length(0.0f) {}
    __device__ glm::vec3 pos() const { return origin + direction * length; }
    __device__ void march(float dist) { length += dist; }
};

// Hit: isPrimitive (1 bit) | startIdx (14 bits) | endIdx (9 bits) | materialIdx (8 bits)
// startIdx and endIdx are experimental and limit the size of scenes when fitting them both in a 32 bits hit.
#define setHitIsPrimitive(hit, value) hit |= (unsigned int)value << 31
#define getHitIsPrimitive(hit) ((hit >> 31) != 0)
#define setHitStartIdx(hit, value) hit |= (value & 0x3FFF) << 17
#define getHitStartIdx(hit) ((hit >> 17) & 0x3FFF)
#define setHitEndIdx(hit, value) hit |= (value & 0x1FF) << 8
#define getHitEndIdx(hit) ((hit >> 8) & 0x1FF)
#define setHitMaterialIdx(hit, value) hit |= value & 0xFF
#define getHitMaterialIdx(hit) (hit & 0xFF)

// Standard-layout doesn't allow inheriting from ray
struct GeometryRay {
    unsigned int hit = 0.0f;
    glm::vec3 origin, direction;
    float length;

    __device__ GeometryRay() = default;
    __device__ GeometryRay(const glm::vec3& o, const glm::vec3& d, float l) : origin(o), direction(d), length(l) {}
    __device__ GeometryRay(const glm::vec3& o, const glm::vec3& d) : origin(o), direction(d), length(0.0f) {}
    __device__ glm::vec3 pos() const { return origin + direction * length; }
    __device__ void march(float dist) { length += dist; }
};

// Standard-layout doesn't allow inheriting from ray
struct ShadowRay {
    float lightDistance;
    float minPenumbraFactor = 1.0f;
    glm::vec3 origin, direction;
    float length;

    __device__ ShadowRay() = default;
    __device__ ShadowRay(const glm::vec3& origin, const glm::vec3& lightDir, float length, float lightDistance) :
        origin(origin), direction(lightDir), length(length), lightDistance(lightDistance) {}
    __device__ ShadowRay(const Ray& hitRay, const glm::vec3& lightDir, float lightDistance) :
        ShadowRay(hitRay.pos(), lightDir, 0.0f, lightDistance) {}

    __device__ glm::vec3 pos() const { return origin + direction * length; }
    __device__ void march(float dist) { length += dist; }

    
    __device__ void updateMinPenumbraFactor(float lightSoftness, float geometryDistance) {
        minPenumbraFactor = min(minPenumbraFactor, 5.0f * geometryDistance / (max(lightSoftness, 0.001f) * length));
    }
};

template <typename T>
class Pool {
private:
    unsigned int* m_arrays[sizeof32b(T)];
    unsigned int m_size = 0;
public:
    __host__ __device__ unsigned int size() const {
        return m_size;
    }

    __host__ void allocate(size_t size) {
        for (int i=0; i<sizeof32b(T); ++i) {
            cudaMalloc(&m_arrays[i], size * sizeof(unsigned int));
        }
        m_size = size;
    }
    __host__ void free() {
        for (int i=0; i<sizeof32b(T); ++i) {
            checkCudaErrors(cudaFree(m_arrays[i]));
        }
        m_size = 0;
    }

    __device__ T read(unsigned int index) {
        unsigned int obj[sizeof32b(T)];
        for (int i=0; i<sizeof32b(T); ++i) {
            obj[i] = m_arrays[i][index];
        }
        return *((T*) obj);
    }

    __device__ void readMember(unsigned int index, void* member, unsigned int memberOffset, unsigned int memberSize) {
        unsigned int buffer[sizeof32b(T)];
        unsigned int from = memberOffset / sizeof(unsigned int);
        unsigned int to = (memberOffset + memberSize + sizeof(unsigned int) - 1) / sizeof(unsigned int);
        for (int i=from; i<to; ++i) {
            buffer[i] = m_arrays[i][index];
        }
        memcpy(member, &((char*) buffer)[memberOffset], memberSize);
    }

    __device__ void write(const T& element, unsigned int index) {
        unsigned int* obj = (unsigned int*) &element;
        for (int i=0; i<sizeof32b(T); ++i) {
            m_arrays[i][index] = obj[i];
        }
    }

    __device__ void writeMember(const T& element, unsigned int index, const void* member, unsigned int memberOffset, unsigned int memberSize) {
        unsigned int* umember = (unsigned int*) member;
        unsigned int from = memberOffset / sizeof(unsigned int);
        unsigned int to = (memberOffset + memberSize + sizeof(unsigned int) - 1) / sizeof(unsigned int);
        for (int i=0; i<to-from; ++i) {
            m_arrays[from + i][index] = umember[i];
        }
    }
};

class RMRenderer {
private:
    cudaGraphicsResource *m_cudarbo = nullptr;
    cudaSurfaceObject_t m_surface;
	Pool<GeometryRay> m_rayPool;
	Pool<glm::vec3> m_normalPool;
	Pool<glm::vec3> m_colorPool;
	Pool<glm::vec3> m_primitiveSpaceHitPosPool;
	Pool<glm::vec3> m_primitiveSpaceNormalPool;
public:
    RMRenderer();
    void registerRBO(GLuint rbo, unsigned int res_x, unsigned int res_y);
    void unregisterRBO();
    void render(unsigned int res_x, unsigned int res_y, const Scene* scene, bool printStats, bool showNormals);
};

#endif