#include "rm_renderer.cuh"

using namespace glm;

__device__ vec2 indexToXY(unsigned int index, unsigned int res_x, unsigned int res_y) {
	// unsigned int y = index / res_x;
	// unsigned int x = index - y * res_x;
	unsigned int baseline = (index / res_x) & 0xFFFFFFFC;
	unsigned int x, y;
	if (baseline > res_y - 4) {
		unsigned int h = res_y - baseline;
		x = (index - baseline * res_x) / h;
		y = baseline + (index % h);
	} else {
		x = (index - baseline * res_x) >> 2;
		y = baseline + (index & 0b11);
	}
	return vec2(x, y);
}

__device__ __forceinline__ vec3 applySpaceTransformers(const vec3& p, const unsigned int* scenePtr, unsigned int* sceneIndex,
									   unsigned int spaceTransformerCount) {
	vec3 p_prime = p;

	// For each space transformer, transform the input position
	for (int i=0; i<spaceTransformerCount; ++i) {
		// read next 4 space transformer ids
		unsigned int raw = scenePtr[(*sceneIndex)++];
		SpaceTransformerId stids[4] = {
			uint2SpaceTransformerId(raw >> 24),
			uint2SpaceTransformerId((raw >> 16) & 0xFF),
			uint2SpaceTransformerId((raw >> 8) & 0xFF),
			uint2SpaceTransformerId(raw & 0xFF),
		};
		for (int sti=0; sti<4; ++sti) {
			callOnSpaceTransformerId(stids[sti], [&]<typename ST>() {
				// Deserialize space transformer
				ST spaceTransformer = ST::deserialize(&scenePtr[*sceneIndex]);
				*sceneIndex += ST::serializedSize();

				// Apply the space transform
				p_prime = spaceTransformer.transform(p_prime);
			});
			if (++i == spaceTransformerCount) break;
		}
	}

	return p_prime;
}

__device__ __forceinline__ float applyDistanceTransformers(const vec3& p, float distance, const unsigned int* scenePtr,
										   unsigned int* sceneIndex, unsigned int distanceTransformerCount) {
	float d = distance;

	// For each distance transformer, transform the output distance
	for (int i=0; i<distanceTransformerCount; ++i) {
		// read next 4 distance transformer ids
		unsigned int raw = scenePtr[(*sceneIndex)++];
		DistanceTransformerId dtids[4] = {
			uint2DistanceTransformerId(raw >> 24),
			uint2DistanceTransformerId((raw >> 16) & 0xFF),
			uint2DistanceTransformerId((raw >> 8) & 0xFF),
			uint2DistanceTransformerId(raw & 0xFF),
		};
		for (int dti=0; dti<4; ++dti) {
			callOnDistanceTransformerId(dtids[dti], [&]<typename DT>() {
				// Deserialize distance transformer
				DT distanceTransformer = DT::deserialize(&scenePtr[*sceneIndex]);
				*sceneIndex += DT::serializedSize();

				// Apply the distance transform
				d = distanceTransformer.transform(p, d);
			});	
			if (++i == distanceTransformerCount) break;
		}
	}

	return d;
}

__device__ __forceinline__ float evalPrimitiveSDF(const vec3& p, const vec3& dir, bool enableDirectionalDistance,
												  unsigned int* materialIdx, const unsigned int* scenePtr, unsigned int* sceneIndex) {
	// Read primitive header
	PrimitiveHeader primitiveHeader = PrimitiveHeader::deserialize(scenePtr[(*sceneIndex)++]);
	*materialIdx = primitiveHeader.materialIdx;

	// Always start with linear transform
	LinearTransform linearTransform = LinearTransform::deserialize(&scenePtr[*sceneIndex]);
	*sceneIndex += LinearTransform::serializedSize();

	// Apply the linear transform
	vec3 p_prime = linearTransform.transform(p);
	
	p_prime = applySpaceTransformers(p_prime, scenePtr, sceneIndex, primitiveHeader.spaceTransformerCount);

	bool useDirDist = enableDirectionalDistance &&
					  primitiveHeader.spaceTransformerCount == 0 &&
					  primitiveHeader.distanceTransformerCount == 0; 
	float d;
	callOnPrimitiveId(primitiveHeader.id, [&]<typename P>() {
		// Deserialize primitive
		P primitive = P::deserialize(&scenePtr[*sceneIndex]);
		*sceneIndex += P::serializedSize();

		if (useDirDist) {
			vec3 dir_prime = linearTransform.dirTransform(dir);
			d = primitive.originDirectionalDistance(p_prime, dir_prime, DIRECTIONAL_DISTANCE_STEPS);
		} else {
			d = primitive.originSDF(p_prime);
		}
	});

	d = applyDistanceTransformers(p_prime, d, scenePtr, sceneIndex, primitiveHeader.distanceTransformerCount);

	return d;
}

__device__ __forceinline__ float evalCombinerSDF(const vec3& p, const vec3& dir, unsigned int* closestHit,
												  bool enableDirectionalDistance, const unsigned int* scenePtr, unsigned int* sceneIndex) {
	float distStack[MAX_COMBINER_CHILDREN]; // Hard set max number of child containers
	unsigned int hitStack[MAX_COMBINER_CHILDREN]; // Hard set max number of child containers
	unsigned int stackSize = 0;
	unsigned int baseCombinerIdx = *sceneIndex;

	while (true) {
		unsigned int combinerIdx = *sceneIndex;
		// Read combiner header
		CombinerHeader combinerHeader = CombinerHeader::deserialize(scenePtr[(*sceneIndex)++]);

		// apply parent space transformers
		vec3 p_prime = applySpaceTransformers(p, scenePtr, sceneIndex, combinerHeader.parentSpaceTransformerCount);

		// apply own linear transform
		LinearTransform linearTransform = LinearTransform::deserialize(&scenePtr[*sceneIndex]);
		*sceneIndex += LinearTransform::serializedSize();
		p_prime = linearTransform.transform(p_prime);
		vec3 dir_prime = linearTransform.dirTransform(dir);

		// Eval proxies
		bool allOutsideProxy = false;
		ProxyHeader proxyHeader(false, 0);
		if (combinerHeader.proxyCount >= 1) {
			for (int i=0; i<combinerHeader.proxyCount; ++i) {
				// Read proxy header
				proxyHeader = ProxyHeader::deserialize(scenePtr[(*sceneIndex)++]);

				// Eval proxy SDF
				unsigned int _matIdx; // Not used
				float d = evalPrimitiveSDF(p_prime, dir_prime, combinerHeader.parentSpaceTransformerCount == 0, &_matIdx, scenePtr, sceneIndex);

				// If outside the proxy for every wrap
				allOutsideProxy = __all_sync(__activemask(), d > PROXY_EPSILON);
				if (allOutsideProxy) {
					// We skip to skipIndex and return distance to proxy instead of evaluating the combiner
					*sceneIndex = proxyHeader.skipIndex;
					distStack[stackSize++] = d;
					break;
				}
			}
		}

		if (allOutsideProxy) {
			if (proxyHeader.isRoot) break;
			else continue;
		}
			
		// Apply own space transformers
		p_prime = applySpaceTransformers(p_prime, scenePtr, sceneIndex, combinerHeader.spaceTransformerCount);

		float accDist;
		unsigned int accHit;
		callOnCombinerId(combinerHeader.id, [&]<typename C> () {
			// Deserialize combiner
			C combiner = C::deserialize(&scenePtr[*sceneIndex]);
			*sceneIndex += C::serializedSize();

			// Instanciate combiner accumulator
			auto accumulator = combiner.getAccumulator();

			// For each child combiner, pop the distance stack and accumulate
			// for (int i=0; i<combinerHeader.childCombinerCount; ++i) {
			unsigned int a = combinerHeader.childCombinerCount;
			for (int i=0; i<a; ++i) {
				float d = distStack[--stackSize];
				unsigned int hit = hitStack[stackSize];
				accumulator.accumulate(d, hit);
			}
			
			// Very strict conditions to enable direction distance (better be safe)
			bool enablePrimitiveDirDist = enableDirectionalDistance &&
									      C::enableDirectionalDistance() &&
									      combinerHeader.parentSpaceTransformerCount == 0 &&
									      combinerHeader.spaceTransformerCount == 0 &&
									      combinerHeader.distanceTransformerCount == 0;
			// For each child primitive, evaluate it and accumulate
			for (int i=0; i<combinerHeader.childPrimitiveCount; ++i) {
				unsigned int matIdx;
				unsigned int primitiveIdx = *sceneIndex;
				float d = evalPrimitiveSDF(p_prime, dir_prime, enablePrimitiveDirDist, &matIdx, scenePtr, sceneIndex);
				
				unsigned int hit = 0;
 				// Technically correct, but hurts performance without proper handling
				// if (C::enablePrimitiveHit() && combinerHeader.isRoot) {
				if (C::enablePrimitiveHit()) {
					setHitIsPrimitive(hit, true);
					setHitStartIdx(hit, combinerIdx);
					setHitEndIdx(hit, primitiveIdx - combinerIdx);
				} else {
					setHitIsPrimitive(hit, false);
					setHitStartIdx(hit, baseCombinerIdx);
					setHitEndIdx(hit, combinerIdx - baseCombinerIdx);
				}
				setHitMaterialIdx(hit, matIdx);

				// Accumulate the evaluated distance
				accumulator.accumulate(d, hit);
			}

			accDist = accumulator.getDistance();
			accHit = accumulator.getHit();
		});

		// Apply distance Transformers
		accDist = applyDistanceTransformers(p_prime, accDist, scenePtr, sceneIndex, combinerHeader.distanceTransformerCount);

		// Push accumulated distance to the stack
		distStack[stackSize] = accDist;
		hitStack[stackSize++] = accHit;

		// Break if it was a root container
		if (combinerHeader.isRoot) break;
	}

	*closestHit = hitStack[0];
	return distStack[0];
}

__device__ __forceinline__ float evalSceneSDF(const vec3& p, const vec3& dir, unsigned int* closestHit,
											  bool enableDirectionalDistance, const unsigned int* scenePtr) {
	float minDist = MAXFLOAT;
	unsigned int sceneIndex = 0;

	while (true) {
		// Read combiner header
		CombinerHeader combinerHeader = CombinerHeader::deserialize(scenePtr[sceneIndex]);

		// Break if end of the scene
		if (combinerHeader.id == CombinerId::End) break;

		// Else evaluate the container SDF and take union with the rest of the scene
		// This is to enable an arbitrary number of containers as children of the scene,
		// because the number of containers that are children of other containers is limited.
		unsigned int hit;
		float d = evalCombinerSDF(p, dir, &hit, enableDirectionalDistance, scenePtr, &sceneIndex);
		if (d < minDist) {
			minDist = d;
			*closestHit = hit;
		}
	}

	return minDist;
}

__global__ void initPrimaryRayPool(Pool<GeometryRay> rayPool,
								   const unsigned int res_x,  const unsigned int res_y,
      							   unsigned int* cameraPtr) {
	__shared__ Camera camera;

	if (threadIdx.x + threadIdx.y == 0) {
		camera = Camera::deserialize(cameraPtr);
	}
	__syncthreads();

	unsigned int poolSize = res_x * res_y;
	unsigned int blockPoolSize = (poolSize - 1) / gridDim.x + 1;
	unsigned int startIndex = blockPoolSize * blockIdx.x;
	unsigned int endIndex = min(startIndex + blockPoolSize, poolSize);

	for (int i=0;;++i) {
		unsigned int index = startIndex + blockDim.x * (i * blockDim.y + threadIdx.y) + threadIdx.x;
		if (index >= endIndex) break;

		vec2 xy = indexToXY(index, res_x, res_y);

		float u = (2.0f * xy.x - res_x) / (float)res_y;
		float v = (2.0f * xy.y - res_y) / (float)res_y;

		vec3 ro = camera.getPosition();
		vec3 rd = camera.getOrientation() * normalize(vec3(u, v, -camera.getFovy()));

		rayPool.write(GeometryRay(ro, rd, camera.getFar()), index);
	}
}

__global__ void marchPrimaryRays(Pool<GeometryRay> rayPool, 
								 unsigned int* nextRayIndex,
								 unsigned int* scene_d, unsigned int sceneSize) {
	extern __shared__ unsigned int scenePtr[]; 
	for (int i=0;;++i) {
		unsigned int index = blockDim.x * (i * blockDim.y + threadIdx.y) + threadIdx.x;
		if (index >= sceneSize) break;
		scenePtr[index] = scene_d[index];
	}
	__syncthreads();

	__shared__ volatile unsigned int warpNextRayIndices[32];
	unsigned int currentIndex;
	unsigned int steps = 0;
	bool active = true;

	GeometryRay ray(vec3(0.0), vec3(0.0), 0.0f);
	GeometryRay minRay(vec3(0.0), vec3(0.0), 0.0f);

	bool terminated = true;
	while (true) {
		unsigned int terminatedMask = __ballot_sync(0xffffffff, terminated);
		unsigned int terminatedCount = bitCount(terminatedMask);
		if (terminatedCount >= INACTIVE_THREADS_THRESHOLD) {
			if (terminated && active) {
				unsigned int terminatedID = bitCount(terminatedMask << (warpSize - threadIdx.x));
				if (terminatedID == 0) {
					warpNextRayIndices[threadIdx.y] = atomicAdd(nextRayIndex, terminatedCount);
				}

				if (ray.length < minRay.length) {
					rayPool.writeMember(ray, currentIndex, &ray.length, offsetof(GeometryRay, length), sizeof(ray.length));
					rayPool.writeMember(ray, currentIndex, &ray.hit, offsetof(GeometryRay, hit), sizeof(ray.hit));
				}

				currentIndex = warpNextRayIndices[threadIdx.y] + terminatedID; 
				if (currentIndex >= rayPool.size()) {
					active = false;
				} else {
					minRay = rayPool.read(currentIndex);
					ray = GeometryRay(minRay.origin, minRay.direction, 0.0f);
					steps = 0;
					terminated = false;
				}
			}
		}
		__syncwarp();

		bool stillOneActive =  __any_sync(0xffffffff, active);
		if (!stillOneActive) break;

		for (int i=0; i<RAYMARCH_UNROLL; ++i) {
			float r = evalSceneSDF(ray.pos(), ray.direction, &ray.hit, true, scenePtr);
			terminated |= abs(r) < EPSILON;
			if (!terminated) {
				ray.march(r);
				++steps;
			}
			terminated |= ray.length > minRay.length || steps >= MAX_STEPS;
		}
	}
}

__global__ void computeNormals(Pool<GeometryRay> rayPool, Pool<vec3> normalPool, Pool<vec3> primitiveSpaceHitPosPool, 
							   Pool<vec3> primitiveSpaceNormalPool, unsigned int* nextRayIndex, unsigned int* cameraPtr,
							   unsigned int* scene_d, unsigned int sceneSize) {
	extern __shared__ unsigned int scenePtr[]; 
	__shared__ Camera camera;

	if (threadIdx.x + threadIdx.y == 0) {
		camera = Camera::deserialize(cameraPtr);
	}

	for (int i=0;;++i) {
		unsigned int index = blockDim.x * (i * blockDim.y + threadIdx.y) + threadIdx.x;
		if (index >= sceneSize) break;
		scenePtr[index] = scene_d[index];
	}
	__syncthreads();

	__shared__ volatile unsigned int warpNextRayIndices[32];
	unsigned int currentIndex;
	bool active = true;

	GeometryRay hitRay;

	while (true) {
		if (threadIdx.x == 0) {
			warpNextRayIndices[threadIdx.y] = atomicAdd(nextRayIndex, warpSize);
		}

		currentIndex = warpNextRayIndices[threadIdx.y] + threadIdx.x; 
		if (currentIndex >= rayPool.size()) {
			active = false;
		} else {
			hitRay = rayPool.read(currentIndex);
		}

		bool stillOneActive =  __any_sync(0xffffffff, active);
		if (!stillOneActive) break;

		vec3 normal;
		vec3 p_prime;
		vec3 primitiveSpaceNormal;
		if (hitRay.length < camera.getFar()) {
			// Tetrahedron technique
			const float resolution = EPSILON;
			const vec3 k0(1.0, -1.0, -1.0);
			const vec3 k1(-1.0, 1.0, -1.0);
			const vec3 k2(-1.0, -1.0, 1.0);
			const vec3 k3(1.0, 1.0, 1.0);
			p_prime = hitRay.pos();
			vec3 p0 = hitRay.pos() + resolution * k0;
			vec3 p1 = hitRay.pos() + resolution * k1;
			vec3 p2 = hitRay.pos() + resolution * k2;
			vec3 p3 = hitRay.pos() + resolution * k3;
			float d0, d1, d2, d3;
			if (getHitIsPrimitive(hitRay.hit)) {
				unsigned int sceneIndex = getHitStartIdx(hitRay.hit);

				// Read combiner header
				CombinerHeader combinerHeader = CombinerHeader::deserialize(scenePtr[sceneIndex++]);

				// For each space transformer, transform the input position
				for (int i=0; i<combinerHeader.parentSpaceTransformerCount; ++i) {
					// read next 4 space transformer ids
					unsigned int raw = scenePtr[sceneIndex++];
					SpaceTransformerId stids[4] = {
						uint2SpaceTransformerId(raw >> 24),
						uint2SpaceTransformerId((raw >> 16) & 0xFF),
						uint2SpaceTransformerId((raw >> 8) & 0xFF),
						uint2SpaceTransformerId(raw & 0xFF),
					};
					for (int sti=0; sti<4; ++sti) {
						callOnSpaceTransformerId(stids[sti], [&]<typename ST>() {
							// Deserialize space transformer
							ST spaceTransformer = ST::deserialize(&scenePtr[sceneIndex]);
							sceneIndex += ST::serializedSize();

							// Apply the space transform
							p_prime = spaceTransformer.transform(p_prime);
							p0 = spaceTransformer.transform(p0);
							p1 = spaceTransformer.transform(p1);
							p2 = spaceTransformer.transform(p2);
							p3 = spaceTransformer.transform(p3);
						});
						if (++i == combinerHeader.parentSpaceTransformerCount) break;
					}
				}

				// apply own linear transform
				LinearTransform linearTransform = LinearTransform::deserialize(&scenePtr[sceneIndex]);
				sceneIndex += LinearTransform::serializedSize();
				p_prime = linearTransform.transform(p_prime);
				p0 = linearTransform.transform(p0);
				p1 = linearTransform.transform(p1);
				p2 = linearTransform.transform(p2);
				p3 = linearTransform.transform(p3);

				// Skip proxies
				if (combinerHeader.proxyCount >= 1) {
					for (int i=0; i<combinerHeader.proxyCount; ++i) {
						++sceneIndex;
						PrimitiveHeader primitiveHeader = PrimitiveHeader::deserialize(scenePtr[sceneIndex++]);
						sceneIndex += LinearTransform::serializedSize() + 1;
						
						for (int i=0; i<primitiveHeader.spaceTransformerCount; ++i) {
							// read next 4 space transformer ids
							unsigned int raw = scenePtr[sceneIndex++];
							SpaceTransformerId stids[4] = {
								uint2SpaceTransformerId(raw >> 24),
								uint2SpaceTransformerId((raw >> 16) & 0xFF),
								uint2SpaceTransformerId((raw >> 8) & 0xFF),
								uint2SpaceTransformerId(raw & 0xFF),
							};
							for (int sti=0; sti<4; ++sti) {
								callOnSpaceTransformerId(stids[sti], [&]<typename ST>() {
									sceneIndex += ST::serializedSize() + 1;
								});
								if (++i == primitiveHeader.spaceTransformerCount) break;
							}
						}

						callOnPrimitiveId(primitiveHeader.id, [&]<typename P>() {
							sceneIndex += P::serializedSize() + 1;
						});

						for (int i=0; i<primitiveHeader.distanceTransformerCount; ++i) {
							// read next 4 distance transformer ids
							unsigned int raw = scenePtr[sceneIndex++];
							DistanceTransformerId dtids[4] = {
								uint2DistanceTransformerId(raw >> 24),
								uint2DistanceTransformerId((raw >> 16) & 0xFF),
								uint2DistanceTransformerId((raw >> 8) & 0xFF),
								uint2DistanceTransformerId(raw & 0xFF),
							};
							for (int dti=0; dti<4; ++dti) {
								callOnDistanceTransformerId(dtids[dti], [&]<typename DT>() {
									sceneIndex += DT::serializedSize() + 1;
								});
								if (++i == primitiveHeader.distanceTransformerCount) break;
							}
						}
					}
				}
					
				for (int i=0; i<combinerHeader.spaceTransformerCount; ++i) {
					// read next 4 space transformer ids
					unsigned int raw = scenePtr[sceneIndex++];
					SpaceTransformerId stids[4] = {
						uint2SpaceTransformerId(raw >> 24),
						uint2SpaceTransformerId((raw >> 16) & 0xFF),
						uint2SpaceTransformerId((raw >> 8) & 0xFF),
						uint2SpaceTransformerId(raw & 0xFF),
					};
					for (int sti=0; sti<4; ++sti) {
						callOnSpaceTransformerId(stids[sti], [&]<typename ST>() {
							// Deserialize space transformer
							ST spaceTransformer = ST::deserialize(&scenePtr[sceneIndex]);
							sceneIndex += ST::serializedSize();

							// Apply the space transform
							p_prime = spaceTransformer.transform(p_prime);
							p0 = spaceTransformer.transform(p0);
							p1 = spaceTransformer.transform(p1);
							p2 = spaceTransformer.transform(p2);
							p3 = spaceTransformer.transform(p3);
						});
						if (++i == combinerHeader.spaceTransformerCount) break;
					}
				}

				// Jump to hit primitive
				sceneIndex = getHitStartIdx(hitRay.hit) + getHitEndIdx(hitRay.hit);

				// Read primitive header
				PrimitiveHeader primitiveHeader = PrimitiveHeader::deserialize(scenePtr[sceneIndex++]);

				// Always start with linear transform
				linearTransform = LinearTransform::deserialize(&scenePtr[sceneIndex]);
				sceneIndex += LinearTransform::serializedSize();

				// Apply the linear transform
				p_prime = linearTransform.transform(p_prime);
				p0 = linearTransform.transform(p0);
				p1 = linearTransform.transform(p1);
				p2 = linearTransform.transform(p2);
				p3 = linearTransform.transform(p3);
				
				for (int i=0; i<primitiveHeader.spaceTransformerCount; ++i) {
					// read next 4 space transformer ids
					unsigned int raw = scenePtr[sceneIndex++];
					SpaceTransformerId stids[4] = {
						uint2SpaceTransformerId(raw >> 24),
						uint2SpaceTransformerId((raw >> 16) & 0xFF),
						uint2SpaceTransformerId((raw >> 8) & 0xFF),
						uint2SpaceTransformerId(raw & 0xFF),
					};
					for (int sti=0; sti<4; ++sti) {
						callOnSpaceTransformerId(stids[sti], [&]<typename ST>() {
							// Deserialize space transformer
							ST spaceTransformer = ST::deserialize(&scenePtr[sceneIndex]);
							sceneIndex += ST::serializedSize();

							// Apply the space transform
							p_prime = spaceTransformer.transform(p_prime);
							p0 = spaceTransformer.transform(p0);
							p1 = spaceTransformer.transform(p1);
							p2 = spaceTransformer.transform(p2);
							p3 = spaceTransformer.transform(p3);
						});
						if (++i == primitiveHeader.spaceTransformerCount) break;
					}
				}

				callOnPrimitiveId(primitiveHeader.id, [&]<typename P>() {
					// Deserialize primitive
					P primitive = P::deserialize(&scenePtr[sceneIndex]);
					sceneIndex += P::serializedSize();

					d0 = primitive.originSDF(p0);
					d1 = primitive.originSDF(p1);
					d2 = primitive.originSDF(p2);
					d3 = primitive.originSDF(p3);
				});

				for (int i=0; i<primitiveHeader.distanceTransformerCount; ++i) {
					// read next 4 space transformer ids
					unsigned int raw = scenePtr[sceneIndex++];
					DistanceTransformerId dtids[4] = {
						uint2DistanceTransformerId(raw >> 24),
						uint2DistanceTransformerId((raw >> 16) & 0xFF),
						uint2DistanceTransformerId((raw >> 8) & 0xFF),
						uint2DistanceTransformerId(raw & 0xFF),
					};
					for (int dti=0; dti<4; ++dti) {
						callOnDistanceTransformerId(dtids[dti], [&]<typename DT>() {
							// Deserialize distance transformer
							DT distanceTransformer = DT::deserialize(&scenePtr[sceneIndex]);
							sceneIndex += DT::serializedSize();

							// Apply the space transform
							d0 = distanceTransformer.transform(p0, d0);
							d1 = distanceTransformer.transform(p1, d1);
							d2 = distanceTransformer.transform(p2, d2);
							d3 = distanceTransformer.transform(p3, d3);
						});
						if (++i == primitiveHeader.distanceTransformerCount) break;
					}
				}
			} else {
				unsigned int sceneIndex = getHitStartIdx(hitRay.hit);
				unsigned int _hit; // unused
				unsigned int tmpSceneIndex = sceneIndex;
				d0 = evalCombinerSDF(p0, vec3(0.0f), &_hit, false, scenePtr, &tmpSceneIndex);
				tmpSceneIndex = sceneIndex;
				d1 = evalCombinerSDF(p1, vec3(0.0f), &_hit, false, scenePtr, &tmpSceneIndex);
				tmpSceneIndex = sceneIndex;
				d2 = evalCombinerSDF(p2, vec3(0.0f), &_hit, false, scenePtr, &tmpSceneIndex);
				tmpSceneIndex = sceneIndex;
				d3 = evalCombinerSDF(p3, vec3(0.0f), &_hit, false, scenePtr, &tmpSceneIndex);
			}

			normal = normalize(k0 * d0 + k1 * d1 + k2 * d2 + k3 * d3);
			vec3 spk0 = p0 - p_prime;
			vec3 spk1 = p1 - p_prime;
			vec3 spk2 = p2 - p_prime;
			vec3 spk3 = p3 - p_prime;
			primitiveSpaceNormal = normalize(spk0 * d0 + spk1 * d1 + spk2 * d2 + spk3 * d3);
		} else {
			normal = vec3(0.0f);
			p_prime = hitRay.pos();
			primitiveSpaceNormal = vec3(0.0f);
		}
		__syncwarp();

		if (active) {
			normalPool.write(normal, currentIndex);
			primitiveSpaceHitPosPool.write(p_prime, currentIndex);
			primitiveSpaceNormalPool.write(primitiveSpaceNormal, currentIndex);
		}
	}
}

__device__ __forceinline__ float sampleTriPlanar(cudaTextureObject_t texObj,
												 const vec3& p, const vec3& n,
												float scale, float normalWeight) {
	float nx = pow(abs(n.x), normalWeight);
	float ny = pow(abs(n.y), normalWeight);
	float nz = pow(abs(n.z), normalWeight);
	vec3 sp = p / scale;
	float txy = tex2D<float>(texObj, sp.x, sp.y);
	float txz = tex2D<float>(texObj, sp.x, sp.z);
	float tyz = tex2D<float>(texObj, sp.y, sp.z);
	return (nx * tyz + ny * txz + nz * txy) / (nx + ny + nz);
}

__device__ __forceinline__ vec3 sampleTriPlanarRGB(cudaTextureObject_t texObj,
												   const vec3& p, const vec3& n,
												   float scale, float normalWeight) {
	float nx = pow(abs(n.x), normalWeight);
	float ny = pow(abs(n.y), normalWeight);
	float nz = pow(abs(n.z), normalWeight);
	vec3 sp = p / scale;
	float4 txy = tex2D<float4>(texObj, sp.x, sp.y);
	float4 txz = tex2D<float4>(texObj, sp.x, sp.z);
	float4 tyz = tex2D<float4>(texObj, sp.y, sp.z);
	float invSum = 1.0f / (nx + ny + nz);
	return vec3(
		(nx * tyz.x + ny * txz.x + nz * txy.x) * invSum,
		(nx * tyz.y + ny * txz.y + nz * txy.y) * invSum,
		(nx * tyz.z + ny * txz.z + nz * txy.z) * invSum);
}

__device__ __forceinline__ vec3 computePBShading(const Material& material, const vec3& wi, const vec3& wo,
												 const vec3& n, const vec3& Li, float penumbraFactor,
												 const vec3& texPoint, const vec3& texNormal) {
	vec3 albedo;
	float metalness;
	float roughness;

	if (material.albedoTex == 0) {
		albedo = material.albedo;
	} else {
		albedo = sampleTriPlanarRGB(material.albedoTex, texPoint, texNormal, material.textureScale, material.triPlanarNormalWeight);
	}
	if (material.metalnessTex == 0) {
		metalness = material.metalness;
	} else {
		metalness = sampleTriPlanar(material.metalnessTex, texPoint, texNormal, material.textureScale, material.triPlanarNormalWeight);
	}
	if (material.roughnessTex == 0) {
		roughness = material.roughness;
	} else {
		roughness = sampleTriPlanar(material.roughnessTex, texPoint, texNormal, material.textureScale, material.triPlanarNormalWeight);
	}

	vec3 h = normalize(wi + wo);
	float squaredRoughness = roughness * roughness;
	float n_dot_h = dot(n, h);
	float tmp = n_dot_h * n_dot_h * (squaredRoughness - 1.0f) + 1.0f;
	float D = squaredRoughness / (M_PIf * tmp * tmp);
	float n_dot_wi = dot(n, wi);
	float n_dot_wo = dot(n, wo);
	tmp = roughness + 1.0f; 
	float k = tmp * tmp * 0.125f;
	float G = n_dot_wo * n_dot_wi / (((1.0f - k) * n_dot_wo + k) * ((1.0f - k) * n_dot_wi + k));
	tmp = 1.0f - dot(h, wo); 
	float tmp2 = tmp * tmp;
	float tmp5 = tmp2 * tmp2 * tmp;
	vec3 F0 = 0.04f * (1.0f - metalness) + albedo * metalness;
	vec3 F = F0 + (1.0f - F0) * tmp5;
	vec3 fCookTorrance = D * G * F / (4.0f * n_dot_wo * n_dot_wi);
	vec3 fLambert = albedo / M_PIf;
	vec3 brdf = (1.0f - F) * (1.0f - metalness) * fLambert + fCookTorrance;
	vec3 Lo = brdf * Li * max(n_dot_wi, 0.0f);
	vec3 color = penumbraFactor * Lo;
	return color;
}

__global__ void initColorPool(Pool<vec3> colorPool) {
	// Clear colorPool
	unsigned int poolSize = colorPool.size();
	unsigned int blockPoolSize = (poolSize - 1) / gridDim.x + 1;
	unsigned int startIndex = blockPoolSize * blockIdx.x;
	unsigned int endIndex = min(startIndex + blockPoolSize, poolSize);

	for (int i=0;;++i) {
		unsigned int index = startIndex + blockDim.x * (i * blockDim.y + threadIdx.y) + threadIdx.x;
		if (index >= endIndex) break;
		colorPool.write(vec3(0.0f), index);
	}
}

__global__ void shadeLights(Pool<GeometryRay> rayPool, Pool<vec3> normalPool, Pool<vec3> colorPool,
							Pool<vec3> primitiveSpaceHitPosPool, Pool<vec3> primitiveSpaceNormalPool,
							unsigned int* lights_d, unsigned int lightsCount, unsigned int lightsSize,
							unsigned int* nextRayIndex, unsigned int* scene_d, unsigned int sceneSize,
      						unsigned int* cameraPtr, Material* materials_d, unsigned int materialsSize) {
	__shared__ Camera camera;

	if (threadIdx.x + threadIdx.y == 0) {
		camera = Camera::deserialize(cameraPtr);
	}

	// Load geometry and lights in shared memory
	extern __shared__ unsigned int scenePtr[]; 
	for (int i=0;;++i) {
		unsigned int index = blockDim.x * (i * blockDim.y + threadIdx.y) + threadIdx.x;
		if (index < sceneSize) {
			scenePtr[index] = scene_d[index];
		} else if (index < sceneSize + lightsSize) {
			scenePtr[index] = lights_d[index - sceneSize];
		} else if (index < sceneSize + lightsSize + materialsSize) {
			scenePtr[index] = ((unsigned int*) materials_d)[index - sceneSize - lightsSize];
		} else {
			break;
		}
	}
	__syncthreads();

	Material* sharedMaterials = (Material*) &(scenePtr[sceneSize + lightsSize]);

	__shared__ volatile unsigned int warpNextRayIndices[32];
	unsigned int currentRayIndex;
	int currentLightIndex = -1;
	unsigned int currentLightSceneIndex = sceneSize;
	unsigned int steps = 0;
	bool active = true;

	GeometryRay hitRay;
	ShadowRay shadowRay;
	vec3 Li;
	vec3 n;
	float lightSoftness;

	bool terminated = true;
	while (true) {
		unsigned int terminatedMask = __ballot_sync(0xffffffff, terminated);
		unsigned int terminatedCount = bitCount(terminatedMask);
		if (terminatedCount >= INACTIVE_THREADS_THRESHOLD) {
			if (terminated && active) {
				unsigned int terminatedID = bitCount(terminatedMask << (warpSize - threadIdx.x));
				if (terminatedID == 0) {
					warpNextRayIndices[threadIdx.y] = atomicAdd(nextRayIndex, terminatedCount);
				}

				// Shade point
				unsigned int rayIndex = currentRayIndex % rayPool.size();
				if (currentLightIndex != -1) {
					if (hitRay.length < 100.0f) {
						unsigned int materialIdx = getHitMaterialIdx(hitRay.hit);
						vec3 primitiveSpaceHitPos = primitiveSpaceHitPosPool.read(rayIndex);
						vec3 primitiveSpaceNormal = primitiveSpaceNormalPool.read(rayIndex);
						vec3 shading = computePBShading(sharedMaterials[materialIdx], shadowRay.direction, -hitRay.direction,
														n, Li, shadowRay.minPenumbraFactor, primitiveSpaceHitPos, primitiveSpaceNormal);
						vec3 color = colorPool.read(rayIndex);
						color += shading;
						colorPool.write(color, rayIndex);
					} else {
						colorPool.write(vec3(0.0f), rayIndex);
					}
				}

				currentRayIndex = warpNextRayIndices[threadIdx.y] + terminatedID; 
				rayIndex = currentRayIndex % rayPool.size();
				if (currentRayIndex >= rayPool.size() * lightsCount) {
					active = false;
				} else {
					// Update light index
					if (currentLightIndex == -1) currentLightIndex = 0;
					unsigned int lightIndex = currentRayIndex / rayPool.size();
					LightHeader lightHeader = LightHeader::deserialize(scenePtr[currentLightSceneIndex]);
					while (currentLightIndex < lightIndex) {
						callOnLightId(lightHeader.id, [&]<typename L> () {
							currentLightSceneIndex += L::serializedSize() + 1;
						});
						lightHeader = LightHeader::deserialize(scenePtr[currentLightSceneIndex]);
						++currentLightIndex;
					}
					// Read hit ray
					hitRay = rayPool.read(rayIndex);
					n = normalPool.read(rayIndex);

					// Sample light to build shadow ray
					vec3 wi;
					float lightDist;
					callOnLightId(lightHeader.id, [&]<typename L> () {
						L light = L::deserialize(&scenePtr[currentLightSceneIndex+1]);
						light.sampleLi(hitRay.pos(), &Li, &wi, &lightDist);
						lightSoftness = light.getSoftness();
					});
					shadowRay = ShadowRay(hitRay.pos() + n * SHADOW_RAY_NORMAL_OFFSET, wi, 0.0f, lightDist);
					steps = 0;
					terminated = false;
				}
			}
		}
		__syncwarp();

		bool stillOneActive =  __any_sync(0xffffffff, active);
		if (!stillOneActive) break;

		for (int i=0; i<RAYMARCH_UNROLL; ++i) {
			unsigned int _hit; // unused;
			// Disable directional distance to get accurate penumbra factor
			float r = evalSceneSDF(shadowRay.pos(), shadowRay.direction, &_hit, false, scenePtr);
			shadowRay.updateMinPenumbraFactor(lightSoftness, max(0.0f, r - EPSILON));
			terminated |= abs(r) < EPSILON;
			if (!terminated) {
				r = min(shadowRay.lightDistance - shadowRay.length, r);
				shadowRay.march(r);
				++steps;
			}
			terminated |= shadowRay.length > shadowRay.lightDistance - EPSILON ||
						  length(shadowRay.pos() - camera.getPosition()) > camera.getFar() ||
						  steps >= MAX_STEPS;
		}
	}
}

__global__ void composeFrame(Pool<vec3> colorPool, cudaSurfaceObject_t renderSurface,
							const unsigned int res_x,  const unsigned int res_y) {
	unsigned int poolSize = res_x * res_y;
	unsigned int blockPoolSize = (poolSize - 1) / gridDim.x + 1; // round up
	unsigned int startIndex = blockPoolSize * blockIdx.x;
	unsigned int endIndex = min(startIndex + blockPoolSize, poolSize);

	for (int i=0;;++i) {
		unsigned int index = startIndex + blockDim.x * (i * blockDim.y + threadIdx.y) + threadIdx.x;
		if (index >= endIndex) break;

		vec2 xy = indexToXY(index, res_x, res_y);

		vec3 color = colorPool.read(index);
		float4 value = make_float4(color.r, color.g, color.b, 1.0f);

		surf2Dwrite(value, renderSurface, xy.x * sizeof(float4), xy.y);
	}
}

RMRenderer::RMRenderer() {}

void RMRenderer::registerRBO(GLuint rbo, unsigned int res_x, unsigned int res_y) {
	if (m_cudarbo != nullptr) unregisterRBO();
	checkCudaErrors(cudaGraphicsGLRegisterImage(&m_cudarbo, rbo, GL_RENDERBUFFER, cudaGraphicsRegisterFlagsSurfaceLoadStore));

	checkCudaErrors(cudaGraphicsMapResources(1, &m_cudarbo, NULL));

	cudaArray_t array;
	checkCudaErrors(cudaGraphicsSubResourceGetMappedArray(&array, m_cudarbo, 0, 0));

	cudaResourceDesc resDesc;
	resDesc.resType = cudaResourceTypeArray;
	resDesc.res.array.array = array;
	checkCudaErrors(cudaCreateSurfaceObject(&m_surface, &resDesc));

    m_rayPool.allocate(res_x * res_y);
    m_normalPool.allocate(res_x * res_y);
    m_colorPool.allocate(res_x * res_y);
    m_primitiveSpaceHitPosPool.allocate(res_x * res_y);
	m_primitiveSpaceNormalPool.allocate(res_x * res_y);
}

void RMRenderer::unregisterRBO() {
	cudaDestroySurfaceObject(m_surface);
	checkCudaErrors(cudaGraphicsUnmapResources(1, &m_cudarbo, NULL));
	checkCudaErrors(cudaGraphicsUnregisterResource(m_cudarbo));
	m_cudarbo = NULL;
	m_rayPool.free();
	m_normalPool.free();
	m_colorPool.free();
	m_primitiveSpaceHitPosPool.free();
	m_primitiveSpaceNormalPool.free();
}

void RMRenderer::render(unsigned int res_x, unsigned int res_y, const Scene* scene, bool printStats, bool showNormals) {
	double t0 = glfwGetTime();

	unsigned int* camera_d;
	unsigned int* geometry_d;
	unsigned int geometrySize;
	unsigned int* lights_d;
	unsigned int lightsSize;
	unsigned int lightsCount;
	Material* materials_d;
	unsigned int materialsSize;
	scene->sendToDevice(&camera_d, &geometry_d, &geometrySize, &lights_d, &lightsSize, &lightsCount, &materials_d, &materialsSize);

	unsigned int* nextRayIndex;
    cudaMalloc(&nextRayIndex, sizeof(unsigned int));
	const unsigned int firstRayIndex = 0;
    cudaMemcpy(nextRayIndex, &firstRayIndex, sizeof(unsigned int), cudaMemcpyHostToDevice);

	dim3 threadsPerBlock(32, 32);
	const int smCount = 36;

	double t1 = glfwGetTime();

	initPrimaryRayPool<<<smCount, threadsPerBlock>>>(m_rayPool, res_x, res_y, camera_d);
	checkCudaErrors(cudaDeviceSynchronize());

	double t2 = glfwGetTime();

	marchPrimaryRays<<<smCount, threadsPerBlock, geometrySize * sizeof(unsigned int)>>>(m_rayPool, nextRayIndex, geometry_d, geometrySize);
	checkCudaErrors(cudaDeviceSynchronize());

	double t3 = glfwGetTime();

    cudaMemcpy(nextRayIndex, &firstRayIndex, sizeof(unsigned int), cudaMemcpyHostToDevice);

	computeNormals<<<smCount, threadsPerBlock, geometrySize * sizeof(unsigned int)>>>(
		m_rayPool, m_normalPool, m_primitiveSpaceHitPosPool, m_primitiveSpaceNormalPool, nextRayIndex, camera_d, geometry_d, geometrySize);
	checkCudaErrors(cudaDeviceSynchronize());

	double t4 = glfwGetTime();

	initColorPool<<<smCount, threadsPerBlock>>>(m_colorPool);
	checkCudaErrors(cudaDeviceSynchronize());

    cudaMemcpy(nextRayIndex, &firstRayIndex, sizeof(unsigned int), cudaMemcpyHostToDevice);

	shadeLights<<<smCount, threadsPerBlock, (geometrySize + lightsSize + materialsSize) * sizeof(unsigned int)>>>(
		m_rayPool, m_normalPool, m_colorPool, m_primitiveSpaceHitPosPool, m_primitiveSpaceNormalPool, lights_d,
		lightsCount, lightsSize, nextRayIndex, geometry_d, geometrySize, camera_d, materials_d, materialsSize);
	checkCudaErrors(cudaDeviceSynchronize());

	double t5 = glfwGetTime();

	if (showNormals) {
		composeFrame<<<smCount, threadsPerBlock>>>(m_normalPool, m_surface, res_x, res_y);
		checkCudaErrors(cudaDeviceSynchronize());
	} else {
		composeFrame<<<smCount, threadsPerBlock>>>(m_colorPool, m_surface, res_x, res_y);
		checkCudaErrors(cudaDeviceSynchronize());
	}

	double t6 = glfwGetTime();

    cudaFree(camera_d);
    cudaFree(geometry_d);
    cudaFree(lights_d);
    cudaFree(materials_d);
	cudaFree(nextRayIndex);

	if (printStats) {
		std::cout << "setup: " << ((t1 - t0) * 1000.0) << " ms | ";
		std::cout << "initPool: " << ((t2 - t1) * 1000.0) << " ms | ";
		std::cout << "marchRays: " << ((t3 - t2) * 1000.0) << " ms | ";
		std::cout << "normals: " << ((t4 - t3) * 1000.0) << " ms | ";
		std::cout << "shade: " << ((t5 - t4) * 1000.0) << " ms | ";
		std::cout << "render: " << ((t6 - t5) * 1000.0) << " ms | ";
		std::cout << "total: " << ((t6 - t0) * 1000.0) << " ms" << std::endl;
	}
}
