#ifndef VERTICAL_DISPLACEMENT_MAP_h
#define VERTICAL_DISPLACEMENT_MAP_h

#include "distance_transformer.cuh"

class VerticalDisplacementMap : public DistanceTransformer<VerticalDisplacementMap> {
protected:
    unsigned int m_texObj;
	float m_textureScale = 1.0f;
	float m_maxDisplacement;
public:
	__host__ __device__ static DistanceTransformerId id() {
        return DistanceTransformerId::VerticalDisplacementMap;
    }
	__host__ __device__ VerticalDisplacementMap() = default;
	__host__ __device__ VerticalDisplacementMap(cudaTextureObject_t texObj, float textureScale, float maxDisplacement) :
		m_texObj(texObj), m_textureScale(textureScale), m_maxDisplacement(maxDisplacement) {}
	__host__ __device__ cudaTextureObject_t getTexObj() const { return m_texObj; }
	__host__ __device__ void setTexObj(cudaTextureObject_t texObj) { m_texObj = texObj; }
	__host__ __device__ float getTextureScale() const { return m_textureScale; }
	__host__ __device__ void setTextureScale(float textureScale) { m_textureScale = textureScale; }
	__host__ __device__ float getMaxDisplacement() const { return m_maxDisplacement; }
	__host__ __device__ void setMaxDisplacement(float maxDisplacement) { m_maxDisplacement = maxDisplacement; }

	__device__ float transform(const glm::vec3& p, float d) {
		if (d >= m_maxDisplacement + 0.05f) return d - m_maxDisplacement;
		float invScale = 1.0f / m_textureScale;
		float u = p.x * invScale;
		float v = p.z * invScale;
		float value = tex2D<float>(m_texObj, u, v);
		return (d - value * m_maxDisplacement) * 0.5f;
	}
};

#endif