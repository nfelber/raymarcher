#ifndef DISTANCE_TRANSFORMER_h
#define DISTANCE_TRANSFORMER_h

#include "../scene.cuh"

enum class DistanceTransformerId : uint8_t {
	Round,
	VerticalDisplacementMap,
	SphericalDisplacementMap,
};

template <typename T>
class DistanceTransformer : public Serializable<T> {
public:
	__host__ __device__ static DistanceTransformerId id() {
        return T::id();
    }

	__device__ float transform(const glm::vec3& p, float d) {
        return static_cast<T*>(this)->transform(p, d);
    }
};

#include "round.cuh"
#include "vertical_displacement_map.cuh"
#include "spherical_displacement_map.cuh"

#define uint2DistanceTransformerId(value) static_cast<DistanceTransformerId>(value)
#define distanceTransformerId2uint(value) static_cast<unsigned int>(value)

template<class F> __host__ __device__ void callOnDistanceTransformerId(DistanceTransformerId id, F f) {
	switch (id) {
		case DistanceTransformerId::Round:
			return f.template operator()<Round>();
		case DistanceTransformerId::VerticalDisplacementMap:
			return f.template operator()<VerticalDisplacementMap>();
		case DistanceTransformerId::SphericalDisplacementMap:
			return f.template operator()<SphericalDisplacementMap>();
	}
}

#endif