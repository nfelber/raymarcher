#ifndef SPHERICAL_DISPLACEMENT_MAP_h
#define SPHERICAL_DISPLACEMENT_MAP_h

#include "distance_transformer.cuh"

class SphericalDisplacementMap : public DistanceTransformer<SphericalDisplacementMap> {
protected:
    unsigned int m_texObj;
    float m_textureScale = 1.0f;
	float m_maxDisplacement;
    float m_triPlanarNormalWeight = 1.0f;
public:
	__host__ __device__ static DistanceTransformerId id() {
        return DistanceTransformerId::SphericalDisplacementMap;
    }
	__host__ __device__ SphericalDisplacementMap() = default;
	__host__ __device__ SphericalDisplacementMap(cudaTextureObject_t texObj, float textureScale,
												 float maxDisplacement, float triPlanarNormalWeight) :
			m_texObj(texObj), m_textureScale(textureScale), m_maxDisplacement(maxDisplacement),
			m_triPlanarNormalWeight(triPlanarNormalWeight) {}

	__host__ __device__ cudaTextureObject_t getTexObj() const { return m_texObj; }
	__host__ __device__ void setTexObj(cudaTextureObject_t texObj) { m_texObj = texObj; }
	__host__ __device__ float getTextureScale() const { return m_textureScale; }
	__host__ __device__ void setTextureScale(float textureScale) { m_textureScale = textureScale; }
	__host__ __device__ float getMaxDisplacement() const { return m_maxDisplacement; }
	__host__ __device__ void setMaxDisplacement(float maxDisplacement) { m_maxDisplacement = maxDisplacement; }
	__host__ __device__ float getTriPlanarNormalWeight() const { return m_triPlanarNormalWeight; }
	__host__ __device__ void setTriPlanarNormalWeight(float triPlanarNormalWeight) { m_triPlanarNormalWeight = triPlanarNormalWeight; }

	__device__ float transform(const glm::vec3& p, float d) {
		if (d >= m_maxDisplacement + 0.05f) return d - m_maxDisplacement;
		glm::vec3 n = normalize(p);
		float nx = pow(abs(n.x), m_triPlanarNormalWeight);
		float ny = pow(abs(n.y), m_triPlanarNormalWeight);
		float nz = pow(abs(n.z), m_triPlanarNormalWeight);
		glm::vec3 sp = p / m_textureScale;
		float txy = tex2D<float>(m_texObj, sp.x, sp.y);
		float txz = tex2D<float>(m_texObj, sp.x, sp.z);
		float tyz = tex2D<float>(m_texObj, sp.y, sp.z);
		float value = (nx * tyz + ny * txz + nz * txy) / (nx + ny + nz);
		return (d - value * m_maxDisplacement) * 0.5f;
	}
};

#endif