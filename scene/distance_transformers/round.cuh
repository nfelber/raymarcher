#ifndef ROUND_h
#define ROUND_h

#include "distance_transformer.cuh"

class Round : public DistanceTransformer<Round> {
protected:
	float m_radius;
public:
	__host__ __device__ static DistanceTransformerId id() {
        return DistanceTransformerId::Round;
    }
	__host__ __device__ Round() = default;
	__host__ __device__ Round(float radius) : m_radius(radius) {}
	__host__ __device__ float getRadius() const { return m_radius; }
	__host__ __device__ void setRadius(float radius) { m_radius = radius; }

	__host__ __device__ float transform(const glm::vec3& p, float d) {
		return d - m_radius;
	}
};

#endif