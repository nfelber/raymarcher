#ifndef TEXTURE_h
#define TEXTURE_h

#include <string>

#include "../stb/stb_image.h"

class Texture {
protected:
    std::string m_filename;
    unsigned int m_width;
    unsigned int m_height;
    cudaArray_t m_cuArray;
    cudaTextureObject_t m_texObj = 0;
public:
    Texture(const std::string& filename) : m_filename(filename) {}
    ~Texture() {
        cudaDestroyTextureObject(m_texObj);
        cudaFreeArray(m_cuArray);
    }

	__host__ __device__ cudaTextureObject_t getTexObj() const { return m_texObj; }
	__host__ __device__ void setRadius(const cudaTextureObject_t& texObj) { m_texObj = texObj; }

    void uploadToDevice() {
        std::cout << "Uploading texture " << m_filename << " to device... ";

        // Read texture file
        int width, height, nrChannels;
        stbi_info(m_filename.c_str(), &width, &height, &nrChannels);
        int desiredChannels = nrChannels == 1 ? 1 : 4;
        unsigned char *h_data = stbi_load(m_filename.c_str(), &width, &height, &nrChannels, desiredChannels);
        if (h_data == nullptr) {
            std::cerr << "ERROR: Can't open " << m_filename << std::endl;
            return;
        }
        m_width = width;
        m_height = height;

        // Allocate CUDA array in device memory
        // cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<uchar4>();
        cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(
            8,
            desiredChannels >= 2 ? 8 : 0,
            desiredChannels >= 3 ? 8 : 0,
            desiredChannels == 4 ? 8 : 0,
            cudaChannelFormatKindUnsigned);
        checkCudaErrors(cudaMallocArray(&m_cuArray, &channelDesc, m_width, m_height));

        // Set pitch of the source (the width in memory in bytes of the 2D array pointed
        // to by src, including padding), we dont have any padding
        const size_t spitch = m_width * sizeof(char) * desiredChannels;
        // Copy data located at address h_data in host memory to device memory
        checkCudaErrors(cudaMemcpy2DToArray(m_cuArray, 0, 0, h_data, spitch, m_width * sizeof(char) * desiredChannels,
                            m_height, cudaMemcpyHostToDevice));

        // Free host memory
        free(h_data);

        // Specify texture
        struct cudaResourceDesc resDesc;
        memset(&resDesc, 0, sizeof(resDesc));
        resDesc.resType = cudaResourceTypeArray;
        resDesc.res.array.array = m_cuArray;

        // Specify texture object parameters
        struct cudaTextureDesc texDesc;
        memset(&texDesc, 0, sizeof(texDesc));
        texDesc.addressMode[0] = cudaAddressModeWrap;
        texDesc.addressMode[1] = cudaAddressModeWrap;
        texDesc.readMode = cudaReadModeNormalizedFloat;
        texDesc.filterMode = cudaFilterModeLinear;
        texDesc.normalizedCoords = 1;

        // Create texture object
        checkCudaErrors(cudaCreateTextureObject(&m_texObj, &resDesc, &texDesc, NULL));
        std::cout << "done." << std::endl;
    }
};

#endif