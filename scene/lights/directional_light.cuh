#ifndef DIRECTIONAL_LIGHT_h
#define DIRECTIONAL_LIGHT_h

#include "light.cuh"
#include <glm/gtx/quaternion.hpp>

class DirectionalLight : public Light<DirectionalLight> {
public:
	__host__ __device__ static LightId id() {
        return LightId::DirectionalLight;
    }

    __host__ __device__ DirectionalLight() = default;
    __host__ __device__ DirectionalLight(const glm::vec3 color, float intensity, float softness) : Light(color, intensity, softness) {}

    __host__ __device__ void sampleLi(const glm::vec3& p, glm::vec3* Li, glm::vec3* wi, float* lightDistance) {
        *Li = m_color * m_intensity;
        *wi = m_orientation * glm::vec3(0.0, 1.0, 0.0);
        *lightDistance = MAXFLOAT;
    }
};

#endif