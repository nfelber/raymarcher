#ifndef POINT_LIGHT_h
#define POINT_LIGHT_h

#include "light.cuh"

class PointLight : public Light<PointLight> {
public:
	__host__ __device__ static LightId id() {
        return LightId::PointLight;
    }

    __host__ __device__ PointLight() = default;
    __host__ __device__ PointLight(const glm::vec3 color, float intensity, float softness) : Light(color, intensity, softness) {}

    __host__ __device__ void sampleLi(const glm::vec3& p, glm::vec3* Li, glm::vec3* wi, float* lightDistance) {
        glm::vec3 pointToLight = m_position - p;
        *lightDistance = length(pointToLight);
        *Li = m_color * m_intensity / (*lightDistance * *lightDistance);
        *wi = normalize(pointToLight);
    }
};

#endif