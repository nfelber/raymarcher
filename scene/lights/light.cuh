#ifndef LIGHT_h
#define LIGHT_h

#include "../scene.cuh"

enum class LightId : unsigned int {
	DirectionalLight,
    PointLight,
};

template<typename T>
class Light : public Transformable, public Serializable<T> {
protected:
    glm::vec3 m_color = glm::vec3(1.0f, 1.0f, 1.0f);
    float m_intensity = 1.0f;
    float m_softness = 1.0f;
public:
	__host__ __device__ static LightId id() {
        return T::id();
    }

    __host__ __device__ Light() = default;
    __host__ __device__ Light(const glm::vec3 color, float intensity, float softness) :
        m_color(color), m_intensity(intensity), m_softness(softness) {}

    __host__ __device__ void setColor(const glm::vec3& color) { m_color = color; }
    __host__ __device__ glm::vec3 getColor() { return m_color; }
    __host__ __device__ void setIntensity(float intensity) { m_intensity = intensity; }
    __host__ __device__ float getIntensity() { return m_intensity; }
    __host__ __device__ void setSoftness(float softness) { m_softness = softness; }
    __host__ __device__ float getSoftness() { return m_softness; }

    __host__ __device__ void sampleLi(const glm::vec3& p, glm::vec3* Li, glm::vec3* wi, float* LightDistance) {
        return static_cast<T*>(this)->sampleLi(p, Li, wi);
    }
};

#include "directional_light.cuh"
#include "point_light.cuh"

#define uint2LightId(value) static_cast<LightId>(value)
#define lightId2uint(value) static_cast<unsigned int>(value)

template<class F> __host__ __device__ void callOnLightId(LightId id, F f) {
	switch (id) {
		case LightId::DirectionalLight:
			return f.template operator()<DirectionalLight>();
		case LightId::PointLight:
			return f.template operator()<PointLight>();
	}
}

struct LightHeader {
	LightId id; // 32 bits

	__host__ __device__ LightHeader(LightId id) : id(id) {}

	__host__ __device__ unsigned int serialize() {
		return primitiveId2uint(id);
	}

	__host__ __device__ static LightHeader deserialize(unsigned int raw) {
		return LightHeader(uint2LightId(raw));
	}
};

#endif