#ifndef MATERIAL_h
#define MATERIAL_h

#include <glm/glm.hpp>

struct Material {
    glm::vec3 albedo;
    float metalness;
    float roughness;

    unsigned int albedoTex = 0;
    unsigned int metalnessTex = 0;
    unsigned int roughnessTex = 0;
    float textureScale = 1.0f;
    float triPlanarNormalWeight = 1.0f;

    __host__ __device__ Material() = default;
    __host__ __device__ Material(const glm::vec3 albedo, float metalness, float roughness) :
        albedo(albedo), metalness(metalness), roughness(roughness) {}
};

#endif