#ifndef CAMERA_h
#define CAMERA_h

#include "scene.cuh"

class Camera : public Transformable, public Serializable<Camera> {
protected:
    float m_fovy, m_aspect, m_near, m_far;
public:
	__host__ __device__ Camera() = default;
	__host__ __device__ Camera(float fovy, float aspect, float near, float far)
							: m_fovy(fovy), m_aspect(aspect), m_near(near), m_far(far)  {}

	__host__ __device__ void setFovy(float fovy) { m_fovy = fovy; }
	__host__ __device__ void setAspect(float aspect) { m_aspect = aspect; }
	__host__ __device__ void setNear(float near) { m_near = near; }
	__host__ __device__ void setFar(float far) { m_far = far; }
	__host__ __device__ float getFovy() const { return m_fovy; }
	__host__ __device__ float getAspect() const { return m_aspect; }
	__host__ __device__ float getNear() const { return m_near; }
	__host__ __device__ float getFar() const { return m_far; }
};

#endif