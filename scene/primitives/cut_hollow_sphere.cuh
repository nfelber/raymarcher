#ifndef CUT_HOLLOW_SPHERE_h
#define CUT_HOLLOW_SPHERE_h

#include "primitive.cuh"

class CutHollowSphere : public Primitive<CutHollowSphere> {
protected:
	float m_radius;
	float m_height;
	float m_thickness;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::CutHollowSphere;
    }

	__host__ __device__ CutHollowSphere() = default;
	__host__ __device__ CutHollowSphere(float radius, float height, float thickness) : m_radius(radius), m_height(height), m_thickness(thickness) {}
    __host__ __device__ float getRadius() const { return m_radius; }
    __host__ __device__ void setRadius(float radius) { m_radius = radius; };
    __host__ __device__ float getHeight() const { return m_height; }
    __host__ __device__ void setHeight(float height) { m_height = height; };
    __host__ __device__ float getThickness() const { return m_thickness; }
    __host__ __device__ void setThickness(float thickness) { m_thickness = thickness; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		float w = sqrt(m_radius * m_radius - m_height * m_height);
		glm::vec2 q(length(glm::vec2(p.x, p.z)), p.y);
		return ((m_height * q.x < w * q.y) ? length(q - glm::vec2(w, m_height)) : abs(length(q) - m_radius)) - m_thickness;
	}
};

#endif