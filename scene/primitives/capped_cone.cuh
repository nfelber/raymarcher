#ifndef CAPPED_CONE_h
#define CAPPED_CONE_h

#include "primitive.cuh"

class CappedCone : public Primitive<CappedCone> {
protected:
	float m_radius1;
	float m_radius2;
	float m_height;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::CappedCone;
    }

	__host__ __device__ CappedCone() = default;
	__host__ __device__ CappedCone(float radius1, float radius2, float height) : m_radius1(radius1), m_radius2(radius2), m_height(height) {}
    __host__ __device__ float getRadius1() const { return m_radius1; }
    __host__ __device__ void setRadius1(float radius) { m_radius1 = radius; };
    __host__ __device__ float getRadius2() const { return m_radius2; }
    __host__ __device__ void setRadius2(float radius) { m_radius2 = radius; };
    __host__ __device__ float getHeight() const { return m_height; }
    __host__ __device__ void setHeight(float height) { m_height = height; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		glm::vec2 q(length(glm::vec2(p.x, p.z)), p.y);
		glm::vec2 k1(m_radius2, m_height);
		glm::vec2 k2(m_radius2 - m_radius1, 2.0f * m_height);
		glm::vec2 ca(q.x - min(q.x, (q.y < 0.0f) ? m_radius1 : m_radius2), abs(q.y) - m_height);
		glm::vec2 cb = q - k1 + k2 * glm::clamp(dot(k1 - q, k2) / dot(k2, k2), 0.0f, 1.0f);
		float s = (cb.x < 0.0f && ca.y < 0.0f) ? -1.0f : 1.0f;
		return s*sqrt(min(dot(ca, ca), dot(cb, cb)));
	}
};

#endif