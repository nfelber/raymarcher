#ifndef BOX_h
#define BOX_h

#include "primitive.cuh"

class Box : public Primitive<Box> {
protected:
    glm::vec3 m_dimensions;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::Box;
    }

	__host__ __device__ Box() = default;
	__host__ __device__ Box(const glm::vec3& dimensions) : m_dimensions(dimensions) {}
	__host__ __device__ glm::vec3 getDimensions() const { return m_dimensions; }
	__host__ __device__ void setDimensions(const glm::vec3& dimensions) { m_dimensions = dimensions; }

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		glm::vec3 q = abs(p) - m_dimensions;
		return length(max(q, 0.0f)) + min(max(q.x, max(q.y, q.z)), 0.0f);
	}
};

#endif