#ifndef BOX_FRAME_h
#define BOX_FRAME_h

#include "primitive.cuh"

class BoxFrame : public Primitive<BoxFrame> {
protected:
    glm::vec3 m_dimensions;
	float m_thickness;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::BoxFrame;
    }

	__host__ __device__ BoxFrame() = default;
	__host__ __device__ BoxFrame(const glm::vec3& dimensions, float thickness) : m_dimensions(dimensions), m_thickness(thickness) {}
	__host__ __device__ glm::vec3 getDimensions() const { return m_dimensions; }
	__host__ __device__ void setDimensions(const glm::vec3& dimensions) { m_dimensions = dimensions; }
    __host__ __device__ float getThickness() const { return m_thickness; }
    __host__ __device__ void setThickness(float thickness) { m_thickness = thickness; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		glm::vec3 q = abs(p) - m_dimensions;
		glm::vec3 q2 = abs(q + m_thickness) - m_thickness;
		return min(min(
			length(max(glm::vec3(q.x, q2.y, q2.z), 0.0f)) + min(max(q.x, max(q2.y, q2.z)), 0.0f),
			length(max(glm::vec3(q2.x, q.y, q2.z), 0.0f)) + min(max(q.x, max(q.y, q2.z)), 0.0f)),
			length(max(glm::vec3(q2.x, q2.y, q.z), 0.0f)) + min(max(q.x, max(q2.y, q.z)), 0.0f));
	}
};

#endif