#ifndef SPHERE_h
#define SPHERE_h

#include "primitive.cuh"

class Sphere : public Primitive<Sphere> {
protected:
	float m_radius;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::Sphere;
    }

	__host__ __device__ Sphere() = default;
	__host__ __device__ Sphere(float radius) : m_radius(radius) {}
    __host__ __device__ float getRadius() const { return m_radius; }
    __host__ __device__ void setRadius(float radius) { m_radius = radius; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		return length(p) - m_radius;
	}
};

#endif