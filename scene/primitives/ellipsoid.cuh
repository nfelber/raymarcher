#ifndef ELLIPSOID_h
#define ELLIPSOID_h

#include "primitive.cuh"

class Ellipsoid : public Primitive<Ellipsoid> {
protected:
    glm::vec3 m_semiAxes;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::Ellipsoid;
    }

	__host__ __device__ Ellipsoid() = default;
	__host__ __device__ Ellipsoid(const glm::vec3& semiAxes) : m_semiAxes(semiAxes) {}
	__host__ __device__ glm::vec3 getSemiAxes() const { return m_semiAxes; }
	__host__ __device__ void setSemiAxes(const glm::vec3& semiAxes) { m_semiAxes = semiAxes; }

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		float k0 = length(p / m_semiAxes);
		float k1 = length(p / (m_semiAxes * m_semiAxes));
		return k0 * (k0 - 1.0f) / k1;
	}
};

#endif