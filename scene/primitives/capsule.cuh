#ifndef CAPSULE_h
#define CAPSULE_h

#include "primitive.cuh"

class Capsule : public Primitive<Capsule> {
protected:
	float m_radius;
	float m_height;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::Capsule;
    }

	__host__ __device__ Capsule() = default;
	__host__ __device__ Capsule(float radius, float height) : m_radius(radius), m_height(height) {}
    __host__ __device__ float getRadius() const { return m_radius; }
    __host__ __device__ void setRadius(float radius) { m_radius = radius; };
    __host__ __device__ float getHeight() const { return m_height; }
    __host__ __device__ void setHeight(float height) { m_height = height; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		glm::vec3 q(p.x, p.y - glm::clamp(p.y, 0.0f, m_height), p.z);
		return length(q) - m_radius;
	}
};

#endif