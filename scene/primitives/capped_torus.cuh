#ifndef CAPPED_TORUS_h
#define CAPPED_TORUS_h

#include "primitive.cuh"

class CappedTorus : public Primitive<CappedTorus> {
protected:
	float m_radius;
	float m_thickness;
	float m_angle;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::CappedTorus;
    }

	__host__ __device__ CappedTorus() = default;
	__host__ __device__ CappedTorus(float radius, float thickness, float angle) : m_radius(radius), m_thickness(thickness), m_angle(angle) {}
    __host__ __device__ float getRadius() const { return m_radius; }
    __host__ __device__ void setRadius(float radius) { m_radius = radius; };
    __host__ __device__ float getThickness() const { return m_thickness; }
    __host__ __device__ void setThickness(float thickness) { m_thickness = thickness; };
    __host__ __device__ float getAngle() const { return m_angle; }
    __host__ __device__ void setAngle(float angle) { m_angle = angle; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		glm::vec2 sc(sin(m_angle), cos(m_angle));
		glm::vec3 q(abs(p.x), p.y, p.z);
		float k = (sc.y * q.x > sc.x * q.y) ? dot(glm::vec2(q.x, q.y), sc) : length(glm::vec2(q.x, q.y));
		return sqrt(dot(q,q) + m_radius * m_radius - 2.0f * m_radius * k) - m_thickness;
	}
};

#endif