#ifndef TORUS_h
#define TORUS_h

#include "primitive.cuh"

class Torus : public Primitive<Torus> {
protected:
	float m_radius;
	float m_thickness;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::Torus;
    }

	__host__ __device__ Torus() = default;
	__host__ __device__ Torus(float radius, float thickness) : m_radius(radius), m_thickness(thickness) {}
    __host__ __device__ float getRadius() const { return m_radius; }
    __host__ __device__ void setRadius(float radius) { m_radius = radius; };
    __host__ __device__ float getThickness() const { return m_thickness; }
    __host__ __device__ void setThickness(float thickness) { m_thickness = thickness; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		glm::vec2 q(length(glm::vec2(p.x, p.z)) - m_radius, p.y);
		return length(q) - m_thickness;
	}
};

#endif