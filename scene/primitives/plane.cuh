#ifndef PLANE_h
#define PLANE_h

#include "primitive.cuh"

class Plane : public Primitive<Plane> {
protected:
    glm::vec3 m_normal;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::Plane;
    }

	__host__ __device__ Plane() = default;
	__host__ __device__ Plane(const glm::vec3& normal) : m_normal(normal) {}
	__host__ __device__ glm::vec3 getNormal() const { return m_normal; }
	__host__ __device__ void setNormal(const glm::vec3& normal) { m_normal = normal; }

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		return abs(dot(p, m_normal)) - 0.001f;
	}

	__host__ __device__ float originDirectionalDistance(const glm::vec3& p, const glm::vec3& dir, unsigned int steps) {
		float d = -dot(p, m_normal) / dot(dir, m_normal);
		if (d < -0.001f) return MAXFLOAT;
        return d - 0.001f;
    }
};

#endif