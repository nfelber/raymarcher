#ifndef CYLINDER_h
#define CYLINDER_h

#include "primitive.cuh"

class Cylinder : public Primitive<Cylinder> {
protected:
	float m_radius;
	float m_height;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::Cylinder;
    }

	__host__ __device__ Cylinder() = default;
	__host__ __device__ Cylinder(float radius, float height) : m_radius(radius), m_height(height) {}
    __host__ __device__ float getRadius() const { return m_radius; }
    __host__ __device__ void setRadius(float radius) { m_radius = radius; };
    __host__ __device__ float getHeight() const { return m_height; }
    __host__ __device__ void setHeight(float height) { m_height = height; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		glm::vec2 d = abs(glm::vec2(length(glm::vec2(p.x, p.z)), p.y)) - glm::vec2(m_radius, m_height);
		return min(max(d.x, d.y), 0.0f) + length(max(d, 0.0f));
	}
};

#endif