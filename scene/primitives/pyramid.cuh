#ifndef PYRAMID_h
#define PYRAMID_h

#include "primitive.cuh"

class Pyramid : public Primitive<Pyramid> {
protected:
	float m_height;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::Pyramid;
    }

	__host__ __device__ Pyramid() = default;
	__host__ __device__ Pyramid(float height) : m_height(height) {}
    __host__ __device__ float getHeight() const { return m_height; }
    __host__ __device__ void setHeight(float height) { m_height = height; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		float m2 = m_height * m_height + 0.25f;
		  
		glm::vec2 pxz = abs(glm::vec2(p.x, p.z));
		pxz = (pxz.y > pxz.x) ? glm::vec2(pxz.y, pxz.x) : pxz;
		pxz -= 0.5f;

		glm::vec3 q(pxz.y, m_height * p.y - 0.5f * pxz.x, m_height * pxz.x + 0.5f * p.y);
		
		float s = max(-q.x, 0.0f);
		float t = glm::clamp((q.y - 0.5f * pxz.y) / (m2 + 0.25f), 0.0f, 1.0f);
			
		float a = m2 * (q.x + s) * (q.x + s) + q.y * q.y;
		float b = m2 * (q.x + 0.5f * t) * (q.x + 0.5f * t) + (q.y - m2 * t) * (q.y - m2 * t);
			
		float d2 = min(q.y, - q.x * m2 - q.y * 0.5f) > 0.0f ? 0.0f : min(a, b);
			
		return sqrt((d2 + q.z * q.z) / m2) * glm::sign(max(q.z, -p.y));
	}
};

#endif