#ifndef OCTAHEDRON_h
#define OCTAHEDRON_h

#include "primitive.cuh"

class Octahedron : public Primitive<Octahedron> {
protected:
	float m_radius;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::Octahedron;
    }

	__host__ __device__ Octahedron() = default;
	__host__ __device__ Octahedron(float radius) : m_radius(radius) {}
    __host__ __device__ float getRadius() const { return m_radius; }
    __host__ __device__ void setRadius(float radius) { m_radius = radius; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		glm::vec3 q = abs(p);
		float m = q.x + q.y + q.z - m_radius;
		glm::vec3 q2;
		if (3.0f * q.x < m) q2 = glm::vec3(q.x, q.y, q.z);
		else if(3.0f * q.y < m) q2 = glm::vec3(q.y, q.z, q.x);
		else if(3.0f * q.z < m) q2 = glm::vec3(q.z, q.x, q.y);
		else return m * 0.57735027f;
			
		float k = glm::clamp(0.5f * (q2.z - q2.y + m_radius), 0.0f, m_radius); 
		return length(glm::vec3(q2.x, q2.y - m_radius + k, q2.z - k));
	}
};

#endif