#ifndef SOLID_ANGLE_h
#define SOLID_ANGLE_h

#include "primitive.cuh"

class SolidAngle : public Primitive<SolidAngle> {
protected:
	float m_radius;
	float m_angle;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::SolidAngle;
    }

	__host__ __device__ SolidAngle() = default;
	__host__ __device__ SolidAngle(float radius, float angle) : m_radius(radius), m_angle(angle) {}
    __host__ __device__ float getRadius() const { return m_radius; }
    __host__ __device__ void setRadius(float radius) { m_radius = radius; };
    __host__ __device__ float getAngle() const { return m_angle; }
    __host__ __device__ void setAngle(float angle) { m_angle = angle; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		glm::vec2 c(sin(m_angle), cos(m_angle));
		glm::vec2 q(length(glm::vec2(p.x, p.z)), p.y);
		float l = length(q) - m_radius;
		float m = length(q - c * glm::clamp(dot(q,c), 0.0f, m_radius) );
		return max(l, m * glm::sign(c.y * q.x - c.x * q.y));
	}
};

#endif