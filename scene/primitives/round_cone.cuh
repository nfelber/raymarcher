#ifndef ROUND_CONE_h
#define ROUND_CONE_h

#include "primitive.cuh"

class RoundCone : public Primitive<RoundCone> {
protected:
	float m_radius1;
	float m_radius2;
	float m_height;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::RoundCone;
    }

	__host__ __device__ RoundCone() = default;
	__host__ __device__ RoundCone(float radius1, float radius2, float height) : m_radius1(radius1), m_radius2(radius2), m_height(height) {}
    __host__ __device__ float getRadius1() const { return m_radius1; }
    __host__ __device__ void setRadius1(float radius) { m_radius1 = radius; };
    __host__ __device__ float getRadius2() const { return m_radius2; }
    __host__ __device__ void setRadius2(float radius) { m_radius2 = radius; };
    __host__ __device__ float getHeight() const { return m_height; }
    __host__ __device__ void setHeight(float height) { m_height = height; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		float b = (m_radius1 - m_radius2) / m_height;
		float a = sqrt(1.0f - b*b);

		glm::vec2 q(length(glm::vec2(p.x, p.z)), p.y);
		float k = dot(q, glm::vec2(-b, a));
		if (k < 0.0f) return length(q) - m_radius1;
		if (k > a * m_height) return length(q - glm::vec2(0.0f, m_height)) - m_radius2;
		return dot(q, glm::vec2(a, b)) - m_radius1;
	}
};

#endif