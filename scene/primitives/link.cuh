#ifndef LINK_h
#define LINK_h

#include "primitive.cuh"

class Link : public Primitive<Link> {
protected:
	float m_radius;
	float m_thickness;
	float m_length;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::Link;
    }

	__host__ __device__ Link() = default;
	__host__ __device__ Link(float radius, float thickness, float length) : m_radius(radius), m_thickness(thickness), m_length(length) {}
    __host__ __device__ float getRadius() const { return m_radius; }
    __host__ __device__ void setRadius(float radius) { m_radius = radius; };
    __host__ __device__ float getThickness() const { return m_thickness; }
    __host__ __device__ void setThickness(float thickness) { m_thickness = thickness; };
    __host__ __device__ float getLength() const { return m_length; }
    __host__ __device__ void setLength(float length) { m_length = length; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		glm::vec3 q(p.x, max(abs(p.y) - m_length, 0.0f), p.z);
		return length(glm::vec2(length(glm::vec2(q.x, q.y)) - m_radius, q.z)) - m_thickness;
	}
};

#endif