#ifndef PRIMITIVE_h
#define PRIMITIVE_h

#include "../scene.cuh"

enum class PrimitiveId : uint16_t {
	Sphere,
	Box,
	BoxFrame,
	Capsule,
	Cone,
	CappedCone,
	RoundCone,
	CutHollowSphere,
	Cylinder,
	Ellipsoid,
	Julia,
	Link,
	Octahedron,
	Plane,
	Pyramid,
	SolidAngle,
	Torus,
	CappedTorus,
};

template <typename T>
class Primitive : public Serializable<T> {
public:
	__host__ __device__ static PrimitiveId id() {
        return T::id();
    }

	__host__ __device__ float originSDF(const glm::vec3& p) {
        return static_cast<T*>(this)->originSDF(p);
    }

	__host__ __device__ float originDirectionalDistance(const glm::vec3& p, const glm::vec3& dir, unsigned int steps) {
		float d = 0.0f;
		for (int i=0; i<steps; ++i) {
			float r = static_cast<T*>(this)->originSDF(p + d * dir);
			d += r;
		}
        return d;
    }
};

#include "sphere.cuh"
#include "box.cuh"
#include "box_frame.cuh"
#include "capsule.cuh"
#include "cone.cuh"
#include "capped_cone.cuh"
#include "round_cone.cuh"
#include "cut_hollow_sphere.cuh"
#include "cylinder.cuh"
#include "ellipsoid.cuh"
#include "julia.cuh"
#include "link.cuh"
#include "octahedron.cuh"
#include "plane.cuh"
#include "pyramid.cuh"
#include "solid_angle.cuh"
#include "torus.cuh"
#include "capped_torus.cuh"

#define uint2PrimitiveId(value) static_cast<PrimitiveId>(value)
#define primitiveId2uint(value) static_cast<unsigned int>(value)

template<class F> __host__ __device__ void callOnPrimitiveId(PrimitiveId id, F f) {
	switch (id) {
		case PrimitiveId::Sphere:
			return f.template operator()<Sphere>();
		case PrimitiveId::Box:
			return f.template operator()<Box>();
		case PrimitiveId::BoxFrame:
			return f.template operator()<BoxFrame>();
		case PrimitiveId::Capsule:
			return f.template operator()<Capsule>();
		case PrimitiveId::Cone:
			return f.template operator()<Cone>();
		case PrimitiveId::CappedCone:
			return f.template operator()<CappedCone>();
		case PrimitiveId::RoundCone:
			return f.template operator()<RoundCone>();
		case PrimitiveId::CutHollowSphere:
			return f.template operator()<CutHollowSphere>();
		case PrimitiveId::Cylinder:
			return f.template operator()<Cylinder>();
		case PrimitiveId::Ellipsoid:
			return f.template operator()<Ellipsoid>();
		case PrimitiveId::Julia:
			return f.template operator()<Julia>();
		case PrimitiveId::Link:
			return f.template operator()<Link>();
		case PrimitiveId::Octahedron:
			return f.template operator()<Octahedron>();
		case PrimitiveId::Plane:
			return f.template operator()<Plane>();
		case PrimitiveId::Pyramid:
			return f.template operator()<Pyramid>();
		case PrimitiveId::SolidAngle:
			return f.template operator()<SolidAngle>();
		case PrimitiveId::Torus:
			return f.template operator()<Torus>();
		case PrimitiveId::CappedTorus:
			return f.template operator()<CappedTorus>();
	}
}

struct PrimitiveHeader {
	PrimitiveId id; // 12 bits
  	unsigned int materialIdx; // 12 bits
	unsigned int spaceTransformerCount; // 4 bits
	unsigned int distanceTransformerCount; // 4 bits

	__host__ __device__ PrimitiveHeader(PrimitiveId id, unsigned int materialIdx,
										unsigned int spaceTransformerCount, unsigned int distanceTransformerCount) :
									   		id(id), materialIdx(materialIdx), spaceTransformerCount(spaceTransformerCount),
											distanceTransformerCount(distanceTransformerCount) {}

	__host__ __device__ unsigned int serialize() {
		return (primitiveId2uint(id) << 20) |
			   ((materialIdx & 0xFFF) << 8) |
			   ((spaceTransformerCount & 0xF) << 4) |
			   (distanceTransformerCount & 0xF);
	}

	__host__ __device__ static PrimitiveHeader deserialize(unsigned int raw) {
		return PrimitiveHeader {
			uint2PrimitiveId(raw >> 20),
			(raw >> 8) & 0xFFF,
			(raw >> 4) & 0xF,
			raw & 0xF
		};
	}
};

#endif