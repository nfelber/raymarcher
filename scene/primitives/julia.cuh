#ifndef JULIA_h
#define JULIA_h

#include "primitive.cuh"

class Julia : public Primitive<Julia> {
protected:
	glm::quat m_kC;
	float m_cutHeight;

	__host__ __device__ static glm::quat qSquare(const glm::quat& q) {
		return glm::quat(q.w*q.w - q.x*q.x - q.y*q.y - q.z*q.z, 2.0f*q.w*glm::vec3(q.x, q.y, q.z));
	}

	__host__ __device__ static glm::quat qCube(const glm::quat& q) {
		glm::quat q2(q.w*q.w, q.x*q.x, q.y*q.y, q.z*q.z);
		return glm::quat(q.w * (q2.w - 3.0f*q2.x - 3.0f*q2.y - 3.0f*q2.z), 
						glm::vec3(q.x, q.y, q.z) * (3.0f*q2.w - q2.x - q2.y - q2.z));
	}
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::Julia;
    }

	__host__ __device__ Julia() = default;
	__host__ __device__ Julia(const glm::quat& kC, float cutHeight) : m_kC(kC), m_cutHeight(cutHeight) {}
    __host__ __device__ glm::quat getkC() const { return m_kC; }
    __host__ __device__ void setkC(const glm::quat& kC) { m_kC = kC; };
    __host__ __device__ float getCutHeight() const { return m_cutHeight; }
    __host__ __device__ void setCutHeight(float cutHeight) { m_cutHeight = cutHeight; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		glm::quat z = glm::quat(p.x, p.y, p.z, 0.0f);
		float dz2 = 1.0f;
		float m2  = 0.0f;
		float n   = 0.0f;
		
		for(int i=0; i<256; i++) {
			// z' = 3z² -> |z'|² = 9|z²|²
			glm::quat zs = qSquare(z);
			dz2 *= 9.0f*glm::dot(zs, zs);
			
			// z = z³ + c		
			z = qCube(z) + m_kC;
			
			// stop under divergence		
			m2 = dot(z, z);

			// exit condition
			if(m2 > 256.0f) break;				 
			n += 1.0f;
		}
	
		// sdf(z) = log|z|·|z|/|dz| : https://iquilezles.org/articles/distancefractals
		float d = 0.25f*log(m2)*sqrt(m2/dz2);
	
		d = max(d, p.y - m_cutHeight);
		
		return d; 
	}
};

#endif