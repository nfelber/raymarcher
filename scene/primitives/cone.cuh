#ifndef CONE_h
#define CONE_h

#include "primitive.cuh"

class Cone : public Primitive<Cone> {
protected:
	float m_angle;
	float m_height;
public:
	__host__ __device__ static PrimitiveId id() {
        return PrimitiveId::Cone;
    }

	__host__ __device__ Cone() = default;
	__host__ __device__ Cone(float angle, float height) : m_angle(angle), m_height(height) {}
    __host__ __device__ float getAngle() const { return m_angle; }
    __host__ __device__ void setAngle(float angle) { m_angle = angle; };
    __host__ __device__ float getHeight() const { return m_height; }
    __host__ __device__ void setHeight(float height) { m_height = height; };

	__host__ __device__ float originSDF(const glm::vec3& p) const {
		glm::vec2 c(sin(m_angle), cos(m_angle));
		glm::vec2 q = m_height * glm::vec2(c.x / c.y, -1.0f);
		glm::vec2 w = glm::vec2(length(glm::vec2(p.x, p.z)), p.y);
		glm::vec2 a = w - q * glm::clamp(dot(w, q) / dot(q, q), 0.0f, 1.0f);
		glm::vec2 b = w - q * glm::vec2(glm::clamp(w.x / q.x, 0.0f, 1.0f), 1.0f);
		float k = glm::sign(q.y);
		float d = min(dot(a, a), dot(b, b));
		float s = max(k * (w.x * q.y - w.y * q.x), k * (w.y - q.y));
		return sqrt(d) * glm::sign(s);
	}
};

#endif