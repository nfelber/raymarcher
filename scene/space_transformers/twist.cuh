#ifndef TWIST_h
#define TWIST_h

#include "space_transformer.cuh"

class Twist : public SpaceTransformer<Twist> {
protected:
	float m_amount;
public:
	__host__ __device__ static SpaceTransformerId id() {
        return SpaceTransformerId::Twist;
    }

	__host__ __device__ Twist() = default;
	__host__ __device__ Twist(float amount) : m_amount(amount) {}
	__host__ __device__ float getAmount() const { return m_amount; }
	__host__ __device__ void setAmount(float amount) { m_amount = amount; }

	__host__ __device__ glm::vec3 transform(const glm::vec3& p) {
		float c = cos(m_amount*p.y);
		float s = sin(m_amount*p.y);
		glm::mat2 m = glm::mat2(c, -s, s, c);
		return glm::vec3(m * glm::vec2(p.x, p.z), p.y);
	}
};

#endif