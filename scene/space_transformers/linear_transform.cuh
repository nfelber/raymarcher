#ifndef LINEAR_TRANSFORM_h
#define LINEAR_TRANSFORM_h

#include "space_transformer.cuh"

class LinearTransform : public SpaceTransformer<LinearTransform> {
protected:
	glm::vec3 m_position = glm::vec3(0.0);
	glm::quat m_orientation = glm::quat(1.0, 0.0, 0.0, 0.0);

public:
	__host__ __device__ static SpaceTransformerId id() {
        return SpaceTransformerId::LinearTransform;
    }

	__host__ __device__ LinearTransform() = default;
	__host__ __device__ LinearTransform(const glm::vec3& position, const glm::quat& orientation) : m_position(position), m_orientation(orientation) {}
	__host__ __device__ glm::vec3 getPosition() const { return m_position; }
	__host__ __device__ glm::quat getOrientation() const { return m_orientation; }
	__host__ __device__ void setPosition(const glm::vec3& position) { m_position = position; }
	__host__ __device__ void setOrientation(const glm::quat& orientation) { m_orientation = orientation; }

	__host__ __device__ glm::vec3 transform(const glm::vec3& p) {
		return conjugate(m_orientation) * (p - m_position);
	}

	__host__ __device__ glm::vec3 dirTransform(const glm::vec3& dir) {
		return conjugate(m_orientation) * dir;
	}
};

#endif