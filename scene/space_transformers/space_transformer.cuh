
#ifndef SPACE_TRANSFORMER_h
#define SPACE_TRANSFORMER_h

#include "../scene.cuh"

enum class SpaceTransformerId : uint8_t {
	LinearTransform,
	InfiniteGrid,
	Reflection,
	Twist,
};

template <typename T>
class SpaceTransformer : public Serializable<T> {
public:
	__host__ __device__ static SpaceTransformerId id() {
        return T::id();
    }

	__host__ __device__ glm::vec3 transform(const glm::vec3& p) {
        return static_cast<T*>(this)->transform(p);
    }
};

#include "linear_transform.cuh"
#include "infinite_grid.cuh"
#include "reflection.cuh"
#include "twist.cuh"

#define uint2SpaceTransformerId(value) static_cast<SpaceTransformerId>(value)
#define spaceTransformerId2uint(value) static_cast<unsigned int>(value)

template<class F> __host__ __device__ void callOnSpaceTransformerId(SpaceTransformerId id, F f) {
	switch (id) {
		case SpaceTransformerId::LinearTransform:
			return f.template operator()<LinearTransform>();
		case SpaceTransformerId::InfiniteGrid:
			return f.template operator()<InfiniteGrid>();
		case SpaceTransformerId::Reflection:
			return f.template operator()<Reflection>();
		case SpaceTransformerId::Twist:
			return f.template operator()<Twist>();
	}
}

#endif