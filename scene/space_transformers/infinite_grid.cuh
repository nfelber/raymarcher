#ifndef INFINITE_GRID_h
#define INFINITE_GRID_h

#include "space_transformer.cuh"

class InfiniteGrid : public SpaceTransformer<InfiniteGrid> {
protected:
	glm::vec3 m_period;
public:
	__host__ __device__ static SpaceTransformerId id() {
        return SpaceTransformerId::InfiniteGrid;
    }

	__host__ __device__ InfiniteGrid() = default;
	__host__ __device__ InfiniteGrid(const glm::vec3& period) : m_period(period) {}
	__host__ __device__ glm::vec3 getPeriod() const { return m_period; }
	__host__ __device__ void setPeriod(const glm::vec3& period) { m_period = period; }

	__host__ __device__ glm::vec3 transform(const glm::vec3& p) {
		glm::vec3 halfPeriod = 0.5f * m_period;
		return mod(p + halfPeriod, m_period) - halfPeriod;
	}
};

#endif