#ifndef REFLECTION_h
#define REFLECTION_h

#include "space_transformer.cuh"

class Reflection : public SpaceTransformer<Reflection> {
protected:
	glm::vec3 m_normal;
public:
	__host__ __device__ static SpaceTransformerId id() {
        return SpaceTransformerId::Reflection;
    }

	__host__ __device__ Reflection() = default;
	__host__ __device__ Reflection(const glm::vec3& normal) : m_normal(normal) {}
	__host__ __device__ glm::vec3 getNormal() const { return m_normal; }
	__host__ __device__ void setNormal(const glm::vec3& normal) { m_normal = normal; }

	__host__ __device__ glm::vec3 transform(const glm::vec3& p) {
		float p_dot_n = glm::dot(p, m_normal);
		if (p_dot_n < 0.0f) return p - 2.0f * p_dot_n * m_normal;
		return p;
	}
};

#endif