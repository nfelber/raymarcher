#ifndef SCENE_h
#define SCENE_h

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/ext/quaternion_float.hpp>

#include <memory>
#include <vector>
#include <map>
#include <algorithm>
#include <functional>

#define sizeof32b(type) ((sizeof(type) + 3) / 4)

template <typename T>
class Serializable {
public:
	__host__ void serialize(std::vector<unsigned int>& v) const {
        // Might fail if size of T is not a multiple of 4 bytes...
        v.reserve(v.size() + sizeof32b(T));
        std::copy(((unsigned int*) this), &((unsigned int*) this)[sizeof32b(T)], std::back_inserter(v));
	}

	__host__ __device__ static unsigned int serializedSize() {
		return sizeof32b(T);
	}

	__host__ __device__ static T deserialize(const unsigned int* raw) {
		return *((T*) raw);
	}

	__host__ __device__ static T deserializeIncr(const unsigned int** raw) {
		T obj = deserialize(*raw);
		*raw += serializedSize();
		return obj;
	}
};

class Transformable {
protected:
	glm::vec3 m_position = glm::vec3(0.0);
	glm::quat m_orientation = glm::quat(1.0, 0.0, 0.0, 0.0);

	__host__ __device__ Transformable() = default;
public:
	__host__ __device__ glm::vec3 getPosition() const { return m_position; }
	__host__ __device__ glm::quat getOrientation() const { return m_orientation; }
	__host__ __device__ void setPosition(const glm::vec3& position) { m_position = position; }
	__host__ __device__ void setOrientation(const glm::quat& orientation) { m_orientation = orientation; }

	__host__ __device__ void translate(const glm::vec3& v) { m_position += v; }
	__host__ __device__ void rotate(const glm::quat& rotation) { m_orientation = normalize(rotation * m_orientation); }
};

#include "primitives/primitive.cuh"
#include "combiners/combiner.cuh"
#include "space_transformers/space_transformer.cuh"
#include "distance_transformers/distance_transformer.cuh"
#include "lights/light.cuh"
#include "camera.cuh"
#include "material.cuh"
#include "texture.cuh"

class VSpaceTransformerContainer {
public:
    virtual unsigned int getHeader() const = 0;
    virtual void serializeAndPush(std::vector<unsigned int>& v) const = 0;
};

template<typename T>
class SpaceTransformerContainer : public VSpaceTransformerContainer {
    static_assert(std::is_base_of<SpaceTransformer<T>, T>::value, "T must be a descendant of SpaceTransformer<T>");
public:
    T spaceTransformer;

    SpaceTransformerContainer(T st) : spaceTransformer(st) {}

    static std::shared_ptr<SpaceTransformerContainer<T>> createNew(T st) {
        return std::shared_ptr<SpaceTransformerContainer<T>>(new SpaceTransformerContainer<T>(st));
    }

    virtual unsigned int getHeader() const {
        return spaceTransformerId2uint(T::id());
    }

    virtual void serializeAndPush(std::vector<unsigned int>& v) const {
        spaceTransformer.serialize(v);
    }
};

class VDistanceTransformerContainer {
public:
    virtual unsigned int getHeader() const = 0;
    virtual void serializeAndPush(std::vector<unsigned int>& v) const = 0;
};

template<typename T>
class DistanceTransformerContainer : public VDistanceTransformerContainer {
    static_assert(std::is_base_of<DistanceTransformer<T>, T>::value, "T must be a descendant of DistanceTransformer<T>");
public:
    T distanceTransformer;

    DistanceTransformerContainer(T dt) : distanceTransformer(dt) {}

    static std::shared_ptr<DistanceTransformerContainer<T>> createNew(T dt) {
        return std::shared_ptr<DistanceTransformerContainer<T>>(new DistanceTransformerContainer<T>(dt));
    }

    virtual unsigned int getHeader() const {
        return distanceTransformerId2uint(T::id());
    }

    virtual void serializeAndPush(std::vector<unsigned int>& v) const {
        distanceTransformer.serialize(v);
    }
};

class SDFContainer {
protected:
    std::vector<std::shared_ptr<VSpaceTransformerContainer>> m_spaceTransformers;
    std::vector<std::shared_ptr<VDistanceTransformerContainer>> m_distanceTransformers;
public:
    void addSpaceTransformer(std::shared_ptr<VSpaceTransformerContainer> st) {
        m_spaceTransformers.push_back(st);
    }

    void removeSpaceTransformer(std::shared_ptr<VSpaceTransformerContainer> st) {
        m_spaceTransformers.erase(
            std::remove(m_spaceTransformers.begin(), m_spaceTransformers.end(), st),
            m_spaceTransformers.end());
    }

    void addDistanceTransformer(std::shared_ptr<VDistanceTransformerContainer> dt) {
        m_distanceTransformers.push_back(dt);
    }

    void removeDistanceTransformer(std::shared_ptr<VDistanceTransformerContainer> dt) {
        m_distanceTransformers.erase(
            std::remove(m_distanceTransformers.begin(), m_distanceTransformers.end(), dt),
            m_distanceTransformers.end());
    }
};

class VPrimitiveContainer : public SDFContainer, public Transformable {
protected:
    std::shared_ptr<Material> m_material = nullptr;
public:
    virtual void serializeAndPush(std::vector<unsigned int>& v, const std::unordered_map<std::shared_ptr<Material>, unsigned int>& materialIndices) const = 0;

    void setMaterial(std::shared_ptr<Material> material) { m_material = material; }
    std::shared_ptr<Material> getMaterial() { return m_material; }
};

template<typename T>
class PrimitiveContainer : public VPrimitiveContainer {
    static_assert(std::is_base_of<Primitive<T>, T>::value, "T must be a descendant of Primitive<T>");
public:
    T primitive;

    PrimitiveContainer(T primitive) : primitive(primitive) {}

    static std::shared_ptr<PrimitiveContainer<T>> createNew(T primitive) {
        return std::shared_ptr<PrimitiveContainer<T>>(new PrimitiveContainer<T>(primitive));
    }

    virtual void serializeAndPush(std::vector<unsigned int>& v, const std::unordered_map<std::shared_ptr<Material>, unsigned int>& materialIndices) const {
        PrimitiveHeader header(T::id(), materialIndices.at(m_material), m_spaceTransformers.size(), m_distanceTransformers.size());
        v.push_back(header.serialize());

        LinearTransform lt(m_position, m_orientation);
        lt.serialize(v);
        
        unsigned int stHeaderIndex;
        for (int i=0; i<m_spaceTransformers.size(); ++i) {
            if (i % 4 == 0) {
                stHeaderIndex = v.size();
                v.push_back(0);
            }
            v[stHeaderIndex] |= (m_spaceTransformers[i]->getHeader() & 0xFF) << (8 * (3 - i));
            m_spaceTransformers[i]->serializeAndPush(v);
        }

        T transformed_primitive = primitive;
        transformed_primitive.serialize(v);

        unsigned int dtHeaderIndex;
        for (int i=0; i<m_distanceTransformers.size(); ++i) {
            if (i % 4 == 0) {
                dtHeaderIndex = v.size();
                v.push_back(0);
            }
            v[dtHeaderIndex] |= (m_distanceTransformers[i]->getHeader() & 0xFF) << (8 * (3 - i));
            m_distanceTransformers[i]->serializeAndPush(v);
        }
    }
};

class VCombinerContainer : public SDFContainer, public Transformable {
protected:
    std::shared_ptr<VPrimitiveContainer> m_proxy;
    std::vector<std::shared_ptr<VCombinerContainer>> m_childCombiners;
    std::vector<std::shared_ptr<VPrimitiveContainer>> m_childPrimitives;
public:
    void mergeMaterials(std::unordered_map<std::shared_ptr<Material>, unsigned int>& materialIndices, unsigned int* nextIndex) const {
        for (auto combiner : m_childCombiners) {
            combiner->mergeMaterials(materialIndices, nextIndex);
        }

        for (auto primitive : m_childPrimitives) {
            if (!materialIndices.contains(primitive->getMaterial())) {
                materialIndices[primitive->getMaterial()] = (*nextIndex)++;
            }
        }
    }

    virtual void serializeAndPush(std::vector<unsigned int>& v, bool isRoot, std::vector<std::shared_ptr<VPrimitiveContainer>>& proxies,
                                  std::vector<unsigned int>& proxyIndices, std::vector<std::shared_ptr<VSpaceTransformerContainer>>& parentST,
                                  const std::unordered_map<std::shared_ptr<Material>, unsigned int>& materialIndices) const = 0;

    void setProxy(std::shared_ptr<VPrimitiveContainer> proxy) {
        m_proxy = proxy;
    }

    void addChildCombiner(std::shared_ptr<VCombinerContainer> child) {
        m_childCombiners.push_back(child);
    }

    void removeChildCombiner(std::shared_ptr<VCombinerContainer> child) {
        m_childCombiners.erase(
            std::remove(m_childCombiners.begin(), m_childCombiners.end(), child),
            m_childCombiners.end());
    }

    void addChildPrimitive(std::shared_ptr<VPrimitiveContainer> child) {
        m_childPrimitives.push_back(child);
    }

    void removeChildPrimitive(std::shared_ptr<VPrimitiveContainer> child) {
        m_childPrimitives.erase(
            std::remove(m_childPrimitives.begin(), m_childPrimitives.end(), child),
            m_childPrimitives.end());
    }
};

template<typename T>
class CombinerContainer : public VCombinerContainer {
    static_assert(std::is_base_of<Combiner<T>, T>::value, "T must be a descendant of Combiner<T>");
public:
    T combiner;

    CombinerContainer(T combiner) : combiner(combiner) {}

    static std::shared_ptr<CombinerContainer<T>> createNew(T combiner) {
        return std::shared_ptr<CombinerContainer<T>>(new CombinerContainer<T>(combiner));
    }

    virtual void serializeAndPush(std::vector<unsigned int>& v, bool isRoot, std::vector<std::shared_ptr<VPrimitiveContainer>>& proxies,
                                  std::vector<unsigned int>& proxyIndices, std::vector<std::shared_ptr<VSpaceTransformerContainer>>& parentST,
                                  const std::unordered_map<std::shared_ptr<Material>, unsigned int>& materialIndices) const {
        auto lt = SpaceTransformerContainer<LinearTransform>::createNew(LinearTransform(m_position, m_orientation));
        parentST.push_back(lt);
        parentST.insert(parentST.end(), m_spaceTransformers.begin(), m_spaceTransformers.end());
        
        if (m_proxy != nullptr) {
            proxies.push_back(m_proxy);
            proxyIndices.push_back(0);
        }

        if (m_childCombiners.size() >= 1) {
            // Transmit proxies to first child
            m_childCombiners[0]->serializeAndPush(v, false, proxies, proxyIndices, parentST, materialIndices);
        }
        for (int i=1; i<m_childCombiners.size(); ++i) {
            std::vector<std::shared_ptr<VPrimitiveContainer>> proxyVec;
            std::vector<unsigned int> proxyIndicesVec;
            m_childCombiners[i]->serializeAndPush(v, false, proxyVec, proxyIndicesVec, parentST, materialIndices);
        }

        parentST.resize(parentST.size()-m_spaceTransformers.size()-1);

        bool isLeaf = m_childCombiners.size() == 0;

        // Try to merge linear transforms
        int lastNonLinearSTIndex = parentST.size() - 1;
        for (; lastNonLinearSTIndex>=0; --lastNonLinearSTIndex) {
            if (auto ltContainer = dynamic_cast<SpaceTransformerContainer<LinearTransform>*>(parentST[lastNonLinearSTIndex].get())) {
                lt->spaceTransformer.setOrientation(ltContainer->spaceTransformer.getOrientation() * lt->spaceTransformer.getOrientation());
                lt->spaceTransformer.setPosition(ltContainer->spaceTransformer.getPosition() + (ltContainer->spaceTransformer.getOrientation() * lt->spaceTransformer.getPosition()));
            } else {
                break;
            }
        }

        // 1. CombinerHeader
        CombinerHeader header(T::id(), isRoot, isLeaf ? proxies.size() : 0,
                              m_childCombiners.size(),
                              m_childPrimitives.size(),
                              lastNonLinearSTIndex + 1,
                              m_spaceTransformers.size(),
                              m_distanceTransformers.size());

        v.push_back(header.serialize());

        // 2. [Parent space transformers]
        unsigned int parentStHeaderIndex;
        for (int i=0; i<=lastNonLinearSTIndex; ++i) {
            if (i % 4 == 0) {
                parentStHeaderIndex = v.size();
                v.push_back(0);
            }
            v[parentStHeaderIndex] |= (parentST[i]->getHeader() & 0xFF) << (8 * (3 - i));
            parentST[i]->serializeAndPush(v);
        }

        // 3. LinearTransform
        lt->serializeAndPush(v);

        // 4. [Proxy primitives]
        if (isLeaf) {
            for (int i=0; i<proxies.size(); ++i) {
                proxyIndices[i] = v.size();
                v.push_back(0);
                proxies[i]->serializeAndPush(v, materialIndices);
            }
        }

        // 5. [Space transformers]
        unsigned int stHeaderIndex;
        for (int i=0; i<m_spaceTransformers.size(); ++i) {
            if (i % 4 == 0) {
                stHeaderIndex = v.size();
                v.push_back(0);
            }
            v[stHeaderIndex] |= (m_spaceTransformers[i]->getHeader() & 0xFF) << (8 * (3 - i));
            m_spaceTransformers[i]->serializeAndPush(v);
        }

        // 6. Combiner data
        combiner.serialize(v);

        // 7. [Child primitives]
        for (auto primitive : m_childPrimitives) {
            primitive->serializeAndPush(v, materialIndices);
        }

        // 8. [Distance transformers]
        unsigned int dtHeaderIndex;
        for (int i=0; i<m_distanceTransformers.size(); ++i) {
            if (i % 4 == 0) {
                dtHeaderIndex = v.size();
                v.push_back(0);
            }
            v[dtHeaderIndex] |= (m_distanceTransformers[i]->getHeader() & 0xFF) << (8 * (3 - i));
            m_distanceTransformers[i]->serializeAndPush(v);
        }

        // Set proxy skip index
        if (m_proxy != nullptr) {
            v[proxyIndices.back()] = ProxyHeader(isRoot, v.size()).serialize();
            proxyIndices.pop_back();
            proxies.pop_back();
        }
    }
};

class VLightContainer {
public:
    virtual void serializeAndPush(std::vector<unsigned int>& v) const = 0;
};

template<typename T>
class LightContainer : public VLightContainer {
    static_assert(std::is_base_of<Light<T>, T>::value, "T must be a descendant of Light<T>");
public:
    T light;

    LightContainer(T light) : light(light) {}

    static std::shared_ptr<LightContainer<T>> createNew(T light) {
        return std::shared_ptr<LightContainer<T>>(new LightContainer<T>(light));
    }

    virtual void serializeAndPush(std::vector<unsigned int>& v) const {
        v.push_back(LightHeader(T::id()).serialize());
        light.serialize(v);
    }
};

class Scene {
protected:
    std::shared_ptr<Camera> m_camera;
    std::vector<std::shared_ptr<VCombinerContainer>> m_combiners;
    std::vector<std::shared_ptr<VLightContainer>> m_lights;

public:
    Scene() = default;

    void setCamera(std::shared_ptr<Camera> camera) { m_camera = camera; }
    std::shared_ptr<Camera> getCamera() { return m_camera; }

    void addCombiner(std::shared_ptr<VCombinerContainer> child) {
        m_combiners.push_back(child);
    }

    void removeCombiner(std::shared_ptr<VCombinerContainer> child) {
        m_combiners.erase(
            std::remove(m_combiners.begin(), m_combiners.end(), child),
            m_combiners.end());
    }

    void addLight(std::shared_ptr<VLightContainer> light) {
        m_lights.push_back(light);
    }

    void removeLight(std::shared_ptr<VLightContainer> light) {
        m_lights.erase(
            std::remove(m_lights.begin(), m_lights.end(), light),
            m_lights.end());
    }

    void sendToDevice(unsigned int** cameraPtr, unsigned int** sceneGeomertyPtr, unsigned int* sceneGeomertySize,
                      unsigned int** sceneLightsPtr, unsigned int* sceneLightsSize, unsigned int* sceneLightsCount,
                      Material** sceneMaterialsPtr, unsigned int* sceneMaterialsSize) const {
        std::unordered_map<std::shared_ptr<Material>, unsigned int> materialIndices;
        materialIndices[nullptr] = 0;
        unsigned int materialCount = 1;
        for (auto combiner : m_combiners) {
            combiner->mergeMaterials(materialIndices, &materialCount);
        }
        std::vector<Material> sceneMaterialsVec(materialCount, Material(glm::vec3(1.0f), 0.0f, 0.4f));
        for (auto const& idx : materialIndices) {
            if (idx.first != nullptr) {
                sceneMaterialsVec[idx.second] = *(idx.first);
            }
        }
        
        *sceneMaterialsSize = materialCount * sizeof32b(Material);
        cudaMalloc(sceneMaterialsPtr, materialCount * sizeof(Material));
        cudaMemcpy(*sceneMaterialsPtr, &sceneMaterialsVec[0],
                   materialCount * sizeof(Material), cudaMemcpyHostToDevice);

        std::vector<unsigned int> cameraVec;
        m_camera->serialize(cameraVec);
        cudaMalloc(cameraPtr, cameraVec.size() * sizeof(unsigned int));
        cudaMemcpy(*cameraPtr, &cameraVec[0],
                   cameraVec.size() * sizeof(unsigned int), cudaMemcpyHostToDevice);

        std::vector<unsigned int> sceneGeomertyVec;

        for (auto combiner : m_combiners) {
            std::vector<std::shared_ptr<VPrimitiveContainer>> proxies;
            std::vector<unsigned int> proxyIndices;
            std::vector<std::shared_ptr<VSpaceTransformerContainer>> spaceTransformers;
            combiner->serializeAndPush(sceneGeomertyVec, true, proxies, proxyIndices, spaceTransformers, materialIndices);
        }

        CombinerHeader endHeader(CombinerId::End, true, 0, 0, 0, 0, 0, 0);
        sceneGeomertyVec.push_back(endHeader.serialize());

        *sceneGeomertySize = sceneGeomertyVec.size();
        cudaMalloc(sceneGeomertyPtr, sceneGeomertyVec.size() * sizeof(unsigned int));
        cudaMemcpy(*sceneGeomertyPtr, &sceneGeomertyVec[0],
                   sceneGeomertyVec.size() * sizeof(unsigned int), cudaMemcpyHostToDevice);
        
        std::vector<unsigned int> sceneLightsVec;

        for (auto light : m_lights) {
            light->serializeAndPush(sceneLightsVec);
        }

        *sceneLightsSize = sceneLightsVec.size();
        *sceneLightsCount = m_lights.size();
        cudaMalloc(sceneLightsPtr, sceneLightsVec.size() * sizeof(unsigned int));
        cudaMemcpy(*sceneLightsPtr, &sceneLightsVec[0],
                   sceneLightsVec.size() * sizeof(unsigned int), cudaMemcpyHostToDevice);
    }
};

#endif