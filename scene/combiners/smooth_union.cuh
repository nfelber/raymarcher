#ifndef SOFT_UNION_h
#define SOFT_UNION_h

#include "combiner.cuh"

class SmoothUnion : public Combiner<SmoothUnion> {
protected:
	float m_smoothness = 0.0f;

public:
	class SmoothUnionAccumulator : Accumulator<SmoothUnion> {
	protected:
		float m_smoothness = 0.0f;
		float m_d0 = MAXFLOAT;
		float m_d1 = MAXFLOAT;
		unsigned int m_hit;

	public:
		__host__ __device__ SmoothUnionAccumulator(float smoothness) : m_smoothness(smoothness) {}

		__host__ __device__ void accumulate(float d, unsigned int hit) {
			if (d < m_d0) {
				m_d1 = m_d0;
				m_d0 = d;
				m_hit = hit;
			}
			else if (d < m_d1) {
				m_d1 = d;
			}
		}

		__host__ __device__ float getDistance() {
			float h = max(m_smoothness - (m_d1 - m_d0), 0.0f);
			return m_d0 - h * h / (4.0f * m_smoothness);
		}

		__host__ __device__ unsigned int getHit() {
			return m_hit;
		}
	};

	__host__ __device__ static CombinerId id() {
		return CombinerId::SmoothUnion;
	}
	__host__ __device__ SmoothUnion() = default;
	__host__ __device__ SmoothUnion(float smoothness) : m_smoothness(smoothness) {}

	__host__ __device__ SmoothUnionAccumulator getAccumulator() {
		return SmoothUnionAccumulator(m_smoothness);
	}
};

#endif