#ifndef UNION_h
#define UNION_h

#include "combiner.cuh"

class Union : public Combiner<Union> {
public:
	class UnionAccumulator : Accumulator<Union> {
	private:
		float m_distance = MAXFLOAT;
		unsigned int m_hit;
	public:
		__host__ __device__ UnionAccumulator() = default;

		__host__ __device__ void accumulate(float d, unsigned int hit) {
			if (d < m_distance) {
				m_distance = d;
				m_hit = hit;
			}
		}

		__host__ __device__ float getDistance() {
			return m_distance;
		}

		__host__ __device__ unsigned int getHit() {
			return m_hit;
		}
	};

	__host__ __device__ static bool enableDirectionalDistance() { return true; }
	__host__ __device__ static bool enablePrimitiveHit() { return true; }

	__host__ __device__ static CombinerId id() {
        return CombinerId::Union;
    }
	__host__ __device__ Union() = default;

	__host__ __device__ UnionAccumulator getAccumulator() {
        return UnionAccumulator();
    }
};

#endif