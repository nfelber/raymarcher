#ifndef COMBINER_h
#define COMBINER_h

#include "../scene.cuh"

enum class CombinerId : uint8_t {
	Union,
	SmoothUnion,
	End
};

template <typename T>
class Combiner : public Serializable<T> {
public:
	template <typename D>
	class Accumulator {
	public:
		__host__ __device__ void accumulate(float d, unsigned int hit) {
			return static_cast<Accumulator<D>*>(this)->accumulate(d, hit);
		}

		__host__ __device__ float getDistance() {
			return static_cast<Accumulator<D>*>(this)->getDistance();
		}

		__host__ __device__ unsigned int getClosestHit() {
			return static_cast<Accumulator<D>*>(this)->getClosestHit();
		}
	};

	__host__ __device__ static bool enableDirectionalDistance() {
        return false;
    }

	__host__ __device__ static bool enablePrimitiveHit() {
        return false;
    }

	__host__ __device__ static CombinerId id() {
        return T::id();
    }

	__host__ __device__ Accumulator<T> getAccumulator() {
        return static_cast<T*>(this)->getAccumulator();
    }
};

#include "union.cuh"
#include "smooth_union.cuh"

#define uint2CombinerId(value) static_cast<CombinerId>(value)
#define combinerId2uint(value) static_cast<unsigned int>(value)

template<class F> __host__ __device__ void callOnCombinerId(CombinerId id, F f) {
	switch (id) {
		case CombinerId::Union:
			return f.template operator()<Union>();
		case CombinerId::SmoothUnion:
			return f.template operator()<SmoothUnion>();
	}
}

struct CombinerHeader {
	CombinerId id; // 5 bits
	bool isRoot; // 1 bit
	unsigned int proxyCount; // 4 bit
	unsigned int childCombinerCount; // 3 bits
	unsigned int childPrimitiveCount; // 8 bits
    unsigned int parentSpaceTransformerCount; // 5 bits
	unsigned int spaceTransformerCount; // 3 bits
	unsigned int distanceTransformerCount; // 3 bits

	__host__ __device__ CombinerHeader(CombinerId id, bool isRoot,
									   unsigned int proxyCount,
									   unsigned int childCombinerCount,
									   unsigned int childPrimitiveCount,
    								   unsigned int parentSpaceTransformerCount,
									   unsigned int spaceTransformerCount,
									   unsigned int distanceTransformerCount) :
									   		id(id), isRoot(isRoot), proxyCount(proxyCount),
									   		childCombinerCount(childCombinerCount),
									   		childPrimitiveCount(childPrimitiveCount),
									   		parentSpaceTransformerCount(parentSpaceTransformerCount),
									   		spaceTransformerCount(spaceTransformerCount),
									   		distanceTransformerCount(distanceTransformerCount) {}

	__host__ __device__ unsigned int serialize() {
		return (combinerId2uint(id) << 27) |
			   (isRoot << 26) |
			   (proxyCount & 0xF) << 22 |
			   (childCombinerCount & 0x7) << 19 |
			   (childPrimitiveCount & 0xFF) << 11 |
			   (parentSpaceTransformerCount & 0x1F) << 6 |
			   (spaceTransformerCount & 0x7) << 3 |
			   (distanceTransformerCount & 0x7);
	}

	__host__ __device__ static CombinerHeader deserialize(unsigned int raw) {
		return CombinerHeader {
			uint2CombinerId(raw >> 27),
			(raw & 0x4000000) != 0,
			(raw >> 22) & 0xF,
			(raw >> 19) & 0x7,
			(raw >> 11) & 0xFF,
			(raw >> 6) & 0x1F,
			(raw >> 3) & 0x7,
			raw & 0x7
		};
	}
};

struct ProxyHeader {
	bool isRoot;
	unsigned int skipIndex; // 31 bits

	__host__ __device__ ProxyHeader(bool isRoot, unsigned int skipIndex) : isRoot(isRoot), skipIndex(skipIndex) {}

	__host__ __device__ unsigned int serialize() {
		return (isRoot << 31) |
			   (skipIndex & 0x7FFFFFFF);
	}

	__host__ __device__ static ProxyHeader deserialize(unsigned int raw) {
		return ProxyHeader{
			(raw & 0x80000000) != 0,
			raw & 0x7FFFFFFF
		};
	}
};

#endif