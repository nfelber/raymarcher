#ifndef SCENE3_h
#define SCENE3_h

#include "test_scene.cuh"

class Scene3 : public TestScene {
protected:
    std::shared_ptr<Material> m_floorMaterial;
    std::shared_ptr<Material> m_lightRedMaterial;
    std::shared_ptr<Material> m_lightGreenMaterial;
    std::shared_ptr<Material> m_lightSkyBlueMaterial;
    std::shared_ptr<Material> m_lightPurpleMaterial;
    std::shared_ptr<Material> m_redMaterial;
    std::shared_ptr<Material> m_orangeMaterial;
    std::shared_ptr<Material> m_yellowMaterial;
    std::shared_ptr<Material> m_limeMaterial;
    std::shared_ptr<Material> m_greenMaterial;
    std::shared_ptr<Material> m_seaGreenMaterial;
    std::shared_ptr<Material> m_skyBlueMaterial;
    std::shared_ptr<Material> m_lapisMaterial;
    std::shared_ptr<Material> m_blueMaterial;
    std::shared_ptr<Material> m_purpleMaterial;
    std::shared_ptr<Material> m_magentaMaterial;
    std::shared_ptr<Material> m_hotPinkMaterial;

    std::shared_ptr<PrimitiveContainer<Plane>> m_plane;
    std::shared_ptr<PrimitiveContainer<Sphere>> m_sphere;
    std::shared_ptr<PrimitiveContainer<Box>> m_box;
    std::shared_ptr<PrimitiveContainer<BoxFrame>> m_boxFrame;
	std::shared_ptr<PrimitiveContainer<Capsule>> m_capsule;
	std::shared_ptr<PrimitiveContainer<Cone>> m_cone;
	std::shared_ptr<PrimitiveContainer<CappedCone>> m_cappedCone;
	std::shared_ptr<PrimitiveContainer<RoundCone>> m_roundCone;
	std::shared_ptr<PrimitiveContainer<CutHollowSphere>> m_cutHollowSphere;
	std::shared_ptr<PrimitiveContainer<Cylinder>> m_cylinder;
	std::shared_ptr<PrimitiveContainer<Ellipsoid>> m_ellipsoid;
	std::shared_ptr<PrimitiveContainer<Link>> m_link;
	std::shared_ptr<PrimitiveContainer<Octahedron>> m_octahedron;
	std::shared_ptr<PrimitiveContainer<Pyramid>> m_pyramid;
	std::shared_ptr<PrimitiveContainer<SolidAngle>> m_solidAngle;
	std::shared_ptr<PrimitiveContainer<Torus>> m_torus;
	std::shared_ptr<PrimitiveContainer<CappedTorus>> m_cappedTorus;

    std::shared_ptr<CombinerContainer<Union>> m_planeContainer;
    std::shared_ptr<CombinerContainer<Union>> m_primitiveCombiner;
    std::shared_ptr<CombinerContainer<Union>> m_primitiveSubCombiner1;
    std::shared_ptr<CombinerContainer<Union>> m_primitiveSubCombiner2;
    std::shared_ptr<CombinerContainer<Union>> m_primitiveSubCombiner3;
    std::shared_ptr<CombinerContainer<Union>> m_primitiveSubCombiner4;

    std::shared_ptr<PrimitiveContainer<Box>> m_primitiveProxy;
    std::shared_ptr<PrimitiveContainer<Box>> m_primitiveSubProxy1;
    std::shared_ptr<PrimitiveContainer<Box>> m_primitiveSubProxy2;
    std::shared_ptr<PrimitiveContainer<Box>> m_primitiveSubProxy3;
    std::shared_ptr<PrimitiveContainer<Box>> m_primitiveSubProxy4;
    std::shared_ptr<PrimitiveContainer<BoxFrame>> m_primitiveProxyFrame;
    std::shared_ptr<PrimitiveContainer<BoxFrame>> m_primitiveSubProxyFrame1;
    std::shared_ptr<PrimitiveContainer<BoxFrame>> m_primitiveSubProxyFrame2;
    std::shared_ptr<PrimitiveContainer<BoxFrame>> m_primitiveSubProxyFrame3;
    std::shared_ptr<PrimitiveContainer<BoxFrame>> m_primitiveSubProxyFrame4;

    std::shared_ptr<LightContainer<DirectionalLight>> m_light1;
    std::shared_ptr<LightContainer<DirectionalLight>> m_light2;

    std::shared_ptr<DistanceTransformerContainer<VerticalDisplacementMap>> m_displacement;
    
    Texture m_floorDiffTex = Texture("../textures/marble_albedo.jpg");
    Texture m_floorMetalTex = Texture("../textures/marble_metal.jpg");
    Texture m_floorRoughTex = Texture("../textures/marble_rough.jpg");
    Texture m_floorDispTex = Texture("../textures/marble_disp.jpg");

    double m_time;
    bool m_usingProxy = true;
    bool m_usingSubProxies = true;
    bool m_proxyFramesVisible = false;
    bool m_softShadows = true;
    bool m_displacementMap = false;

public:
    virtual void setup() {
        m_floorDispTex.uploadToDevice();
        m_floorDiffTex.uploadToDevice();
        m_floorMetalTex.uploadToDevice();
        m_floorRoughTex.uploadToDevice();
        m_floorMaterial = std::shared_ptr<Material>(new Material(glm::vec3(1.0f), 0.0f, 0.2f));
        m_floorMaterial->albedoTex = m_floorDiffTex.getTexObj();
        m_floorMaterial->metalnessTex = m_floorMetalTex.getTexObj();
        m_floorMaterial->roughnessTex = m_floorRoughTex.getTexObj();
        m_floorMaterial->textureScale = 3.0f;
        m_displacement = DistanceTransformerContainer<VerticalDisplacementMap>::createNew(
            VerticalDisplacementMap(m_floorDispTex.getTexObj(), 6.0f, 0.05f));

        float metalness = 1.0f;
        float roughness = 0.2f;
        m_lightRedMaterial = std::shared_ptr<Material>(new Material(glm::vec3(1.0f, 0.6f, 0.6f), metalness, roughness));
        m_lightGreenMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.8f, 1.0f, 0.6f), metalness, roughness));
        m_lightSkyBlueMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.6f, 1.0f, 1.0f), metalness, roughness));
        m_lightPurpleMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.8f, 0.6f, 1.0f), metalness, roughness));
        m_redMaterial = std::shared_ptr<Material>(new Material(glm::vec3(1.0f, 0.0f, 0.0f), metalness, roughness));
        m_orangeMaterial = std::shared_ptr<Material>(new Material(glm::vec3(1.0f, 0.5f, 0.0f), metalness, roughness));
        m_yellowMaterial = std::shared_ptr<Material>(new Material(glm::vec3(1.0f, 1.0f, 0.0f), metalness, roughness));
        m_limeMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.5f, 1.0f, 0.0f), metalness, roughness));
        m_greenMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.0f, 1.0f, 0.0f), metalness, roughness));
        m_seaGreenMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.0f, 1.0f, 0.5f), metalness, roughness));
        m_skyBlueMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.0f, 1.0f, 1.0f), metalness, roughness));
        m_lapisMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.0f, 0.5f, 1.0f), metalness, roughness));
        m_blueMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.0f, 0.0f, 1.0f), metalness, roughness));
        m_purpleMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.5f, 0.0f, 1.0f), metalness, roughness));
        m_magentaMaterial = std::shared_ptr<Material>(new Material(glm::vec3(1.0f, 0.0f, 1.0f), metalness, roughness));
        m_hotPinkMaterial = std::shared_ptr<Material>(new Material(glm::vec3(1.0f, 0.0f, 0.5f), metalness, roughness));

        m_sphere = PrimitiveContainer<Sphere>::createNew(Sphere(1.0f));
        m_box = PrimitiveContainer<Box>::createNew(Box(glm::vec3(0.8f)));
        m_plane = PrimitiveContainer<Plane>::createNew(Plane(glm::vec3(0.0f, 1.0f, 0.0f)));
        m_boxFrame = PrimitiveContainer<BoxFrame>::createNew(BoxFrame(glm::vec3(1.0f, 0.5f, 0.8f), 0.1f));
        m_capsule = PrimitiveContainer<Capsule>::createNew(Capsule(0.25f, 1.0f));
        m_cone = PrimitiveContainer<Cone>::createNew(Cone(glm::radians(30.0f), 1.0f));
        m_cappedCone = PrimitiveContainer<CappedCone>::createNew(CappedCone(1.0f, 0.2f, 1.0f));
        m_roundCone = PrimitiveContainer<RoundCone>::createNew(RoundCone(0.2f, 0.5f, 1.0f));
        m_cutHollowSphere = PrimitiveContainer<CutHollowSphere>::createNew(CutHollowSphere(1.0f, 0.3f, 0.05f));
        m_cylinder = PrimitiveContainer<Cylinder>::createNew(Cylinder(0.2f, 1.0f));
        m_ellipsoid = PrimitiveContainer<Ellipsoid>::createNew(Ellipsoid(glm::vec3(1.0f, 0.5f, 0.3f)));
        m_link = PrimitiveContainer<Link>::createNew(Link(0.5f, 0.1f, 0.5f));
        m_octahedron = PrimitiveContainer<Octahedron>::createNew(Octahedron(1.0f));
        m_pyramid = PrimitiveContainer<Pyramid>::createNew(Pyramid(1.0f));
        m_solidAngle = PrimitiveContainer<SolidAngle>::createNew(SolidAngle(1.0f, glm::radians(30.0f)));
        m_torus = PrimitiveContainer<Torus>::createNew(Torus(1.0f, 0.15f));
        m_cappedTorus = PrimitiveContainer<CappedTorus>::createNew(CappedTorus(1.0f, 0.15f, glm::radians(120.0f)));

        m_planeContainer = CombinerContainer<Union>::createNew(Union());
        m_primitiveCombiner = CombinerContainer<Union>::createNew(Union());
        m_primitiveSubCombiner1 = CombinerContainer<Union>::createNew(Union());
        m_primitiveSubCombiner2 = CombinerContainer<Union>::createNew(Union());
        m_primitiveSubCombiner3 = CombinerContainer<Union>::createNew(Union());
        m_primitiveSubCombiner4 = CombinerContainer<Union>::createNew(Union());

        float gridSize = 2.5f;

        m_primitiveProxy = PrimitiveContainer<Box>::createNew(Box(glm::vec3(2*gridSize, 0.5 * gridSize, 2*gridSize)));
        m_primitiveProxyFrame = PrimitiveContainer<BoxFrame>::createNew(BoxFrame(glm::vec3(2*gridSize, 0.5 * gridSize, 2*gridSize), 0.025f));

        m_primitiveSubProxy1 = PrimitiveContainer<Box>::createNew(Box(glm::vec3(gridSize, 0.5 * gridSize, gridSize)));
        m_primitiveSubProxyFrame1 = PrimitiveContainer<BoxFrame>::createNew(BoxFrame(glm::vec3(gridSize, 0.5 * gridSize, gridSize), 0.025f));
        m_primitiveSubProxy2 = PrimitiveContainer<Box>::createNew(Box(glm::vec3(gridSize, 0.5 * gridSize, gridSize)));
        m_primitiveSubProxyFrame2 = PrimitiveContainer<BoxFrame>::createNew(BoxFrame(glm::vec3(gridSize, 0.5 * gridSize, gridSize), 0.025f));
        m_primitiveSubProxy3 = PrimitiveContainer<Box>::createNew(Box(glm::vec3(gridSize, 0.5 * gridSize, gridSize)));
        m_primitiveSubProxyFrame3 = PrimitiveContainer<BoxFrame>::createNew(BoxFrame(glm::vec3(gridSize, 0.5 * gridSize, gridSize), 0.025f));
        m_primitiveSubProxy4 = PrimitiveContainer<Box>::createNew(Box(glm::vec3(gridSize, 0.5 * gridSize, gridSize)));
        m_primitiveSubProxyFrame4 = PrimitiveContainer<BoxFrame>::createNew(BoxFrame(glm::vec3(gridSize, 0.5 * gridSize, gridSize), 0.025f));

        m_light1 = LightContainer<DirectionalLight>::createNew(DirectionalLight(glm::vec3(1.0f), 2.0f, 0.2f));
        m_light2 = LightContainer<DirectionalLight>::createNew(DirectionalLight(glm::vec3(1.0f), 2.0f, 0.2f));

        m_plane->translate(glm::vec3(0.0f, -gridSize * 0.5f, 0.0f));
        m_sphere->translate(glm::vec3(0.0f, m_sphere->primitive.getRadius(), 0.0f));
        m_box->translate(glm::vec3(gridSize, m_box->primitive.getDimensions().y, 0.0f));
        m_boxFrame->translate(glm::vec3(2 * gridSize, m_boxFrame->primitive.getDimensions().y + m_boxFrame->primitive.getThickness(), 0.0f));
        m_capsule->translate(glm::vec3(3 * gridSize, m_capsule->primitive.getRadius(), 0.0f));
        m_cone->translate(glm::vec3(0.0f, m_cone->primitive.getHeight(), gridSize));
        m_cappedCone->translate(glm::vec3(gridSize, m_cappedCone->primitive.getHeight(), gridSize));
        m_roundCone->translate(glm::vec3(2 * gridSize, m_roundCone->primitive.getRadius1(), gridSize));
        m_cutHollowSphere->translate(glm::vec3(3 * gridSize, m_cutHollowSphere->primitive.getRadius() + m_cutHollowSphere->primitive.getThickness(), gridSize));
        m_cylinder->translate(glm::vec3(0.0f, m_cylinder->primitive.getHeight(), 2 * gridSize));
        m_ellipsoid->translate(glm::vec3(gridSize, m_ellipsoid->primitive.getSemiAxes().y, 2 * gridSize));
        m_link->translate(glm::vec3(2 * gridSize, m_link->primitive.getLength() + m_link->primitive.getRadius() + m_link->primitive.getThickness(), 2 * gridSize));
        m_octahedron->translate(glm::vec3(3 * gridSize, m_octahedron->primitive.getRadius(), 2 * gridSize));
        m_pyramid->translate(glm::vec3(0.0f, 0.0f, 3 * gridSize));
        m_solidAngle->translate(glm::vec3(gridSize, 0.0f, 3 * gridSize));
        m_torus->translate(glm::vec3(2 * gridSize, m_torus->primitive.getThickness(), 3 * gridSize));
        m_cappedTorus->translate(glm::vec3(3 * gridSize, m_cappedTorus->primitive.getRadius() + m_cappedTorus->primitive.getThickness(), 3 * gridSize));

        m_cappedCone->setMaterial(m_lightRedMaterial);
        m_roundCone->setMaterial(m_lightGreenMaterial);
        m_link->setMaterial(m_lightSkyBlueMaterial);
        m_ellipsoid->setMaterial(m_lightPurpleMaterial);
        m_sphere->setMaterial(m_redMaterial);
        m_box->setMaterial(m_orangeMaterial);
        m_boxFrame->setMaterial(m_yellowMaterial);
        m_capsule->setMaterial(m_limeMaterial);
        m_cutHollowSphere->setMaterial(m_greenMaterial);
        m_octahedron->setMaterial(m_seaGreenMaterial);
        m_cappedTorus->setMaterial(m_skyBlueMaterial);
        m_torus->setMaterial(m_lapisMaterial);
        m_solidAngle->setMaterial(m_blueMaterial);
        m_pyramid->setMaterial(m_purpleMaterial);
        m_cylinder->setMaterial(m_magentaMaterial);
        m_cone->setMaterial(m_hotPinkMaterial);
        m_plane->setMaterial(m_floorMaterial);

        m_primitiveProxy->translate(glm::vec3(1.5 * gridSize, 0.5 * gridSize, 1.5 * gridSize));
        m_primitiveProxyFrame->translate(glm::vec3(1.5 * gridSize, 0.5 * gridSize, 1.5 * gridSize));

        m_primitiveSubProxy1->translate(glm::vec3(0.5 * gridSize, 0.5 * gridSize, 0.5 * gridSize));
        m_primitiveSubProxyFrame1->translate(glm::vec3(0.5 * gridSize, 0.5 * gridSize, 0.5 * gridSize));
        m_primitiveSubProxy2->translate(glm::vec3(2.5 * gridSize, 0.5 * gridSize, 0.5 * gridSize));
        m_primitiveSubProxyFrame2->translate(glm::vec3(2.5 * gridSize, 0.5 * gridSize, 0.5 * gridSize));
        m_primitiveSubProxy3->translate(glm::vec3(0.5 * gridSize, 0.5 * gridSize, 2.5 * gridSize));
        m_primitiveSubProxyFrame3->translate(glm::vec3(0.5 * gridSize, 0.5 * gridSize, 2.5 * gridSize));
        m_primitiveSubProxy4->translate(glm::vec3(2.5 * gridSize, 0.5 * gridSize, 2.5 * gridSize));
        m_primitiveSubProxyFrame4->translate(glm::vec3(2.5 * gridSize, 0.5 * gridSize, 2.5 * gridSize));

        m_light1->light.setOrientation(glm::angleAxis(glm::radians(30.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
        m_light2->light.setOrientation(glm::angleAxis(glm::radians(45.0f), glm::vec3(0.0f, 0.0f, 1.0f)));

        m_primitiveSubCombiner1->addChildPrimitive(m_sphere);
        m_primitiveSubCombiner1->addChildPrimitive(m_box);
        m_primitiveSubCombiner2->addChildPrimitive(m_boxFrame);
        m_primitiveSubCombiner2->addChildPrimitive(m_capsule);
        m_primitiveSubCombiner1->addChildPrimitive(m_cone);
        m_primitiveSubCombiner1->addChildPrimitive(m_cappedCone);
        m_primitiveSubCombiner2->addChildPrimitive(m_roundCone);
        m_primitiveSubCombiner2->addChildPrimitive(m_cutHollowSphere);
        m_primitiveSubCombiner3->addChildPrimitive(m_cylinder);
        m_primitiveSubCombiner3->addChildPrimitive(m_ellipsoid);
        m_primitiveSubCombiner4->addChildPrimitive(m_link);
        m_primitiveSubCombiner4->addChildPrimitive(m_octahedron);
        m_primitiveSubCombiner3->addChildPrimitive(m_pyramid);
        m_primitiveSubCombiner3->addChildPrimitive(m_solidAngle);
        m_primitiveSubCombiner4->addChildPrimitive(m_torus);
        m_primitiveSubCombiner4->addChildPrimitive(m_cappedTorus);

        m_primitiveSubCombiner1->setProxy(m_primitiveSubProxy1);
        m_primitiveSubCombiner2->setProxy(m_primitiveSubProxy2);
        m_primitiveSubCombiner3->setProxy(m_primitiveSubProxy3);
        m_primitiveSubCombiner4->setProxy(m_primitiveSubProxy4);

        m_primitiveCombiner->translate(glm::vec3(-1.5, -0.5, -1.5) * gridSize);
        m_primitiveCombiner->setProxy(m_primitiveProxy);
        m_primitiveCombiner->addChildCombiner(m_primitiveSubCombiner1);
        m_primitiveCombiner->addChildCombiner(m_primitiveSubCombiner2);
        m_primitiveCombiner->addChildCombiner(m_primitiveSubCombiner3);
        m_primitiveCombiner->addChildCombiner(m_primitiveSubCombiner4);

        m_planeContainer->addChildPrimitive(m_plane);

        addCombiner(m_primitiveCombiner);
        addCombiner(m_planeContainer);
        addLight(m_light1);
        addLight(m_light2);

        m_camera = std::shared_ptr<Camera>(new Camera(glm::radians(45.0f), 1.0f, 0.1f, 100.0f));
        m_camera->translate(glm::vec3(0.0, 1.0, gridSize * 3));
    }

    virtual void update(double dt) {
        m_time += dt;
        m_light1->light.rotate(glm::angleAxis(0.2f * (float) dt, glm::vec3(0.0f, 1.0f, 0.0f)));
        m_light2->light.rotate(glm::angleAxis(0.1f * (float) dt, glm::vec3(0.0f, 1.0f, 0.0f)));
    }

    virtual void keyEvent(int key, int scancode, int action, int mods) {
        if (key == GLFW_KEY_P && action == GLFW_PRESS) {
            if (mods & GLFW_MOD_SHIFT) {
                m_usingSubProxies = !m_usingSubProxies;
                m_primitiveSubCombiner1->setProxy(m_usingSubProxies ? m_primitiveSubProxy1 : nullptr);
                m_primitiveSubCombiner2->setProxy(m_usingSubProxies ? m_primitiveSubProxy2 : nullptr);
                m_primitiveSubCombiner3->setProxy(m_usingSubProxies ? m_primitiveSubProxy3 : nullptr);
                m_primitiveSubCombiner4->setProxy(m_usingSubProxies ? m_primitiveSubProxy4 : nullptr);
            } else {
                m_usingProxy = !m_usingProxy;
                m_primitiveCombiner->setProxy(m_usingProxy ? m_primitiveProxy : nullptr);
            }
        }
        if (key == GLFW_KEY_V && action == GLFW_PRESS) {
            m_proxyFramesVisible = !m_proxyFramesVisible;
            if (m_proxyFramesVisible) {
                m_primitiveCombiner->addChildPrimitive(m_primitiveProxyFrame);
                m_primitiveSubCombiner1->addChildPrimitive(m_primitiveSubProxyFrame1);
                m_primitiveSubCombiner2->addChildPrimitive(m_primitiveSubProxyFrame2);
                m_primitiveSubCombiner3->addChildPrimitive(m_primitiveSubProxyFrame3);
                m_primitiveSubCombiner4->addChildPrimitive(m_primitiveSubProxyFrame4);
            } else {
                m_primitiveCombiner->removeChildPrimitive(m_primitiveProxyFrame);
                m_primitiveSubCombiner1->removeChildPrimitive(m_primitiveSubProxyFrame1);
                m_primitiveSubCombiner2->removeChildPrimitive(m_primitiveSubProxyFrame2);
                m_primitiveSubCombiner3->removeChildPrimitive(m_primitiveSubProxyFrame3);
                m_primitiveSubCombiner4->removeChildPrimitive(m_primitiveSubProxyFrame4);
            }
        }
        if (key == GLFW_KEY_O && action == GLFW_PRESS) {
            m_softShadows = !m_softShadows;
            if (m_softShadows) {
                m_light1->light.setSoftness(0.2f);
                m_light2->light.setSoftness(0.2f);
            } else {
                m_light1->light.setSoftness(0.0f);
                m_light2->light.setSoftness(0.0f);
            }
        }
        if (key == GLFW_KEY_T && action == GLFW_PRESS) {
            m_displacementMap = !m_displacementMap;
            if (m_displacementMap) {
                m_plane->addDistanceTransformer(m_displacement);
                // m_plane->setMaterial(m_floorMaterial);
            } else {
                m_plane->removeDistanceTransformer(m_displacement);
                // m_plane->setMaterial(nullptr);
            }
        }
    }
};

#endif