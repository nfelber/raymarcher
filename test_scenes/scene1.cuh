#ifndef SCENE1_h
#define SCENE1_h

#include "test_scene.cuh"

class Scene1 : public TestScene {
protected:
    std::shared_ptr<Material> m_sphereMaterial;
    std::shared_ptr<Material> m_boxMaterial;

    std::shared_ptr<SpaceTransformerContainer<InfiniteGrid>> m_grid;
    std::shared_ptr<SpaceTransformerContainer<Twist>> m_twist;
    std::shared_ptr<DistanceTransformerContainer<Round>> m_rounding;

    std::shared_ptr<PrimitiveContainer<Sphere>> m_sphere;
    std::shared_ptr<PrimitiveContainer<Box>> m_box;
    std::shared_ptr<CombinerContainer<Union>> m_root;

    std::shared_ptr<LightContainer<PointLight>> m_light;

    double m_time;

public:
    virtual void setup() {
        m_sphereMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.714f, 1.0f, 0.55f), 0.0f, 0.05f));
        m_boxMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.925f, 0.55f, 1.0f), 1.0, 0.2));

        m_grid = SpaceTransformerContainer<InfiniteGrid>::createNew(InfiniteGrid(glm::vec3(4.0f, 8.0f, 4.0f)));
        m_twist = SpaceTransformerContainer<Twist>::createNew(Twist(1.0f));
        m_rounding = DistanceTransformerContainer<Round>::createNew(Round(0.2f));

        m_sphere = PrimitiveContainer<Sphere>::createNew(Sphere(1.0f));
        m_box = PrimitiveContainer<Box>::createNew(Box(glm::vec3(0.8f)));
        m_root = CombinerContainer<Union>::createNew(Union());

        m_light = LightContainer<PointLight>::createNew(PointLight(glm::vec3(1.0f), 100.0f, 0.2f));

        m_sphere->translate(glm::vec3(0.0f, 2.0f, 0.0f));
        m_sphere->setMaterial(m_sphereMaterial);

        m_box->translate(glm::vec3(0.0f, -2.0f, 0.0f));
        m_box->addSpaceTransformer(m_twist);
        m_box->addDistanceTransformer(m_rounding);
        m_box->setMaterial(m_boxMaterial);

        m_root->addChildPrimitive(m_sphere);
        m_root->addChildPrimitive(m_box);
        m_root->addSpaceTransformer(m_grid);

        addCombiner(m_root);
        addLight(m_light);

        m_camera = std::shared_ptr<Camera>(new Camera(glm::radians(45.0f), 1.0f, 0.1f, 100.0f));
        m_camera->translate(glm::vec3(0.0, 0.0, 0.0));
    }

    virtual void update(double dt) {
        m_time += dt;
        m_box->rotate(glm::angleAxis(0.2f * (float)dt, glm::vec3(1.0f, 0.0f, 0.0f)));
        m_root->rotate(glm::angleAxis(0.2f * (float)dt, glm::vec3(0.0f, 1.0f, 0.0f)));
        m_twist->spaceTransformer.setAmount(0.5 * 1.57 * (sin(m_time) + 1.0));
        m_light->light.setPosition(m_camera->getPosition());
    }

    virtual void keyEvent(int key, int scancode, int action, int mods) {}
};

#endif