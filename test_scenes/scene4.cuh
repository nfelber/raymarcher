#ifndef SCENE4_h
#define SCENE4_h

#include "test_scene.cuh"

class Scene4 : public TestScene {
protected:
    std::shared_ptr<Material> m_juliaMaterial;

    std::shared_ptr<PrimitiveContainer<Plane>> m_plane;
    std::shared_ptr<PrimitiveContainer<Julia>> m_julia;
    std::shared_ptr<CombinerContainer<Union>> m_root;

    std::shared_ptr<LightContainer<DirectionalLight>> m_light;

    Texture m_chemicalAlbedoTex = Texture("../textures/chemical_albedo.jpg");

    double m_time;

public:
    virtual void setup() {
        // m_juliaMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.7f, 0.0f, 0.55f), 1.0f, 0.3f));
        m_chemicalAlbedoTex.uploadToDevice();
        m_juliaMaterial = std::shared_ptr<Material>(new Material(glm::vec3(1.0f), 0.5f, 0.2f));
        m_juliaMaterial->albedoTex = m_chemicalAlbedoTex.getTexObj();
        m_juliaMaterial->textureScale = 2.0f;
        m_juliaMaterial->triPlanarNormalWeight = 4.0f;

        m_plane = PrimitiveContainer<Plane>::createNew(Plane(glm::vec3(0.0f, 1.0f, 0.0f)));
        m_julia = PrimitiveContainer<Julia>::createNew(Julia(glm::quat(-2.0f, 6.0f, 15.0f, -6.0f) / 22.0f, 0.5f));
        m_root = CombinerContainer<Union>::createNew(Union());

        m_julia->translate(glm::vec3(0.0f, 1.0f, 0.0f));
        m_julia->setMaterial(m_juliaMaterial);

        m_light = LightContainer<DirectionalLight>::createNew(DirectionalLight(glm::vec3(1.0f), 3.0f, 0.2f));
        m_light->light.setOrientation(glm::angleAxis(glm::radians(30.0f), glm::vec3(1.0f, 0.0f, 0.0f)));

        m_root->addChildPrimitive(m_plane);
        m_root->addChildPrimitive(m_julia);

        addCombiner(m_root);
        addLight(m_light);

        m_camera = std::shared_ptr<Camera>(new Camera(glm::radians(45.0f), 1.0f, 0.1f, 100.0f));
        m_camera->translate(glm::vec3(0.0, 1.5, 2.5));
    }

    virtual void update(double dt) {
        m_time += dt;
        m_light->light.rotate(glm::angleAxis(0.5f * (float) dt, glm::vec3(0.0f, 1.0f, 0.0f)));
        m_julia->primitive.setCutHeight(-0.6f * cos(0.2f * m_time) + 0.3);
        m_julia->primitive.setkC(normalize(glm::quat(
            sin(0.23 * m_time),
            cos(0.11 * m_time),
            sin(0.13 * m_time),
            cos(0.17 * m_time)
        )) * 0.8f);
    }

    virtual void keyEvent(int key, int scancode, int action, int mods) {}
};

#endif