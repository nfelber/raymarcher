#ifndef SCENE6_h
#define SCENE6_h

#include <glm/gtc/random.hpp>

#include "test_scene.cuh"

class Scene6 : public TestScene {
protected:
    class Ship {
        protected:
            std::shared_ptr<CombinerContainer<Union>> m_root;

            std::shared_ptr<CombinerContainer<SmoothUnion>> m_hullUnion;
            std::shared_ptr<PrimitiveContainer<Ellipsoid>> m_cockpit;
            std::shared_ptr<CombinerContainer<SmoothUnion>> m_symmetryUnion;
            std::shared_ptr<PrimitiveContainer<Ellipsoid>> m_body;
            std::shared_ptr<PrimitiveContainer<Ellipsoid>> m_reactor;
            std::shared_ptr<PrimitiveContainer<Link>> m_reactorSupport;
            std::shared_ptr<PrimitiveContainer<Box>> m_tail;
            std::shared_ptr<PrimitiveContainer<Box>> m_wing;
            std::shared_ptr<SpaceTransformerContainer<Reflection>> m_zreflection;
            std::shared_ptr<SpaceTransformerContainer<Reflection>> m_yreflection;

            std::shared_ptr<CombinerContainer<SmoothUnion>> m_torusUnion;
            std::shared_ptr<PrimitiveContainer<Torus>> m_torus;
            std::shared_ptr<PrimitiveContainer<Sphere>> m_sphere1;
            std::shared_ptr<PrimitiveContainer<Sphere>> m_sphere2;
            std::shared_ptr<PrimitiveContainer<Sphere>> m_sphere3;

            std::shared_ptr<PrimitiveContainer<Box>> m_proxy;

            float m_rotationSpeed;
            float m_speed;
        
        public:
            void setup(Scene6& scene) {
                m_root = CombinerContainer<Union>::createNew(Union());

                m_hullUnion = CombinerContainer<SmoothUnion>::createNew(SmoothUnion(0.5f));
                m_cockpit = PrimitiveContainer<Ellipsoid>::createNew(Ellipsoid(glm::vec3(2.0f, 0.75f, 1.0f)));
                m_symmetryUnion = CombinerContainer<SmoothUnion>::createNew(SmoothUnion(0.3f));
                m_body = PrimitiveContainer<Ellipsoid>::createNew(Ellipsoid(glm::vec3(5.0f, 1.0f, 2.0f)));
                m_reactor = PrimitiveContainer<Ellipsoid>::createNew(Ellipsoid(glm::vec3(2.5f, 0.3f, 0.8f)));
                m_reactorSupport = PrimitiveContainer<Link>::createNew(Link(0.4f, 0.1f, 0.7f));
                m_tail = PrimitiveContainer<Box>::createNew(Box(glm::vec3(0.8f, 0.05f, 2.5f)));
                m_wing = PrimitiveContainer<Box>::createNew(Box(glm::vec3(1.0f, 0.02f, 0.6f)));
                m_zreflection = SpaceTransformerContainer<Reflection>::createNew(Reflection(glm::vec3(0.0f, 0.0f, 1.0f)));
                m_yreflection = SpaceTransformerContainer<Reflection>::createNew(Reflection(glm::vec3(0.0f, 1.0f, 0.0f)));

                m_body->setMaterial(scene.m_whiteMaterial);
                m_cockpit->rotate(glm::angleAxis(glm::radians(15.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
                m_cockpit->translate(glm::vec3(-2.2f, 0.4f, 0.0f));
                m_cockpit->setMaterial(scene.m_cockpitMaterial);
                m_reactor->translate(glm::vec3(4.75f, 1.6f, 1.8f));
                m_reactor->setMaterial(scene.m_goldMaterial);
                m_reactorSupport->rotate(glm::angleAxis(glm::radians(30.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
                m_reactorSupport->rotate(glm::angleAxis(glm::radians(-40.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
                m_reactorSupport->translate(glm::vec3(4.0f, 0.75f, 1.2f));
                m_reactorSupport->setMaterial(scene.m_whiteMaterial);
                m_tail->rotate(glm::angleAxis(glm::radians(20.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
                m_tail->translate(glm::vec3(4.5f, 0.0f, 0.0f));
                m_tail->setMaterial(scene.m_goldMaterial);
                m_wing->rotate(glm::angleAxis(glm::radians(-20.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
                m_wing->rotate(glm::angleAxis(glm::radians(-40.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
                m_wing->translate(glm::vec3(0.0f, 0.25f, 1.5f));
                m_wing->setMaterial(scene.m_goldMaterial);

                m_symmetryUnion->addChildPrimitive(m_body);
                m_symmetryUnion->addChildPrimitive(m_reactor);
                m_symmetryUnion->addChildPrimitive(m_reactorSupport);
                m_symmetryUnion->addChildPrimitive(m_tail);
                m_symmetryUnion->addChildPrimitive(m_wing);
                m_symmetryUnion->addSpaceTransformer(m_zreflection);
                m_symmetryUnion->addSpaceTransformer(m_yreflection);

                m_hullUnion->addChildCombiner(m_symmetryUnion);
                m_hullUnion->addChildPrimitive(m_cockpit);

                m_torusUnion = CombinerContainer<SmoothUnion>::createNew(SmoothUnion(0.8f));
                m_torus = PrimitiveContainer<Torus>::createNew(Torus(3.5f, 0.15f));
                m_sphere1 = PrimitiveContainer<Sphere>::createNew(Sphere(0.6f));
                m_sphere2 = PrimitiveContainer<Sphere>::createNew(Sphere(0.6f));
                m_sphere3 = PrimitiveContainer<Sphere>::createNew(Sphere(0.6f));

                m_torus->rotate(glm::angleAxis(glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
                m_torus->setMaterial(scene.m_darkGreyMaterial);
                m_sphere1->translate(glm::vec3(0.0f, 3.5f, 0.0f));
                m_sphere1->setMaterial(scene.m_darkGreyMaterial);
                m_sphere2->translate(glm::vec3(0.0f, -0.5f * 3.5f, 0.866f * 3.5f));
                m_sphere2->setMaterial(scene.m_darkGreyMaterial);
                m_sphere3->translate(glm::vec3(0.0f, -0.5f * 3.5f, -0.866f * 3.5f));
                m_sphere3->setMaterial(scene.m_darkGreyMaterial);

                m_torusUnion->addChildPrimitive(m_torus);
                m_torusUnion->addChildPrimitive(m_sphere1);
                m_torusUnion->addChildPrimitive(m_sphere2);
                m_torusUnion->addChildPrimitive(m_sphere3);

                m_proxy = PrimitiveContainer<Box>::createNew(Box(glm::vec3(6.5f, 4.5f, 4.3f)));
                m_proxy->translate(glm::vec3(1.5f, 0.0f, 0.0f));

                m_root->addChildCombiner(m_hullUnion);
                m_root->addChildCombiner(m_torusUnion);

                m_speed = 0.5f + (float)rand() / (float)RAND_MAX * 2.0f;
                m_rotationSpeed = 0.05f + (float)rand() / (float)RAND_MAX * 0.2f;
            }

            std::shared_ptr<VCombinerContainer> getRootCombiner() { return m_root; }

            void update(double t, float dt) {
                m_torusUnion->rotate(glm::angleAxis(2.0f * dt, glm::vec3(1.0f, 0.0f, 0.0f)));
                m_torusUnion->setPosition(glm::vec3(1.75f + sin(t) * 0.1f, sin(t) * 0.2f, cos(t) * 0.2f));

                m_root->rotate(glm::angleAxis(m_rotationSpeed * dt, glm::vec3(0.0f, 1.0f, 0.0f)));
                m_root->translate(m_speed * dt * (m_root->getOrientation() * glm::vec3(-1.0f, 0.0f, 0.0f)));
            }

            void setProxy(bool setProxy) {
                m_root->setProxy(setProxy ? m_proxy : nullptr);
            }
    };

    std::shared_ptr<Material> m_whiteMaterial;
    std::shared_ptr<Material> m_goldMaterial;
    std::shared_ptr<Material> m_darkGreyMaterial;
    std::shared_ptr<Material> m_cockpitMaterial;

    std::vector<Ship> m_ships;

    std::shared_ptr<PrimitiveContainer<Plane>> m_plane;

    std::shared_ptr<CombinerContainer<Union>> m_primitiveCombiner;

    std::shared_ptr<LightContainer<DirectionalLight>> m_light1;
    std::shared_ptr<LightContainer<DirectionalLight>> m_light2;

    double m_time;
    bool m_usingProxy = false;

public:
    virtual void setup() {
        m_whiteMaterial = std::shared_ptr<Material>(new Material(glm::vec3(1.0f, 1.0f, 1.0f), 0.0f, 0.1f));
        m_goldMaterial = std::shared_ptr<Material>(new Material(glm::vec3(1.0f, 0.7f, 0.3f), 1.0f, 0.5f));
        m_darkGreyMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.5f, 0.5f, 0.5f), 1.0f, 0.5f));
        m_cockpitMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.5f, 0.8f, 1.0f), 1.0f, 0.2f));

        m_plane = PrimitiveContainer<Plane>::createNew(Plane(glm::vec3(0.0f, 1.0f, 0.0f)));
        m_primitiveCombiner = CombinerContainer<Union>::createNew(Union());

        m_primitiveCombiner->addChildPrimitive(m_plane);

        addCombiner(m_primitiveCombiner);

        for (int i=0; i<16; ++i) {
            m_ships.push_back(Ship());
            m_ships[i].setup(*this);
            m_ships[i].getRootCombiner()->translate(glm::vec3(((i % 4) - 2) * 20.0f, 6.0f, ((i / 4) - 2) * 20.0f));
            m_ships[i].getRootCombiner()->rotate(glm::angleAxis((float)rand() / (float)RAND_MAX * 3.14f, glm::vec3(0.0f, 1.0f, 0.0f)));
        }
        addCombiner(m_ships[0].getRootCombiner());

        m_light1 = LightContainer<DirectionalLight>::createNew(DirectionalLight(glm::vec3(1.0f), 2.0f, 0.2f));
        m_light2 = LightContainer<DirectionalLight>::createNew(DirectionalLight(glm::vec3(1.0f), 2.0f, 0.2f));
        m_light1->light.setOrientation(glm::angleAxis(glm::radians(-45.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
        m_light2->light.setOrientation(glm::angleAxis(glm::radians(45.0f), glm::vec3(1.0f, 0.0f, 0.0f)));

        addLight(m_light1);
        addLight(m_light2);

        m_camera = std::shared_ptr<Camera>(new Camera(glm::radians(45.0f), 1.0f, 0.1f, 100.0f));
        m_camera->translate(glm::vec3(-40.0f, 10.0f, -25.0f));
    }

    virtual void update(double dt) {
        m_time += dt;
        for (auto ship : m_ships) {
            ship.update(m_time, dt);
        }
    }

    virtual void keyEvent(int key, int scancode, int action, int mods) {
        if (key == GLFW_KEY_P && action == GLFW_PRESS) {
            m_usingProxy = !m_usingProxy;
            for (auto ship : m_ships) {
                ship.setProxy(m_usingProxy);
            }
        }
        if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
            for (int i=1; i<16; ++i) {
                addCombiner(m_ships[i].getRootCombiner());
            }
        }
        if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
            for (int i=1; i<16; ++i) {
                removeCombiner(m_ships[i].getRootCombiner());
            }
        }
    }
};

#endif