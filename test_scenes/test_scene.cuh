#ifndef TEST_SCENE_h
#define TEST_SCENE_h

#include "../scene/scene.cuh"

class TestScene : public Scene {
public:
    virtual void setup() = 0;
    virtual void update(double dt) = 0;
    virtual void keyEvent(int key, int scancode, int action, int mods) = 0;
};

#endif