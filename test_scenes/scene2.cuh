#ifndef SCENE2_h
#define SCENE2_h

#include <glm/gtc/random.hpp>

#include "test_scene.cuh"

class Scene2 : public TestScene {
protected:
    std::shared_ptr<Material> m_unionMaterial;

    std::shared_ptr<PrimitiveContainer<Plane>> m_plane;
    std::shared_ptr<PrimitiveContainer<Sphere>> m_sphere1;
    std::shared_ptr<PrimitiveContainer<Sphere>> m_sphere2;
    std::shared_ptr<PrimitiveContainer<Box>> m_box;

    std::shared_ptr<CombinerContainer<Union>> m_root;
    std::shared_ptr<CombinerContainer<SmoothUnion>> m_smoothUnion;

    std::shared_ptr<LightContainer<PointLight>> m_light1;
    std::shared_ptr<LightContainer<PointLight>> m_light2;
    std::shared_ptr<LightContainer<PointLight>> m_light3;

    double m_time;
    bool m_isMetal = false;

public:
    virtual void setup() {
        m_unionMaterial = std::shared_ptr<Material>(new Material(glm::vec3(0.8f, 0.8f, 0.8f), 0.0f, 0.05f));

        m_plane = PrimitiveContainer<Plane>::createNew(Plane(glm::vec3(0.0f, 1.0f, 0.0f)));
        m_sphere1 = PrimitiveContainer<Sphere>::createNew(Sphere(1.0f));
        m_sphere2 = PrimitiveContainer<Sphere>::createNew(Sphere(1.0f));
        m_box = PrimitiveContainer<Box>::createNew(Box(glm::vec3(1.0f)));
        m_root = CombinerContainer<Union>::createNew(Union());
        m_smoothUnion = CombinerContainer<SmoothUnion>::createNew(SmoothUnion(1.0f));

        m_plane->translate(glm::vec3(0.0f, -6.0f, 0.0f));

        m_sphere1->setMaterial(m_unionMaterial);
        m_sphere2->setMaterial(m_unionMaterial);
        m_box->setMaterial(m_unionMaterial);

        m_light1 = LightContainer<PointLight>::createNew(PointLight(glm::vec3(1.0f, 0.0f, 0.0f), 80.0f, 0.2f));
        m_light2 = LightContainer<PointLight>::createNew(PointLight(glm::vec3(0.0f, 1.0f, 0.0f), 80.0f, 0.2f));
        m_light3 = LightContainer<PointLight>::createNew(PointLight(glm::vec3(0.0f, 0.0f, 1.0f), 80.0f, 0.2f));

        m_light1->light.setPosition(glm::vec3(5.0f, 5.0f, 0.0f));
        m_light2->light.setPosition(glm::vec3(3.0f, 5.0f, 4.0f));
        m_light3->light.setPosition(glm::vec3(0.0f, 5.0f, 5.0f));

        m_smoothUnion->addChildPrimitive(m_sphere1);
        m_smoothUnion->addChildPrimitive(m_sphere2);
        m_smoothUnion->addChildPrimitive(m_box);

        m_root->addChildPrimitive(m_plane);

        addCombiner(m_root);
        addCombiner(m_smoothUnion);
        addLight(m_light1);
        addLight(m_light2);
        addLight(m_light3);

        m_camera = std::shared_ptr<Camera>(new Camera(glm::radians(45.0f), 1.0f, 0.1f, 100.0f));
        m_camera->translate(glm::vec3(0.0, 0.0, 5.0));
    }

    virtual void update(double dt) {
        m_time += dt;
        m_sphere1->setPosition(glm::vec3(4.0 * sin(m_time), 0.0, 0.0));
        m_sphere2->setPosition(glm::vec3(0.0, 4.0 * cos(m_time), 0.0));

        m_light1->light.setPosition(glm::angleAxis(0.4f * (float)dt, glm::vec3(0.0f, 1.0f, 0.0f)) * m_light1->light.getPosition());
        m_light2->light.setPosition(glm::angleAxis(0.3f * (float)dt, glm::vec3(0.0f, 1.0f, 0.0f)) * m_light2->light.getPosition());
        m_light3->light.setPosition(glm::angleAxis(0.2f * (float)dt, glm::vec3(0.0f, 1.0f, 0.0f)) * m_light3->light.getPosition());
    }

    virtual void keyEvent(int key, int scancode, int action, int mods) {
        if (key == GLFW_KEY_M && action == GLFW_PRESS) {
            m_isMetal = !m_isMetal;
            m_unionMaterial->metalness = m_isMetal ? 1.0f : 0.0f;
        }
    }
};

#endif