#ifndef SCENE5_h
#define SCENE5_h

#include "test_scene.cuh"

class Scene5 : public TestScene {
protected:
    std::shared_ptr<SpaceTransformerContainer<Twist>> m_twist;

    std::vector<std::shared_ptr<Material>> m_materials;
    std::vector<std::shared_ptr<DistanceTransformerContainer<SphericalDisplacementMap>>> m_sdms;
    std::vector<std::shared_ptr<VPrimitiveContainer>> m_primitives;

    std::shared_ptr<PrimitiveContainer<Plane>> m_plane;

    std::shared_ptr<CombinerContainer<Union>> m_root;

    std::shared_ptr<LightContainer<DirectionalLight>> m_light1;
    std::shared_ptr<LightContainer<PointLight>> m_light2;

    Texture m_monasteryAlbedoTex = Texture("../textures/monastery_stone_floor_diff_4k.jpg");
    Texture m_monasteryRoughTex = Texture("../textures/monastery_stone_floor_rough_4k.jpg");
    Texture m_monasteryDispTex = Texture("../textures/monastery_stone_floor_disp_4k.jpg");

    Texture m_marbleAlbedoTex = Texture("../textures/marble_albedo.jpg");
    Texture m_marbleMetalTex = Texture("../textures/marble_metal.jpg");
    Texture m_marbleRoughTex = Texture("../textures/marble_rough.jpg");
    Texture m_marbleDispTex = Texture("../textures/marble_disp.jpg");

    Texture m_chemicalAlbedoTex = Texture("../textures/chemical_albedo.jpg");
    Texture m_chemicalDispTex = Texture("../textures/chemical_disp.jpg");

    Texture m_plateAlbedoTex = Texture("../textures/metal_plate_diff_4k.jpg");
    Texture m_plateMetalTex = Texture("../textures/metal_plate_metal_4k.jpg");
    Texture m_plateRoughTex = Texture("../textures/metal_plate_rough_4k.jpg");
    Texture m_plateDispTex = Texture("../textures/metal_plate_disp_4k.jpg");

    Texture m_scalesAlbedoTex = Texture("../textures/scales_albedo.jpg");
    Texture m_scalesRoughTex = Texture("../textures/scales_rough.jpg");
    Texture m_scalesDispTex = Texture("../textures/scales_disp.jpg");

    Texture m_cyberAlbedoTex = Texture("../textures/cyber_albedo.jpg");
    Texture m_cyberMetalTex = Texture("../textures/cyber_metal.jpg");
    Texture m_cyberRoughTex = Texture("../textures/cyber_rough.jpg");
    Texture m_cyberDispTex = Texture("../textures/cyber_disp.jpg");

    Texture m_goldAlbedoTex = Texture("../textures/gold_albedo.jpg");
    Texture m_goldMetalTex = Texture("../textures/gold_metal.jpg");
    Texture m_goldDispTex = Texture("../textures/gold_disp.jpg");

    double m_time;
    unsigned int currentPrimitiveIdx = 0;
    bool m_twisted;

public:
    virtual void setup() {
        m_twist = SpaceTransformerContainer<Twist>::createNew(Twist(0.5f));

        m_monasteryAlbedoTex.uploadToDevice();
        m_monasteryRoughTex.uploadToDevice();
        m_monasteryDispTex.uploadToDevice();
        m_materials.push_back(std::shared_ptr<Material>(new Material(glm::vec3(1.0f), 0.0f, 0.05f)));
        m_materials[0]->albedoTex = m_monasteryAlbedoTex.getTexObj();
        m_materials[0]->roughnessTex = m_monasteryRoughTex.getTexObj();
        m_materials[0]->textureScale = 2.0f;
        m_materials[0]->triPlanarNormalWeight = 4.0f;
        m_sdms.push_back(DistanceTransformerContainer<SphericalDisplacementMap>::createNew(
            SphericalDisplacementMap(m_monasteryDispTex.getTexObj(), 2.0f, 0.1f, 4.0f)));

        m_marbleAlbedoTex.uploadToDevice();
        m_marbleMetalTex.uploadToDevice();
        m_marbleRoughTex.uploadToDevice();
        m_marbleDispTex.uploadToDevice();
        m_materials.push_back(std::shared_ptr<Material>(new Material(glm::vec3(1.0f), 0.0f, 0.05f)));
        m_materials[1]->albedoTex = m_marbleAlbedoTex.getTexObj();
        m_materials[1]->metalnessTex = m_marbleMetalTex.getTexObj();
        m_materials[1]->roughnessTex = m_marbleRoughTex.getTexObj();
        m_materials[1]->textureScale = 1.0f;
        m_materials[1]->triPlanarNormalWeight = 4.0f;
        m_sdms.push_back(DistanceTransformerContainer<SphericalDisplacementMap>::createNew(
            SphericalDisplacementMap(m_marbleDispTex.getTexObj(), 2.0f, 0.1f, 4.0f)));

        m_chemicalAlbedoTex.uploadToDevice();
        m_chemicalDispTex.uploadToDevice();
        m_materials.push_back(std::shared_ptr<Material>(new Material(glm::vec3(1.0f), 0.5f, 0.2f)));
        m_materials[2]->albedoTex = m_chemicalAlbedoTex.getTexObj();
        m_materials[2]->textureScale = 2.0f;
        m_materials[2]->triPlanarNormalWeight = 4.0f;
        m_sdms.push_back(DistanceTransformerContainer<SphericalDisplacementMap>::createNew(
            SphericalDisplacementMap(m_chemicalDispTex.getTexObj(), 2.0f, 0.1f, 4.0f)));

        m_plateAlbedoTex.uploadToDevice();
        m_plateMetalTex.uploadToDevice();
        m_plateRoughTex.uploadToDevice();
        m_plateDispTex.uploadToDevice();
        m_materials.push_back(std::shared_ptr<Material>(new Material(glm::vec3(1.0f), 0.0f, 0.05f)));
        m_materials[3]->albedoTex = m_plateAlbedoTex.getTexObj();
        m_materials[3]->metalnessTex = m_plateMetalTex.getTexObj();
        m_materials[3]->roughnessTex = m_plateRoughTex.getTexObj();
        m_materials[3]->textureScale = 2.0f;
        m_materials[3]->triPlanarNormalWeight = 4.0f;
        m_sdms.push_back(DistanceTransformerContainer<SphericalDisplacementMap>::createNew(
            SphericalDisplacementMap(m_plateDispTex.getTexObj(), 2.0f, 0.1f, 4.0f)));

        m_scalesAlbedoTex.uploadToDevice();
        m_scalesRoughTex.uploadToDevice();
        m_scalesDispTex.uploadToDevice();
        m_materials.push_back(std::shared_ptr<Material>(new Material(glm::vec3(1.0f), 0.6f, 0.05f)));
        m_materials[4]->albedoTex = m_scalesAlbedoTex.getTexObj();
        m_materials[4]->roughnessTex = m_scalesRoughTex.getTexObj();
        m_materials[4]->textureScale = 2.0f;
        m_materials[4]->triPlanarNormalWeight = 4.0f;
        m_sdms.push_back(DistanceTransformerContainer<SphericalDisplacementMap>::createNew(
            SphericalDisplacementMap(m_scalesDispTex.getTexObj(), 2.0f, 0.1f, 4.0f)));

        m_cyberAlbedoTex.uploadToDevice();
        m_cyberMetalTex.uploadToDevice();
        m_cyberRoughTex.uploadToDevice();
        m_cyberDispTex.uploadToDevice();
        m_materials.push_back(std::shared_ptr<Material>(new Material(glm::vec3(1.0f), 0.0f, 0.05f)));
        m_materials[5]->albedoTex = m_cyberAlbedoTex.getTexObj();
        m_materials[5]->metalnessTex = m_cyberMetalTex.getTexObj();
        m_materials[5]->roughnessTex = m_cyberRoughTex.getTexObj();
        m_materials[5]->textureScale = 2.0f;
        m_materials[5]->triPlanarNormalWeight = 4.0f;
        m_sdms.push_back(DistanceTransformerContainer<SphericalDisplacementMap>::createNew(
            SphericalDisplacementMap(m_cyberDispTex.getTexObj(), 2.0f, 0.1f, 4.0f)));

        m_goldAlbedoTex.uploadToDevice();
        m_goldMetalTex.uploadToDevice();
        m_goldDispTex.uploadToDevice();
        m_materials.push_back(std::shared_ptr<Material>(new Material(glm::vec3(1.0f), 0.0f, 0.3f)));
        m_materials[6]->albedoTex = m_goldAlbedoTex.getTexObj();
        m_materials[6]->metalnessTex = m_goldMetalTex.getTexObj();
        m_materials[6]->textureScale = 2.0f;
        m_materials[6]->triPlanarNormalWeight = 4.0f;
        m_sdms.push_back(DistanceTransformerContainer<SphericalDisplacementMap>::createNew(
            SphericalDisplacementMap(m_goldDispTex.getTexObj(), 2.0f, 0.1f, 4.0f)));

        m_root = CombinerContainer<Union>::createNew(Union());

        m_plane = PrimitiveContainer<Plane>::createNew(Plane(glm::vec3(0.0f, 1.0f, 0.0f)));
        m_root->addChildPrimitive(m_plane);
        
        for (int i=0; i<m_materials.size(); ++i) {
            m_primitives.push_back(PrimitiveContainer<Sphere>::createNew(Sphere(1.0f)));
            m_primitives[i]->setMaterial(m_materials[i]);
            m_primitives[i]->addDistanceTransformer(m_sdms[i]);
            m_root->addChildPrimitive(m_primitives[i]);
        }
        
        for (int i=0; i<m_materials.size(); ++i) {
            m_primitives.push_back(PrimitiveContainer<Box>::createNew(Box(glm::vec3(0.6f))));
            m_primitives[i + m_materials.size()]->setMaterial(m_materials[i]);
            m_primitives[i + m_materials.size()]->addDistanceTransformer(m_sdms[i]);
            // m_primitives[i + m_materials.size()]->rotate(glm::angleAxis(glm::radians(45.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
            // m_primitives[i + m_materials.size()]->rotate(glm::angleAxis(glm::radians(45.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
        }
        
        for (int i=0; i<m_materials.size(); ++i) {
            m_primitives.push_back(PrimitiveContainer<Octahedron>::createNew(Octahedron(1.0f)));
            m_primitives[i + 2*m_materials.size()]->setMaterial(m_materials[i]);
            m_primitives[i + 2*m_materials.size()]->addDistanceTransformer(m_sdms[i]);
        }
        
        for (int i=0; i<m_materials.size(); ++i) {
            m_primitives.push_back(PrimitiveContainer<Torus>::createNew(Torus(1.0f, 0.15f)));
            m_primitives[i + 3*m_materials.size()]->setMaterial(m_materials[i]);
            m_primitives[i + 3*m_materials.size()]->addDistanceTransformer(m_sdms[i]);
            m_primitives[i + 3*m_materials.size()]->rotate(glm::angleAxis(glm::radians(45.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
        }

        m_light1 = LightContainer<DirectionalLight>::createNew(DirectionalLight(glm::vec3(1.0f), 2.0f, 0.4f));
        m_light2 = LightContainer<PointLight>::createNew(PointLight(glm::vec3(1.0f), 80.0f, 0.4f));
        m_light1->light.setOrientation(glm::angleAxis(glm::radians(45.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
        m_light2->light.setPosition(glm::vec3(0.0f, 5.0f, 0.0f));

        addCombiner(m_root);
        addLight(m_light1);
        addLight(m_light2);

        m_camera = std::shared_ptr<Camera>(new Camera(glm::radians(45.0f), 1.0f, 0.1f, 100.0f));
        m_camera->translate(glm::vec3(0.0f, 2.5f, 0.0f));
    }

    virtual void update(double dt) {
        m_time += dt;
        m_light1->light.rotate(glm::angleAxis(0.1f * (float) dt, glm::vec3(0.0f, 1.0f, 0.0f)));
        float r = 5.0f;
        float a = 2.0 * M_PI / m_materials.size();

        for (int i=currentPrimitiveIdx; i<currentPrimitiveIdx+m_materials.size(); ++i) {
            m_primitives[i]->rotate(glm::angleAxis(0.2f * (float) dt, glm::vec3(0.0f, 1.0f, 0.0f)));
            m_primitives[i]->setPosition(glm::vec3(r * sin(i * a), 1.6f + 0.2f*sin((float)i + m_time), r * cos(i * a)));
        }
    }

    virtual void keyEvent(int key, int scancode, int action, int mods) {
        if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
            for (int i=currentPrimitiveIdx; i<currentPrimitiveIdx+m_materials.size(); ++i) {
                m_root->removeChildPrimitive(m_primitives[i]);
            }
            currentPrimitiveIdx = (currentPrimitiveIdx + m_primitives.size() - m_materials.size()) % m_primitives.size();
            for (int i=currentPrimitiveIdx; i<currentPrimitiveIdx+m_materials.size(); ++i) {
                m_root->addChildPrimitive(m_primitives[i]);
            }
        }
        if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
            for (int i=currentPrimitiveIdx; i<currentPrimitiveIdx+m_materials.size(); ++i) {
                m_root->removeChildPrimitive(m_primitives[i]);
            }
            currentPrimitiveIdx = (currentPrimitiveIdx + m_materials.size()) % m_primitives.size();
            for (int i=currentPrimitiveIdx; i<currentPrimitiveIdx+m_materials.size(); ++i) {
                m_root->addChildPrimitive(m_primitives[i]);
            }
        }
        if (key == GLFW_KEY_T && action == GLFW_PRESS) {
            m_twisted = !m_twisted;
            for (int i=0; i<m_primitives.size(); ++i) {
                if (m_twisted) {
                    m_primitives[i]->addSpaceTransformer(m_twist);
                } else {
                    m_primitives[i]->removeSpaceTransformer(m_twist);
                }
            }
        }
    }
};

#endif